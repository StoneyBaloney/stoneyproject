#pragma once
#include "pch.h"
#include <PxPhysicsAPI.h>
#include <extensions/PxDefaultAllocator.h>
namespace StoneyCore {
	namespace StPxPhysics {
		template <typename T>
		void StPxRelease(T*& ptr) {
			if (ptr)
			{
				ptr->release();
				ptr = nullptr;
			}
		}

		constexpr auto PVD_ADDR = "127.0.0.1";

		using namespace physx;
		class StPxAllocator : public PxAllocatorCallback {
			// Inherited via PxAllocatorCallback
			virtual void* allocate(size_t size, const char* typeName, const char* filename, int line) override;

			virtual void deallocate(void* ptr) override;
		};
		class StPxErrorCallback : public PxErrorCallback {
			// Inherited via PxErrorCallback
			virtual void reportError(PxErrorCode::Enum code, const char* message, const char* file, int line) override;
		};
		struct StPxObjectParams {
			PxConvexFlag::Enum m_flag{ PxConvexFlag::eCOMPUTE_CONVEX };
		};
		struct StPxActorInfo {
			uint32 instance_index{ 0 };
			uint32 user_id{ 0 };
		};
		struct StPxInstanceHandle {
			uint32 shape_id{ 0 };
			uint32 px_instance_id{ 0 };
			uint32 entity_id{ 0 };
			physx::PxRigidActor* m_actor{ nullptr };
		};
		struct StPxHitResult {
			StPxActorInfo* actor_info{ nullptr };
			StPxInstanceHandle* instance_handle{ nullptr };
		};
		struct StPxShapeHandle {
			uint32 shape_id{ 0 };
		};
		PxVec3 MakeVec(glm::vec3 in_vec);
		class StPxWorld
		{
			DataRegistrar<> m_data_reg;
			PxFoundation* m_fountation{ nullptr };
			PxPhysics* m_physics{ nullptr };
			PxDefaultCpuDispatcher* m_dispatcher{ nullptr };
			PxScene* m_scene{ nullptr };
			PxPvd* m_pvd{ nullptr };
			PxCooking* m_cooking{ nullptr };
			stVector<PxConvexMesh*> m_meshes;
			stVector< PxRigidActor*> m_actors;
			StPxAllocator m_allocator;
			StPxErrorCallback m_error_callback;
			uint32 next_actor_user_data_id{ 0 };
		public:
			void BeginUpdateScene(float& delta) {
				m_scene->simulate(delta * 0.001f);
			}
			PxRigidActor* GetRigidActor(uint32 instance_id) {
				return m_actors[instance_id];
			}
			void GetPhysicsResults() {
				m_scene->fetchResults(true);
			}
			void InitWorld() {
				m_fountation = PxCreateFoundation(PX_PHYSICS_VERSION, m_allocator, m_error_callback);

				m_pvd = PxCreatePvd(*m_fountation);
				PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate(PVD_ADDR, 5425, 10);
				m_pvd->connect(*transport, PxPvdInstrumentationFlag::eALL);

				m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_fountation, PxTolerancesScale(), true, m_pvd);
				m_dispatcher = PxDefaultCpuDispatcherCreate(2);

				PxSceneDesc scene_desc(m_physics->getTolerancesScale());
				scene_desc.cpuDispatcher = m_dispatcher;
				scene_desc.filterShader = PxDefaultSimulationFilterShader;
				m_scene = m_physics->createScene(scene_desc);
				m_cooking = PxCreateCooking(PX_PHYSICS_VERSION, *m_fountation, PxCookingParams(PxTolerancesScale()));
			}

			uint32 AddInstance(uint32 mesh_id, const StoneyCore::Transform& transform, uint32 user_id = 0) {
				PxTransform localTm(PxVec3(transform.location.x, transform.location.y, transform.location.z));
				PxRigidStatic* static_bod = m_physics->createRigidStatic(localTm);
				m_actors.emplace_back(static_bod);
				auto&& ret = uint32(m_actors.size() - 1);
				PxConvexMeshGeometry geo(m_meshes[mesh_id]);
				auto shape = m_physics->createShape(geo, *m_physics->createMaterial(0.5f, 0.5f, 0.5f));
				static_bod->attachShape(*shape);
				m_scene->addActor(*static_bod);

				auto user_data = m_data_reg.Create< StPxActorInfo>(next_actor_user_data_id++);
				user_data->instance_index = ret;
				user_data->user_id = user_id;
				static_bod->userData = user_data;
				return ret;
			}
			bool RaytestAll(glm::vec3 origin, glm::vec3 target, StPxHitResult& out_result) {
				PxRaycastBuffer res;
				if (m_scene->raycast(MakeVec(origin), MakeVec(target), 10000000.f, res))
				{
					out_result.actor_info = static_cast<StPxActorInfo*>(res.block.actor->userData);
					return true;
				}
				else {
					return false;
				}
			}

			uint32 AddObject(stVector<physx::PxVec3>& verts, StoneyCore::StPxPhysics::StPxObjectParams& phys_params)
			{
				PxConvexMeshDesc convex_desc;
				convex_desc.points.count = PxU32(verts.size());
				convex_desc.points.stride = sizeof(PxVec3);
				convex_desc.points.data = verts.data();
				convex_desc.flags = phys_params.m_flag;
				PxDefaultMemoryOutputStream mesh_cook;
				if (m_cooking->cookConvexMesh(convex_desc, mesh_cook))
				{
					PxU32 mesh_size = mesh_cook.getSize();
					PxDefaultMemoryInputData input_stream(mesh_cook.getData(), mesh_cook.getSize());
					PxConvexMesh* convex = m_physics->createConvexMesh(input_stream);
					m_meshes.emplace_back(convex);
					return uint32(m_meshes.size() - 1);
				}
				else {
					FatalError("Failed to cook mesh. PX, \n");
					return 0;
				}
			}

			uint32 AddObject2(StoneyCore::ModelVertices& mesh, StPxObjectParams phys_params) {
				stVector<PxVec3> verts;

				for (auto& index : mesh.indices) {
					verts.emplace_back(
						mesh.vertices[index * 3],
						mesh.vertices[index * 3 + 1],
						mesh.vertices[index * 3 + 2]
					);
				}

				return AddObject(verts, phys_params);
			}

			void CleanupWorld() {
				StPxRelease(m_dispatcher);
				StPxRelease(m_physics);

				if (m_pvd)
				{
					auto tp = m_pvd->getTransport();
					StPxRelease(m_pvd);
					StPxRelease(tp);
				}
				StPxRelease(m_cooking);

				StPxRelease(m_fountation);
			}
			StPxWorld();
			~StPxWorld();
		};
	}
}