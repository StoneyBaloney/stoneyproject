#pragma once
#include <stdint.h>
#include <unordered_map>
#include "pch.h"
#include "TaskScheduler.h"

namespace StoneyCore {
	template <typename T>
	struct TypeHelper {
	};
	struct ComponentInfo {
		template<typename DataType>
		ComponentInfo(TypeHelper<DataType>) :
			size(sizeof(DataType))
		{}
		const long long size{};
	};
	class SceneHandler;
	class ComponentRegister {
		friend class SceneHandler;
		friend class Editor::EditorSceneHandler;
		std::unordered_map<uint32_t, uint32_t> m_component_map; // Key is data id according to data_reg, val is index into component info array
		stVector<ComponentInfo> component_infos;
		DataRegistrar<> data_reg;

		uint32 next_id{ 0 };

		template <typename DataType>
		uint32 RegisterComponentType() {
			auto&& t_id = data_reg.GetTypeID<stVector<DataType>>();

			if (m_component_map.find(t_id) == m_component_map.end())
			{
				auto&& ret_id = next_id++;
				data_reg.Create<stVector<DataType>>(ret_id);
				TypeHelper<DataType> data_type;
				component_infos.emplace_back(ComponentInfo(data_type));
				return uint32(component_infos.size() - 1);
			}
			else {
				return 0;
			}
		}
		template <typename DataType, typename...Args>
		uint32 RegisterComponent(Args...args) {
			auto component_vec = data_reg.GetData<stVector<DataType>>(m_component_map[data_reg.GetTypeID<stVector<DataType>>()]);
			component_vec->emplace_back(args...);
			return uint32(component_vec->size() - 1);
		}
	};
}