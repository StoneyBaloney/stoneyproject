#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

namespace StoneyCore {
	using namespace glm;

	struct Camera {
		Camera(
			float _FOV = 70.f,
			float _AspectRatio = 800.f / 600.f,
			float _ZNear = 0.2f,
			float _ZFar = 10000.f);

		vec3 m_location;
		vec3 m_rotation;
		vec3 m_velocity;
		vec3 m_target;
		vec3 direction;
		mat4 m_current;
		float m_lerp = 1;
		float m_fov{};
		float m_aspect{};
		float m_near{};
		float m_far{};
		void SetLerp(float _lerp) { m_lerp = _lerp; };
		void MoveCameraX(float _x) { m_location.x += _x; m_velocity.x = _x; };
		void MoveCameraY(float _y) { m_location.y += _y; m_velocity.y = _y; };
		void MoveCameraZ(float _z) { m_location.z += _z; m_velocity.z = _z; };
		void RotateCameraX(float _x) { m_rotation.x += m_rotation.x + _x > 89.f || m_rotation.x + _x < -89.f ? 0 : _x; };
		void RotateCameraY(float _y) { m_rotation.y += _y; };
		void RotateCameraZ(float _z) { m_rotation.z += _z; };
		void SetLocation(float _x, float _y, float _z) { m_location = { _x,_y,_z }; }
		void SetRotation(float _x, float _y, float _z) { m_rotation = { _x,_y,_z }; }
		void MoveForward(float _dist) {
			SetDirectionVector();
			m_location += direction * m_lerp;
		}
		void MoveBack(float _dist) {
			SetDirectionVector();
			m_location -= direction;
		}
		void SetDirectionVector()
		{
			direction.x = cos(radians(m_rotation.y)) * cos(radians(m_rotation.x));
			direction.y = sin(radians(m_rotation.x));
			direction.z = sin(radians(m_rotation.y)) * cos(radians(m_rotation.x));
		}
		mat4 GetViewMatrix() {
			vec3 right(normalize(cross(direction, vec3(0, 1, 0))));

			vec3 up(normalize(glm::cross(right, direction)));
			return lookAt(m_location, vec3(direction.x, direction.y, direction.z) +
				m_location, up);
		}
		mat4 GetPerspectiveMat() {
			return perspective(m_fov, m_aspect, m_near, m_far);
		}
		mat4 GetProjectionMat() {
			SetDirectionVector();

			return
				GetPerspectiveMat() *
				GetViewMatrix() *
				mat4(1);
		}
		vec3 Position() {
			return m_location;
		}
		void SetProjectionMat(mat4& _target_mat) {
			_target_mat = GetProjectionMat();

			//m_target += (vec3(m_current[0][2], m_current[1][2], m_current[2][2]) *-m_velocity.z  +				//current forward
			//
			//			vec3(m_current[0][0], m_current[1][0], m_current[2][0]) * m_velocity.x) * m_lerp;            //current lateral

			//m_current = glm::rotate(mat4(1), m_rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			//m_current *= glm::rotate(mat4(1), m_rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			//m_current *= translate(mat4(1), vec3(-m_target));
			//
			//_target_mat = perspective(m_fov, m_aspect, m_near, m_far) * m_current * mat4(1);

			//m_target = { 0 };
			//m_velocity = { 0 };
			//
		};
	}; // struct Camera
};
