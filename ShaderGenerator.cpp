#include "ShaderGenerator.h"
namespace StoneyCore {
	namespace Renderer {
		constexpr auto RGL_VERSION_STRING = "#version 460\n";
		constexpr auto TEX_UNIT_DIFFUSE = "layout (binding = 0)uniform sampler2DArray diffuse_textures;\n";
		constexpr auto TEX_UNIT_NORMALS = "layout (binding = 1)uniform sampler2DArray normal_textures;\n";
		constexpr auto TEX_UNIT_SPECULAR = "layout (binding = 2)uniform sampler2DArray specular_textures;\n";
		constexpr auto TEX_UNIT_AMBIENT = "layout (binding = 3)uniform sampler2DArray ambient_textures;\n";
		constexpr auto TEX_UNIT_EMISSIVE = "layout (binding = 4)uniform sampler2DArray emissive_textures;\n";
		constexpr auto TEX_UNIT_HEIGHT = "layout (binding = 5)uniform sampler2DArray height_textures;\n";
		constexpr auto TEX_UNIT_OPACITY = "layout (binding = 6)uniform sampler2DArray opacity_textures;\n";
		constexpr auto TEX_UNIT_LIGHTING = "layout (binding = 7)uniform sampler2DArray lighting_textures;\n";
		constexpr auto GENERATE_TBN_FUNC =
			"void SetTBN(vec3 normal){\n"			
			"mat3 normal_matrix = mat3(MTransform);\n"
			"TBN = mat3(Tangent, Bitangent, Normal) * normal_matrix;\n"
			"}\n";

		//case StoneyCore::TEX_TYPE_DIFFUSE:
		//	out_frag +=
		//		"uniform sampler2DArray diffuse_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_NORM:
		//	out_frag +=
		//		"uniform sampler2DArray normal_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_SPECULAR:
		//	out_frag +=
		//		"uniform sampler2DArray specular_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_AMBIENT:
		//	out_frag +=
		//		"uniform sampler2DArray ambient_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_EMISSIVE:
		//	out_frag +=
		//		"uniform sampler2DArray emissive_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_HEIGHT:
		//	out_frag +=
		//		"uniform sampler2DArray height_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_OPACITY:
		//	out_frag +=
		//		"uniform sampler2DArray opacity_textures;\n";
		//	break;
		//case StoneyCore::TEX_TYPE_LIGHTING:
		//	out_frag +=
		//		"uniform sampler2DArray lighting_textures;\n";
		//	break;
		//default:
		//	break;
	//}
		template <typename Val, typename...IntType>
		bool AllBitsSet(Val val, IntType...ints) {
			return ((val & ints) && ...);
		}

		constexpr const char* GetDefaultDeferredGeomVertSource() {
			return
				"	void main() {\n"
				"	DrawID = gl_DrawID;\n"
				"	frag_normal = vec4(vec4(Normal, 1.0) * MTransform).xyz;\n"
				"	frag_diff_uv = TexCoord;\n"
				"	vec4 ray_start =\n"
				"		inverse(Matrices.MView) *\n"
				"			vec4((vec2(0.5,0.5) * 2) - 1,\n"
				"		-1.0,\n"
				"		1.0f);\n"
				"	ray_start /= ray_start.w;\n"
				"	vec4 world_pos = MTransform * vec4(VertexPosition, 1.0);\n"
				"	vec4 g_pos = Matrices.MVP * Matrices.MView * world_pos;\n"
				"	cam_pos = ray_start;\n"
				"	frag_pos = world_pos.xyz;\n"
				"	gl_Position = g_pos;\n"
				"}\n";
		}

		void GetDefaultDeferredGeomVertSource(stString& out_vert_shader) {
			out_vert_shader += GetDefaultDeferredGeomVertSource();
		}
		void GenerateVetexOutputOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_vertex_shader) {
			auto remaining_outputs = params.vertex_outputs;
			auto val = glm::findLSB(remaining_outputs);
			const auto& outputs = params.vertex_outputs;
			const auto& inputs = params.vertex_inputs;

			if (params.texture_flags & (1<<TEX_TYPE_NORM))
			{
				out_vertex_shader += GENERATE_TBN_FUNC;
			}


			out_vertex_shader += "void main (){\n";
			if (AllBitsSet(inputs, SHADER_IN_WORLD_TRANSFORM, SHADER_IN_POSITION, SHADER_IN_PROJECTION))
			{
				out_vertex_shader +=
					"vec4 world_pos = MTransform * vec4((VertexPosition),1.0);\n"
					"vec4 g_pos = Matrices.MVP *Matrices.MView* world_pos;\n"
					"gl_Position = g_pos;\n";
			}

			while (val != -1)
			{
				remaining_outputs &= ~(1 << val);
				int32_t input = 1 << val;
				val = glm::findLSB(remaining_outputs);

				switch (input)
				{
				case StoneyCore::Renderer::VERT_OUT_FRAG_POSITION:

					if (AllBitsSet(inputs, SHADER_IN_WORLD_TRANSFORM, SHADER_IN_POSITION)) // Regular geometry with transform, just output the transformed vertex;
					{
						out_vertex_shader +=
							"frag_pos = world_pos.xyz;\n";
					}
					break;
				case StoneyCore::Renderer::VERT_OUT_NORMAL:
					if (!(params.texture_flags & (1 << TEX_TYPE_NORM)))
					{
						out_vertex_shader +=
							"frag_normal = vec4(vec4(Normal,1.0)* MTransform).xyz;\n";
					}
					break;

				case StoneyCore::Renderer::VERT_OUT_UV_COORDS:{
					int8_t unique_channels{};
					for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
					{
						//unique_channels.insert(params.uv_channels[channel]);
						if (!((1 << params.uv_channels[channel]) & unique_channels))
						{
							auto in_uv = std::to_string(params.uv_channels[channel]);
							auto uv_channel = Concat("uv_channel", in_uv);
							out_vertex_shader += Concat(
								uv_channel, " = TexCoord", in_uv, ";\n");
						}
						unique_channels |= (1 << (params.uv_channels[channel]));
					}

					if (params.texture_flags & (1<<TEX_TYPE_NORM))
					{
						out_vertex_shader += "::SetTBN((normalize(Normal)));\n";
					}

					break;
				}
				case StoneyCore::Renderer::VERT_OUT_FLAT_CAMERA_POS:
					out_vertex_shader +=
						"vec4 ray_start = inverse(Matrices.MVP * Matrices.MView) *\n"
						"vec4((vec2(0.5, 0.5) * 2) - 1,\n"
						"	-1.0,\n"
						"	1.0f);\n"
						"ray_start /= ray_start.w;\n"
						"cam_pos =ray_start;\n";
					break;
				case StoneyCore::Renderer::VERT_OUT_DRAW_ID:
					out_vertex_shader +=
						"DrawID = gl_DrawID;\n";
					break;
				default:
					break;
				}
			}

			out_vertex_shader += "}\n";
		}
		void GenerateVertexInputs(const StoneyCore::Renderer::ShaderGenParams params, stString& out_vertex)
		{
			auto inputs = params.vertex_inputs;
			auto val = glm::findLSB(inputs);
			while (val != -1)
			{
				inputs &= ~(1 << val);
				int8_t input = 1 << val;
				val = glm::findLSB(inputs);

				switch (input)
				{
				case StoneyCore::Renderer::SHADER_IN_POSITION:
					out_vertex +=Concat(
						"layout(location = ",std::to_string(VERTEX_BINDING_POSITIONS),") in vec3 VertexPosition;\n");
					break;
				case StoneyCore::Renderer::SHADER_IN_NORMAL:
					out_vertex += Concat(
						"layout(location = ", std::to_string(VERTEX_BINDING_NORMALS), ") in vec3 Normal;\n");
					break;
				case StoneyCore::Renderer::SHADER_IN_UV_COORDS: {

					int8_t unique_channels{};
					uint32 location = VERTEX_BINDING_UV_COORDS;
					
					for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
					{
						//unique_channels.insert(params.uv_channels[channel]);
						if (!((1 << params.uv_channels[channel]) & unique_channels))
						{
							auto in_uv = std::to_string(params.uv_channels[channel]);

							out_vertex += Concat(
								"layout (location = ", std::to_string(location), ") in vec2 TexCoord", in_uv, ";\n");

						}
						unique_channels |= (1 << (params.uv_channels[channel]));
						location++;
					}
					break;
				}
				case StoneyCore::Renderer::SHADER_IN_WORLD_TRANSFORM: {
					out_vertex += Concat(
						"layout (location = ",std::to_string(VERTEX_BINDING_TRANSFORM),") in mat4 MTransform;\n");
					break;
				}
				case StoneyCore::Renderer::SHADER_IN_PROJECTION: {
					out_vertex +=
						"layout (binding = 0) uniform _Matrices{\n"
						"mat4 MVP;\n"
						"mat4 MView;\n"
						"} Matrices;\n";
					break;
				}
				case StoneyCore::Renderer::SHADER_IN_TANGENTS: {
					out_vertex += Concat(
						"layout (location = ", std::to_string(VERTEX_BINDING_BITANGENTS), ") in vec3 Bitangent;\n"
						"layout (location = ",std::to_string(VERTEX_BINDING_TANGENTS),") in vec3 Tangent;\n"
					);
					break;
				}
				default:
					break;
				}
			}
		}

		void GenerateVertexOutputs(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_vertex, stString& out_frag)
		{
			auto inputs = params.vertex_outputs;
			auto val = glm::findLSB(inputs);
			while (val != -1)
			{
				inputs &= ~(1 << val);
				int8_t input = 1 << val;
				val = glm::findLSB(inputs);

				switch (input)
				{
				case StoneyCore::Renderer::VERT_OUT_FRAG_POSITION:
					out_vertex += "out vec3 frag_pos;\n";
					out_frag += "in vec3 frag_pos;\n";
					break;
				case StoneyCore::Renderer::VERT_OUT_NORMAL:{
					if (!(params.texture_flags & (1 << TEX_TYPE_NORM)))
					{
						out_vertex += "out vec3 frag_normal;\n";
						out_frag += "in vec3 frag_normal;\n";
					}
					break;
				}
				case StoneyCore::Renderer::VERT_OUT_UV_COORDS: {
					//		stSet<int8_t> unique_channels;
					int8_t unique_channels2{};
					for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
					{
						//unique_channels.insert(params.uv_channels[channel]);
						if (!((1 << params.uv_channels[channel]) & unique_channels2))
						{
							out_vertex += Concat("out vec2 uv_channel", std::to_string(channel), ";\n");
							out_frag += Concat("in vec2 uv_channel", std::to_string(channel), ";\n");
						}
						unique_channels2 |= (1 << (params.uv_channels[channel]));
					}
					if (params.texture_flags & (1 << TEX_TYPE_NORM))
					{
						out_vertex += "out mat3 TBN;";
						out_frag += "in mat3 TBN;";
					}

					break;
				}
				case StoneyCore::Renderer::VERT_OUT_FLAT_CAMERA_POS:
					out_vertex += "out flat vec4 cam_pos;\n";
					out_frag += "in vec4 cam_pos;\n";
					break;
				case StoneyCore::Renderer::VERT_OUT_DRAW_ID:
					out_vertex += "out float DrawID;\n";
					out_frag += "in float DrawID;\n";
					break;
				default:
					break;
				}
			}

			


		}

		void GenerateFragOutputLayout(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag)
		{
			
			auto outputs = params.frag_outputs;
			auto val = glm::findLSB(outputs);
			while (val != -1)
			{
				outputs &= ~(1 << val);
				int8_t output = 1 << val;
				val = glm::findLSB(outputs);

				switch (output)
				{
				case StoneyCore::Renderer::SHADER_OUT_FRAG_COLOR:
					out_frag +=
						"layout(location = 0) out ivec4 OutFragmentColor;\n";
					break;
				case StoneyCore::Renderer::SHADER_OUT_FRAG_NORMAL_AND_DEPTH:
					out_frag +=
						"layout(location = 1) out vec4 OutNormal;\n";
					break;
				default:
					break;
				}
			}
		}
		void GenerateUVFragOutputOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag)
		{
			
			auto outputs = params.texture_flags;
			auto val = glm::findLSB(outputs);
		
			while (val != -1)
			{
				outputs &= ~(1 << val);
				
				auto channel = std::to_string(params.uv_channels[val]);
				switch (val)
				{
				case StoneyCore::TEX_TYPE_DIFFUSE:					
					
					out_frag += Concat(
						"out_channel_1.xyz = texture(diffuse_textures,vec3(uv_channel", channel, ",DrawID)).xyz;\n");
					break;
				case StoneyCore::TEX_TYPE_NORM:
					out_frag += Concat(
						"vec3 normal = texture(normal_textures,vec3(uv_channel", channel, ", DrawID)).xyz;\n",
						"normal.xy = normal.xy*2.0-1.0;\n"
						"OutNormal.xyz = TBN * normalize(normal); \n");
					break;
				case StoneyCore::TEX_TYPE_SPECULAR:
					out_frag += Concat(
						"out_channel_3.xyz = texture(specular_textures, vec3(uv_channel", channel, ", DrawID)).xyz;\n");
					break;
				case StoneyCore::TEX_TYPE_AMBIENT:
					out_frag += Concat(
						"out_channel_2.xyz = texture(ambient_textures, vec3(uv_channel", channel, ", DrawID)).xyz;\n");
					break;
				case StoneyCore::TEX_TYPE_EMISSIVE:
					out_frag += Concat(
						"out_channel_4.xyz = texture(emissive_textures, vec3(uv_channel", channel, ", DrawID)).xyz;\n");
					break;
				case StoneyCore::TEX_TYPE_HEIGHT:
					out_frag += Concat(
						"out_channel_1.w = texture(height_textures,vec3(uv_channel", channel, ",DrawID)).x;\n");
					break;
				case StoneyCore::TEX_TYPE_OPACITY:
				//No support for opacity or lighting just yet
					break;
				case StoneyCore::TEX_TYPE_LIGHTING:
			
					break;
				default:
					break;
				}
				
				val = glm::findLSB(outputs);
			};

			
		}

		void GenerateFragmentColorOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag)
		{
			if (params.vertex_outputs & VERT_OUT_UV_COORDS)
			{

				GenerateUVFragOutputOps(params, out_frag);
				
				
				
			}
			else {
			}
		}

		void GenerateFragmentOutputOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag)
		{

			auto outputs = params.frag_outputs;
			auto val = glm::findLSB(outputs);
			while (val != -1)
			{
				outputs &= ~(1 << val);
				int8_t output = 1 << val;
				val = glm::findLSB(outputs);

				switch (output)
				{
				case StoneyCore::Renderer::SHADER_OUT_FRAG_COLOR:
					GenerateFragmentColorOps(params, out_frag);




					break;
				case StoneyCore::Renderer::SHADER_OUT_FRAG_NORMAL_AND_DEPTH:

					if (AllBitsSet(params.vertex_outputs, VERT_OUT_FLAT_CAMERA_POS, VERT_OUT_NORMAL, VERT_OUT_FRAG_POSITION))
					{
						if (!(params.texture_flags & (1<<TEX_TYPE_NORM)))
						{
							out_frag +=
								"OutNormal.xyz = frag_normal;\n";
						}
						else {

						}

						out_frag +=
							"OutNormal.w = distance (cam_pos.xyz,frag_pos);\n";
					}
					else {
						LogErrorToFile("Failed to generate shader. Deferred fragment normal and depth output requires 'VERT_OUT_FLAT_CAMERA_POS | VERT_OUT_NORMAL | VERT_OUT_FRAG_POSITION' from vertex texture_flags");
					}
					break;
				default:
					break;
				}
			}
		}

		void GenerateFragmentTextureInputs(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag)
		{
			auto texture_flags = params.texture_flags;
			int val = glm::findLSB(texture_flags);
			while (val != -1)
			{
				texture_flags &= ~(1 << val);
				

				switch (val)
				{
				case StoneyCore::TEX_TYPE_DIFFUSE:
					out_frag += TEX_UNIT_DIFFUSE;
					break;
				case StoneyCore::TEX_TYPE_NORM:
					out_frag += TEX_UNIT_NORMALS;
					break;
				case StoneyCore::TEX_TYPE_SPECULAR:
					out_frag += TEX_UNIT_SPECULAR;
					break;
				case StoneyCore::TEX_TYPE_AMBIENT:
					out_frag += TEX_UNIT_AMBIENT;
					break;
				case StoneyCore::TEX_TYPE_EMISSIVE:
					out_frag += TEX_UNIT_EMISSIVE;
					break;
				case StoneyCore::TEX_TYPE_HEIGHT:
					out_frag += TEX_UNIT_HEIGHT;
					break;
				case StoneyCore::TEX_TYPE_OPACITY:
					out_frag += TEX_UNIT_OPACITY;
					break;
				case StoneyCore::TEX_TYPE_LIGHTING:
					out_frag += TEX_UNIT_LIGHTING;
					break;
				default:
					break;
				}

				val = glm::findLSB(texture_flags);
			}
		}

		bool ShaderGenerator::GenerateShader(const ShaderGenParams params, stString& out_vertex, stString& out_frag) {
			out_vertex = RGL_VERSION_STRING;
			out_frag = RGL_VERSION_STRING;
			GenerateVertexInputs(params, out_vertex);
			GenerateFragOutputLayout(params, out_frag);

			GenerateVertexOutputs(params, out_vertex, out_frag);
			GenerateVetexOutputOps(params, out_vertex);

			GenerateFragmentTextureInputs(params, out_frag);
			out_frag +=
				"int PackVec4(vec4 in_vec) {\n"
				"in_vec *= 255.0;\n"
				"int retx = (int(in_vec.r) | (int(in_vec.g) << 8) | (int(in_vec.b) << 16) | (int(in_vec.a) << 24));\n"
				"return  retx;\n"
				"}\n";
			if (params.texture_flags & (1<<TEX_TYPE_NORM))
			{
				out_frag  ;
					
			}

			out_frag +=
				"void main (){\n";
			
			out_frag +=
				"vec4 out_channel_1 = vec4(0,0,0,1);\n"
				"vec4 out_channel_2	= vec4(0,0,0,1);\n"
				"vec4 out_channel_3	= vec4(0,0,0,1);\n"
				"vec4 out_channel_4	= vec4(0,0,0,1);\n";
			
			GenerateFragmentOutputOps(params, out_frag);
			
			out_frag +=
				"OutFragmentColor =	ivec4(::PackVec4(out_channel_1),::PackVec4(out_channel_2),::PackVec4(out_channel_3),::PackVec4(out_channel_4));\n"
				"}\n";



			return false;
		}

		void ShaderGenerator::GenerateDeferredShaderParams(const StoneyCore::ModelAsset& model, stVector<ShaderGenParams>& out_params) {
			if (!model.vertices.size() ||
				model.vertices.size() != model.meshes.size())
			{
				LogErrorToFile("Tried to generate shader for invalid model. See ShaderGenerator.cpp.");
				return;
			}
			out_params.clear();
			out_params.resize(model.vertices.size());
			for (size_t mesh_index = 0; mesh_index < model.vertices.size(); mesh_index++)
			{
				auto& mesh_info = model.meshes[mesh_index];

				auto& mesh_verts = model.vertices[mesh_index];
				auto& out_param = out_params[mesh_index];
				auto& vert_inputs = out_param.vertex_inputs;

				out_param.gen_type = SHADER_GEN_Deffered_Geometry;
				out_param.texture_flags = mesh_info.mesh_info.texture_flags;
				out_param.frag_outputs |= (SHADER_OUT_FRAG_COLOR | SHADER_OUT_FRAG_NORMAL_AND_DEPTH);
				out_param.vertex_outputs |= (VERT_OUT_FLAT_CAMERA_POS);
				bool has_vertices = mesh_verts.vertices.size();
				bool has_normals = mesh_verts.normals.size();
				bool has_uv_coords{ false };
				bool has_texture = mesh_info.mesh_info.uses_texture;
				bool has_tangents = mesh_verts.tangents.size() && (mesh_verts.bitangents.size() == mesh_verts.tangents.size());
				if (has_vertices)
				{
					vert_inputs |= SHADER_IN_POSITION;
					if (has_normals)
					{
						vert_inputs |= (SHADER_IN_NORMAL | SHADER_IN_WORLD_TRANSFORM | SHADER_IN_PROJECTION);
					}
				}

				for (uint32 channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
				{
					if (mesh_verts.texcoords[channel].size())
					{
						const auto in_channel = mesh_verts.uv_channel_indices[channel];

						out_param.uv_channels[channel] = in_channel;
						has_uv_coords = true;
					}
				}
				
				vert_inputs |= (SHADER_IN_UV_COORDS * has_uv_coords);
				vert_inputs |= (SHADER_IN_TANGENTS * has_tangents);

				auto& vert_outputs = out_param.vertex_outputs;

				vert_outputs |= (VERT_OUT_FRAG_POSITION * has_vertices);
				vert_outputs |= (VERT_OUT_NORMAL * has_normals);
				vert_outputs |= (VERT_OUT_UV_COORDS * has_uv_coords);


				vert_outputs |= (VERT_OUT_COLOR * (!has_texture));
				vert_outputs |= (VERT_OUT_DRAW_ID * has_texture);
			}
		}
	}
}