#pragma once
#include "pch.h"

namespace StoneyCore {
	struct ShaderUniformDescriptor {
		GLint num_blocks{ 0 };
		stVector<std::string> uniform_names;
	};
}