#pragma once
#ifndef SP_USE_EDITOR

#include <unordered_map>

#include "pch.h"
#include "StPxWorld.h"
#include "Transform.h"
#include "AssetHandler.h"
#include "TaskScheduler.h"
#include "RenderBatch.h"
#include "BatchRenderer3D.h"

namespace StoneyCore {
	using namespace StoneyCore::Renderer;
	class SceneHandler
	{
	public:
		SceneHandler(fu2::function<void(void)> exit_callback = DefaultExitCB);
		ScreenSettings m_screen_settings;
		fu2::function<void(void)> m_exit_callback;
		StPxPhysics::StPxWorld* m_physics_world{ nullptr };
		DataRegistrar<> data_reg;
		TaskScheduler task_sched;
		BigBitField<8192> prefab_ids;
		BigBitField<8192> batch_ids;
		std::set<uint32_t> pending_prefab_ids;
		std::map<std::string, uint32> m_prefab_map;
		std::map<std::string, uint32> m_model_map;
		std::map<uint32, uint32> m_physics_instance_map;
		std::map<uint32_t, PrefabHandle> m_batch_map; // key is prefab id
		std::map<uint32_t, InstanceHandle> m_instance_map;
		std::map<uint32, uint32> m_model_phys_handle_map;
		stVector<ModelData> m_models;
		stVector<PrefabDescription> m_prefab_descriptions;
		Renderer::RenderInitializer m_renderer_handle;
		Renderer::RendererConstArgs& m_queues = m_renderer_handle.m_queues;

		std::map<std::string, TextureInfo> m_texture_infos;

		ShaderTracker m_shader_tracker;
		std::map<std::string, Texture> textures;

		std::atomic<bool> is_pending{ false };

		BatchRenderer3D* m_batch_renderer{ nullptr };
		stVector<RenderBatch2*> batches;
		stVector<RenderBatch2*> pending_batches;
		std::set<RenderBatch2*> submitted_batches;
		std::set<RenderBatch2*> free_batches;
		BigBitField<8192> instance_ids;
		uint32_t m_next_instance_id{ 1 };
		uint32_t next_version{ 0 };
		uint32_t next_shader_id{ 0 };

		SceneGraph m_graph;
		std::chrono::high_resolution_clock::time_point last;
		Camera main_camera;
		AssetHandler asset_handler;
		bool trap_mouse{ false };
		float delta{ 0 };
		float physics_step_helper{ 0 };
		void BeginUpdatePhysicsScene(float delta) {
			m_physics_world->BeginUpdateScene(delta);
		}
		void GetPhysicsResults() {
			m_physics_world->GetPhysicsResults();
		}

	public:
		float SetDelta() {
			using namespace std;
			auto&& now = std::chrono::high_resolution_clock::now();

			delta = std::chrono::duration<float, std::milli>(now - last).count();
			last = now;
			return delta;
		}
		void GetDelta(float& output) {
			output = delta;
		}
		void DequeueFreeBatches() {
			auto start = free_batches.end();
			RenderBatch2* deque_temp[64] = { nullptr };
			if (auto num_batches = m_queues.free_batch_queue->try_dequeue_bulk(deque_temp, 64))
			{
				free_batches.insert(deque_temp, &deque_temp[num_batches - 1]);
			}
		}
		void HandleRenderWindowResize(int x, int y) {
			m_screen_settings = { x,y };
			main_camera.m_aspect = (float(x) / float(y));
			RendererTask task = { [x,y]() {
				glViewport(0,0,x,y);
			},0 };
			m_queues.render_queue->enqueue(task);
		}
		void WaitForFreeBatch(RenderBatch2* batch) {
			if (free_batches.find(batch) != free_batches.end())
			{
				return;
			}
			else {
				RenderBatch2* deque_temp[64] = { nullptr };
				while (free_batches.find(batch) == free_batches.end())
				{
					while (auto num_batches = m_queues.free_batch_queue->try_dequeue_bulk(deque_temp, 64)) {
						free_batches.insert(deque_temp, &deque_temp[num_batches]);
					}
				}
			}
		}
		void HandleEvents();
		void SetTransform(const InstanceHandle& instance_handle, const Transform& transform);

		void WriteSceneToRenderBuffer(StoneyCore::Renderer::RenderBuffer* buffer);

		void UpdateRendererScene();

		InstanceHandle AddInstance(uint32_t prefab_id, const Transform transform);
		uint32_t AddPrefab(PrefabDescription& prefab);

		~SceneHandler() {
			if (m_batch_renderer)
			{
				m_batch_renderer->Shutdown();
			}
			for (auto& tex : textures) {
				asset_handler.FreeTexture(tex.second);
			}

			CleanupPhysics();
			data_reg.ClearAll();
		}
		void LoadModelToBatch(RenderBatch2* batch, const PrefabDescription& prefab_description);
		void LoadTextureToBatch(RenderBatch2* batch, const PrefabDescription& prefab_description);
		void LoadShaderTextToBatch(Renderer::RenderBatch2* batch, const PrefabDescription& prefab)
		{
			asset_handler.LoadTextFileToString(batch->vertex_shader, prefab.render_info.shader_stages.vs_filename);
			asset_handler.LoadTextFileToString(batch->frag_shader, prefab.render_info.shader_stages.fs_filename);
		}
		void AddPrefabToBatch(Renderer::RenderBatch2* batch, const PrefabDescription& prefab)
		{
			LoadModelToBatch(batch, prefab);

			if (batch->render_spec.use_texture)
			{
				LoadTextureToBatch(batch, prefab);
			}

			if (batch->render_spec.uses_material) {
				batch->materials.emplace_back(prefab.material);
			}
		}
		void RemovePrefabFromBatch(Renderer::RenderBatch2* batch, const PrefabDescription& prefab) {
			auto renderable_index = m_batch_map[m_prefab_map[prefab.name]].renderable_index;
			batch->total_verts -= batch->model_infos[renderable_index].num_verts;
			batch->model_infos.erase(batch->model_infos.begin() + renderable_index);
			batch->model_datas.erase(batch->model_datas.begin() + renderable_index);
			batch->textures.erase(batch->textures.begin() + renderable_index);

			if (batch->materials.size())
			{
				batch->materials.erase(batch->materials.begin() + renderable_index);
			}
		}

		void UpdateBatchFromPrefab(Renderer::RenderBatch2* batch, const PrefabDescription& prefab) {
		}
		void InitBatchFromPrefab(RenderBatch2* batch, const PrefabDescription& prefab)
		{
			batch->shader_id = m_shader_tracker.GetProgramID(
				prefab.render_info.shader_stages.vs_filename,
				prefab.render_info.shader_stages.fs_filename
			);

			if (prefab.render_spec.use_texture)
			{
				batch->texture_info = m_texture_infos[prefab.texture_filename];
			}

			batch->render_spec = prefab.render_spec;
			LoadShaderTextToBatch(batch, prefab);
			AddPrefabToBatch(batch, prefab);
		}
		void InitRenderer(RendererInitInfo init_info) {
			m_screen_settings.width = init_info.window_width;
			m_screen_settings.height = init_info.window_height;
			if (m_batch_renderer)
			{
				m_batch_renderer->Init(init_info);
			}
			m_queues.render_buffer_queue->enqueue_bulk(m_renderer_handle.buffer_ptok, m_renderer_handle.m_render_buffers, 3);
		}

		bool InitPhysics() {
			if (m_physics_world)
			{
				m_physics_world->InitWorld();
				return true;
			}
			return false;
		}
		void CleanupPhysics() {
			if (m_physics_world)
			{
				m_physics_world->CleanupWorld();
			}
		}
		void StartRenderer() {
			if (m_batch_renderer)
			{
				m_batch_renderer->Start();
			}
		}
		void ShutdownRenderer() {
			m_batch_renderer->Shutdown();
		}
		void SubmitPendingBatches() {
			if (m_batch_renderer && m_queues.batch_queue)
			{
				m_queues.batch_queue->enqueue_bulk(m_renderer_handle.batch_ptok, pending_batches.begin(), pending_batches.size());
				pending_batches.clear();
			}
		}

		void PrepareRenderBuffer(RenderBuffer* buffer) {
			main_camera.SetProjectionMat(buffer->camera_mvp);
		}
		bool SortBatches();

		bool IsBatchCompatible(RenderBatch2* batch, const PrefabDescription& description);

		RenderBatch2* AddPrefabToBatchSystem(const uint32_t& prefab_id);

		bool VerifyBatches() {
			for (auto batch : batches) {
				if (!batch) {
					WarnMessage("Failed to verify all batches!");
					return false;
				}
			}
			return true;
		}
	};
}
#endif // !SP_USE_EDITOR