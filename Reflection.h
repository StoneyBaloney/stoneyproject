#pragma once

#include <stdint.h>
#include <type_traits>
#include <string_view>
#include <magic_enum.hpp>
#include <refl.hpp>
#include <GL/glew.h>
#ifdef SP_USE_EDITOR
#include <wx/propgrid/manager.h>
#include <wx/propgrid/propgrid.h>
#endif // SP_USE_EDITOR
#include "Errors.h"
#include "EditorTypes.h"
struct StBaseComponent {
};

template <typename DataType>
void NoPropertySpec() {
	static_assert(0);
}
#ifdef SP_USE_EDITOR
enum class StwxEDITOR_PROPERTY_TYPE {
	Integer,
	Floating,
	Bool,
	String,
	Enum,
	Complex,
	Unspecified
};
template <typename...Ts>
struct TypeTest {
	template <typename TestType>
	static constexpr bool HasType() {
		return (std::is_same_v<Ts, TestType> || ...);
	}
};

template <typename TestType>
constexpr auto GetPropertyType() {
	if constexpr (std::is_enum<TestType>::value) {
		return StwxEDITOR_PROPERTY_TYPE::Enum;
	}
	else if constexpr (TypeTest<
		float,
		double,
		GLfloat
	>::template HasType<TestType>()) {
		return StwxEDITOR_PROPERTY_TYPE::Floating;
	}
	else if constexpr (TypeTest<
		int,
		unsigned,
		long,
		long long,
		long long,
		GLint,
		uint32_t,
		GLuint,
		uint64_t,
		size_t
	>::template HasType<TestType>()) {
		return StwxEDITOR_PROPERTY_TYPE::Integer;
	}
	else if constexpr (TypeTest<
		bool,
		GLboolean
	>::template HasType<TestType>()) {
		return StwxEDITOR_PROPERTY_TYPE::Bool;
	}
	else if constexpr (TypeTest<
		std::string,
		const char*,
		std::string_view
	>::template HasType<TestType>()) {
		return StwxEDITOR_PROPERTY_TYPE::String;
	}
	else if constexpr (std::is_class<TestType>::value) {
		return StwxEDITOR_PROPERTY_TYPE::Complex;
	}
	else {
		return StwxEDITOR_PROPERTY_TYPE::Unspecified;
	}
}

template<typename DataType>
wxPGProperty* GenerateEnumProperty(wxString& name, DataType& data)
{
	auto names = magic_enum::enum_names< DataType>();
	auto values = magic_enum::enum_values<DataType>();

	wxArrayString name_arr;
	for (auto& name : names) {
		name_arr.Add(wxString(std::string(name)));
	}
	wxArrayInt value_arr;
	for (auto& value : values) {
		value_arr.Add(int(value));
	}
	wxPGChoices choices(name_arr, value_arr);
	auto ret = new wxEnumProperty(name, name, choices);
	ret->SetChoiceSelection(static_cast<int>(data));
	return ret;
}
template <typename DataType>
inline void SetPropsComplexImpl(StPropertyPage* page, DataType& data, wxString& name, wxString& prefab_name, wxPGProperty* parent = nullptr) {
	for_each(refl::reflect(data).members, [&](auto member) {
		wxString name = member.name.str();

		SetPropsImpl(page, member.get(data), name, prefab_name, parent);
	});
}
template <typename DataType>
inline void SetPropsImpl(StPropertyPage* page, DataType& data, wxString& name, wxString& prefab_name, wxPGProperty* parent = nullptr) {
	using  Prop = StwxEDITOR_PROPERTY_TYPE;
	constexpr auto type = GetPropertyType<DataType>();

	wxPGProperty* prop{ nullptr };
	if (parent)
	{
		prop = parent->GetPropertyByName(name);
	}
	else {
		prop = page->GetPropertyByName(prefab_name)->GetPropertyByName(name);
		if (!prop)
		{
			auto err = std::string("Failed to load property for type ");
			err += name;
			ThrowErrorContinue(err);
		}
	}
	if constexpr (type == Prop::Enum || type == Prop::Integer) {
		data = static_cast<DataType>(prop->GetValue().GetInteger());
	}
	else if constexpr (type == Prop::Floating) {
		data = static_cast<DataType>(prop->GetValue().GetDouble());
	}

	else if constexpr (type == Prop::Bool) {
		data = static_cast<DataType>(prop->GetValue().GetBool());
	}
	else if constexpr (type == Prop::String) {
		data = static_cast<DataType>(prop->GetValue().GetString());
	}
	else if constexpr (type == Prop::Complex) {
		SetPropsComplexImpl(page, data, name, prefab_name, prop);
	}
	else {
		static_assert(0, "Primitive type without a specialization. See 'GetPropertyType.' in reflection. Or add a template specialization 'StwxEDITOR_PROPERTY_TYPE GetPropertyType<T>()");
	}
}
template <typename DataType>
inline auto ExposeComplexInEditor(StPropertyPage* page, DataType& data, wxString& name) {
	auto complex_prop = new wxStringProperty(name, name);

	page->SetPropertyReadOnly(complex_prop);

	for_each(refl::reflect(data).members, [&](auto member) {
		wxString m_name = member.name.str();
		wxPGProperty* prop = ExposeInEditorImpl(page, member.get(data), m_name);

		complex_prop->AppendChild(prop);
	});
	return complex_prop;
}
template <typename DataType>
inline wxPGProperty* ExposeInEditorImpl(StPropertyPage* page, DataType& data, wxString& name) {
	using  Prop = StwxEDITOR_PROPERTY_TYPE;
	constexpr auto type = GetPropertyType<DataType>();
	if constexpr (type == Prop::Enum) {
		return GenerateEnumProperty(name, data);
	}
	else if constexpr (type == Prop::Floating) {
		return new wxFloatProperty(name, name, data);
	}
	else if constexpr (type == Prop::Integer) {
		return new wxIntProperty(name, name, data);
	}
	else if constexpr (type == Prop::Bool) {
		return new wxBoolProperty(name, name, data);
	}
	else if constexpr (type == Prop::String) {
		return new wxStringProperty(name, name, wxString(std::string(data)));
	}
	else if constexpr (type == Prop::Complex) {
		return ExposeComplexInEditor(page, data, name);
	}
	else {
		static_assert(0, "Primitive type without a specialization. See 'GetPropertyType.' in reflection. Or add a template specialization 'StwxEDITOR_PROPERTY_TYPE GetPropertyType<T>()");
	}
}

template <typename ComponentType>
void StGenerateProperties(StPropertyPage* page, ComponentType& comp) {
	wxString parent_name = refl::reflect(comp).name.str();
	wxPGProperty* comp_prop = new wxStringProperty(parent_name, parent_name);

	for_each(refl::reflect(comp).members, [&](auto member) {
		wxString name = member.name.str();
		wxPGProperty* prop = ExposeInEditorImpl(page, member.get(comp), name);

		comp_prop->AppendChild(prop);
	});
	page->Append(comp_prop);
}

template <typename ComponentType>
void SetPropertiesImpl(StPropertyPage* page, ComponentType& comp) {
	wxString prefab_name = refl::reflect(comp).name.str();

	for_each(refl::reflect(comp).members, [&](auto member) {
		wxString name = member.name.str();
		SetPropsImpl(page, member.get(comp), name, prefab_name);
	});
}

template <typename ComponentType, typename BaseType>
void GetBasePropertiesImpl(StPropertyPage* page, ComponentType* comp) {
	auto base_ptr = static_cast<BaseType*>(comp);
	StGenerateProperties(page, *base_ptr);
}
template <typename ComponentType, typename FirstBase, typename Next, typename...Remaining>
void GetBasePropertiesImpl(StPropertyPage* page, ComponentType* comp) {
	GetBasePropertiesImpl<ComponentType, FirstBase>(page, comp);
	GetBasePropertiesImpl<ComponentType, Next, Remaining...>(page, comp);
}

template <typename ComponentType>
struct BaseHelper {
	template <typename...Bases>
	static void StGetBaseProperties(StPropertyPage* page, ComponentType* comp) {
		GetBasePropertiesImpl<ComponentType, Bases...>(page, comp);
	}
};

//template <typename...>
//void GetBaseProperties(...) {
//
//
//}

#endif // SP_USE_EDITOR
