#pragma once
#include <entt/entity/registry.hpp>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Defines.h"
#include "SEngineTypes.h"
namespace StoneyCore {
	struct DirectionalLightProperties {
		glm::vec3 ambient{ 0 };
		glm::vec3 direction{ 1 };
		glm::vec3 diffuse{ 1 };
		glm::vec3 specular{ 0 };
	};
#pragma pack(push, 1)
	struct PointLightProperties {
		glm::vec3 ambient{ 0 };
		float padding1;
		glm::vec3 position{ 1 };
		float padding2;
		glm::vec3 diffuse{ 1 };
		float padding3;
		glm::vec3 specular{ 0 };
		float padding4;
		//constant = m_point_light.x;
		//linear = m_point_light.y;
		//quadratic = m_point_light.z;
		glm::vec3 m_point_light{ 0 };
	};
#pragma pack(pop)
	const auto sizeofpointlight = sizeof(PointLightProperties);

	enum class LightType
	{
		Directional,
		Point,
		Spot
	};

	struct LightPropertiesBuffer {
		stVector<PointLightProperties> point_lights;
		stVector<DirectionalLightProperties> dir_lights;
		uint32 version{ 0 };
	};
}