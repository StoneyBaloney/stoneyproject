#version 460
layout(location = 0)in vec3 VertexPosition;
layout(location = 1)in vec3 WidgetColor; 
layout (binding = 0) uniform _Matrices{
mat4 MVP;
mat4 MView;
} Matrices;
layout (std140,binding = 4) uniform _Editor{
mat4 MTransform;
} EditorData;
out vec3 FragColor;
void main(){    
    gl_Position = Matrices.MVP * Matrices.MView*EditorData.MTransform* vec4(VertexPosition,1.0);
    FragColor = WidgetColor;   
}