#version 460
int PackVec4(vec4 in_vec) {
	in_vec *= 255.0;
	int retx = (int(in_vec.r) | (int(in_vec.g) << 8) | (int(in_vec.b) << 16) | (int(in_vec.a) << 24));
	return  retx;
};
in vec3 FragColor;
out ivec4 FragmentColor;
void main(){FragmentColor =  ivec4(FragColor,1.0);}