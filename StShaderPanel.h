#pragma once
#ifdef SP_USE_EDITOR

#include "ShaderPanelBase.h"
namespace StoneyCore {
	namespace Editor { class EditorSceneHandler; class EditorSceneManager; }
}
class StShaderPanel :
	public ShaderPanelBase
{
public:
	friend class StoneyCore::Editor::EditorSceneHandler;
	friend class StoneyCore::Editor::EditorSceneManager;
	StShaderPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(605, 512), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~StShaderPanel();
};

#endif // SP_USE_EDITOR
