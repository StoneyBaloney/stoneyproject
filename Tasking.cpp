#include "pch.h"
#include "Tasking.h"
#include <unordered_set>
struct SortHandle {

	uint32_t array_index;
	uint32_t object_index;


};
using SortSet = std::vector<SortHandle>;













void PipelineManager::GenerateTaskQueue(std::vector<std::set<size_t>> & mapped_stages, std::vector<PipelineStage> & main_stages)
{
	for (auto& stage : mapped_stages) {
		main_stages.emplace_back();
		auto& tasks = main_stages.back().tasks;
		for (auto task_index : stage) {

			tasks.emplace_back(system_funcs[task_index]);


		}


	}
}







void GetConflicts(TaskIDField &current_stage_tasks, DataIDField &current_stage_conflicts, std::vector<SystemMeta> & g_infos)
{
	for (auto task : current_stage_tasks) {

		auto ind = task - 1;
		
		current_stage_conflicts &= g_infos[ind].m_writable_data_ids;


	}
}




void SortPipelines(std::vector<TaskIDField> &_priority_lists, std::vector<SystemMeta> & g_infos) {
	std::vector<DAGNode> dag_vec;
	
	TaskIDField current_stage;
	size_t task_index = 1;
	_priority_lists.clear();
	_priority_lists.reserve(MAX_TASKS);
	for (auto& task : g_infos) {
		if (task.dependencies) {

			current_stage.SetBit(task_index);

		}
		task_index++;

	}
	for (auto task : current_stage) {
		



	}

}


void PipelineManager::PrepareTasks()
{
	
	m_task_graph.Clear();
	if (!m_task_graph.GenDag(system_infos)) { 

		return; 


	}
	m_info.main_per_frame_stages.clear();
	m_info.relaxed_per_frame_stages.clear();
	
	auto sort_graph = [&](DAG::GraphType& input) {
		
		for (auto& phase : input) {

			m_info.main_per_frame_stages.emplace_back();
			m_info.relaxed_per_frame_stages.emplace_back();

			auto& main_stage = m_info.main_per_frame_stages.back();
			auto& relaxed_stage = m_info.relaxed_per_frame_stages.back();

			

			for (auto& _task_index : phase.vertices) {

				auto index = _task_index - 1;
				
				auto& info = system_infos[index];
				auto& task = system_funcs[index];

				switch (info.thread_affinity)
				{
				case SGS_THREAD_AFFINITY::MainOnly: {
					main_stage.tasks.emplace_back(task);
					break; }
				case SGS_THREAD_AFFINITY::Relaxed:
				{
					relaxed_stage.tasks.emplace_back(task);
					break;}				
				default:
					break;
				}
			


			}
		}
	
	
	
	};
	m_task_graph.Print(sort_graph);
	


}

void STY_ThrowError(const char * error) {

	throw(error);

}

void PipelineErrorMsg(const char * error) {	std::cout << error << "\n"; }
static std::chrono::high_resolution_clock::time_point last;
void GetDelta(double & delta_time) {

	auto&& now = std::chrono::high_resolution_clock::now();
	
	delta_time = double(std::chrono::duration_cast<std::chrono::microseconds>(now - last).count() * 0.001);
	last = now;

}





void DAG::Clear()
{

	m_graph.clear();
	contained_tasks.Clear();
	start = 0;
	complete = false;


}

bool DAG::GenDag(const std::vector<SystemMeta>& g_infos)
{
	if (complete) {
		return true;

	}
	complete = false;

	m_graph.clear();
	m_graph.reserve(100);
	m_graph.emplace_back();

	TaskIDField remaining_indices;
	TaskIDField pushed_ids;

	if (!initGraph(g_infos, pushed_ids, remaining_indices)) {
		return false;

	}

	uint32_t next_id = 2;
	bool satisfied_deps{ false };

	while (!satisfied_deps) {

		m_graph.emplace_back() = { getNextTasks(g_infos, remaining_indices, pushed_ids),next_id };
		satisfied_deps = m_graph.back().vertices ? false : true;
		next_id++;

	}
	m_graph.pop_back();

	if (remaining_indices)
	{
		TaskIDField failed_ids;
		for (auto& val : remaining_indices) {
			failed_ids |= g_infos[val - 1].m_id;


		}
		for (auto& val : remaining_indices) {

			if (!(g_infos[val - 1].dependencies & pushed_ids)) {



				return false;


			}

		}
		return false;
	}
	complete = true;
	return true;
	
}



bool DAG::initGraph(const std::vector<SystemMeta>& g_infos, TaskIDField & pushed_ids, TaskIDField & remaining_indices)
{
	m_graph[0].m_id = 1;
	size_t task_index = 1;
	DataIDField write_ids;
	DataIDField read_ids;
	for (auto& task : g_infos) {
		if (task.m_id < 1)  break;
		if (!task.dependencies)
		{
			if ((!((read_ids.AnyBitsSet(task.m_writable_data_ids)))) && (!(write_ids.AnyBitsSet(task.m_readable_data_ids,task.m_writable_data_ids)))) {
				write_ids |= task.m_writable_data_ids;
				read_ids |= task.m_readable_data_ids;
				contained_tasks.SetBit(task.m_id);
				m_graph[0].vertices.SetBit(task_index);
				pushed_ids.SetBit(task.m_id);

			}
			else {

				remaining_indices.SetBit(task_index);
			}
		}
		else {

			remaining_indices.SetBit(task_index);

		}
		task_index++;
	}
	if (!m_graph[0].vertices) {

		return false;

	}
	return true;
}

TaskIDField DAG::getNextTasks(const std::vector<SystemMeta>& g_infos, TaskIDField & _remaining, TaskIDField & pushed_ids)
{
	TaskIDField next;
	
	DataIDField write_ids;
	DataIDField read_ids;
	for (auto& task_index : _remaining)
	{

		auto& current_task = g_infos[task_index - 1];
		
		if (pushed_ids.AllBitsSet(current_task.dependencies)) {
			if ((!(read_ids.AnyBitsSet(current_task.m_writable_data_ids))) && (!write_ids.AnyBitsSet(current_task.m_readable_data_ids, current_task.m_writable_data_ids))) {
				write_ids |= current_task.m_writable_data_ids;
				read_ids |= current_task.m_readable_data_ids;
				next.SetBit(task_index);
				contained_tasks.SetBit(current_task.m_id);
				_remaining.UnsetBit(task_index);
			}

		}

	}

	for (auto& index : next) {

		pushed_ids.SetBit(g_infos[index - 1].m_id);
	}

	return next;

}
