#ifndef _WIN32
#include "LinuxEntry.h"
#include "StoneyMain.h"

int main(int argc, char* argv[])
{
	StoneyMain::Start();

	return 0;
}

#endif