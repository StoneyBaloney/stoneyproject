#ifdef SP_USE_EDITOR

#include "SceneTreePanelBase.h"
SceneTreePanelBase::SceneTreePanelBase(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer(wxVERTICAL);

	m_scene_tree = new wxDataViewTreeCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
	bSizer14->Add(m_scene_tree, 1, wxALL | wxEXPAND, 5);

	bSizer13->Add(bSizer14, 1, wxEXPAND, 0);

	this->SetSizer(bSizer13);
	this->Layout();
}

SceneTreePanelBase::~SceneTreePanelBase()
{
}
#endif // SP_USE_EDITOR