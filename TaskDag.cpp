#include "pch.h"
#include "TaskDag.h"

void DAG::Clear()
{
	m_graph.clear();
	contained_tasks.Clear();
	start = 0;
	complete = false;
}

bool DAG::GenDag(const stVector<TaskInfo>& g_infos)
{
	if (complete) {
		return true;
	}
	complete = false;

	m_graph.clear();
	m_graph.reserve(100);
	m_graph.emplace_back();

	TaskIDField remaining_indices;
	TaskIDField pushed_ids;

	if (!initGraph(g_infos, pushed_ids, remaining_indices)) {
		return false;
	}

	uint32_t next_id = 2;
	bool satisfied_deps{ false };

	while (!satisfied_deps) {
		m_graph.emplace_back() = { getNextTasks(g_infos, remaining_indices, pushed_ids),next_id };
		satisfied_deps = m_graph.back().vertices ? false : true;
		next_id++;
	}
	m_graph.pop_back();

	if (remaining_indices)
	{
		TaskIDField failed_ids;
		for (auto& val : remaining_indices) {
			failed_ids |= g_infos[val - 1].m_id;
		}
		for (auto& val : remaining_indices) {
			if (!(g_infos[val - 1].dependencies & pushed_ids)) {
				return false;
			}
		}
		return false;
	}
	complete = true;
	return true;
}

bool DAG::initGraph(const stVector<TaskInfo>& g_infos, TaskIDField& pushed_ids, TaskIDField& remaining_indices)
{
	m_graph[0].m_id = 1;
	size_t task_index = 1;
	DataIDField write_ids;
	DataIDField read_ids;
	for (auto& task : g_infos) {
		if (task.m_id < 1)  break;
		if (!task.dependencies)
		{
			if ((!((read_ids.AnyBitsSet(task.m_writable_data_ids)))) && (!(write_ids.AnyBitsSet(task.m_readable_data_ids, task.m_writable_data_ids)))) {
				write_ids |= task.m_writable_data_ids;
				read_ids |= task.m_readable_data_ids;
				contained_tasks.SetBit(task.m_id);
				m_graph[0].vertices.SetBit(task_index);
				pushed_ids.SetBit(task.m_id);
			}
			else {
				remaining_indices.SetBit(task_index);
			}
		}
		else {
			remaining_indices.SetBit(task_index);
		}
		task_index++;
	}
	if (!m_graph[0].vertices) {
		return false;
	}
	return true;
}

TaskIDField DAG::getNextTasks(const stVector<TaskInfo>& g_infos, TaskIDField& _remaining, TaskIDField& pushed_ids)
{
	TaskIDField next;

	DataIDField write_ids;
	DataIDField read_ids;
	for (auto& task_index : _remaining)
	{
		auto& current_task = g_infos[task_index - 1];

		if (pushed_ids.AllBitsSet(current_task.dependencies)) {
			if ((!(read_ids.AnyBitsSet(current_task.m_writable_data_ids))) && (!write_ids.AnyBitsSet(current_task.m_readable_data_ids, current_task.m_writable_data_ids))) {
				write_ids |= current_task.m_writable_data_ids;
				read_ids |= current_task.m_readable_data_ids;
				next.SetBit(task_index);
				contained_tasks.SetBit(current_task.m_id);
				_remaining.UnsetBit(task_index);
			}
		}
	}

	for (auto& index : next) {
		pushed_ids.SetBit(g_infos[index - 1].m_id);
	}

	return next;
}