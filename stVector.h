#pragma once
#include <vector>
namespace StoneyCore {
	template <typename...Params>
	class stVectorWrapper : public std::vector<Params...>
	{
	public:
		template <typename...Args>
		stVectorWrapper(Args& ...args) : std::vector<Params...>(args...) {};
	
		
	};
	template <typename...Params>
	void Append(const stVectorWrapper<Params...>& input, stVectorWrapper<Params...>& target) {
		target.insert(target.begin(), input.begin(), input.end());
	}
}