#include "pch.h"
#include "TaskScheduler.h"

bool TaskScheduler::SortGraph() {
	return m_graph.GenDag(m_infos);
}

void TaskScheduler::GenPipelines() {
	std::lock_guard lock_pipeline(pipeline_lock);

	size_t stage = 0;
	for (const auto& phase : m_graph.Get()) {
		relaxed_tasks.emplace_back();
		main_tasks.emplace_back();

		for (const auto& task_index : phase.vertices) {
			auto index = task_index - 1;
			auto& info = m_infos[index];
			switch (info.thread_affinity)
			{
			case SGS_THREAD_AFFINITY::MainOnly: {
				main_tasks[stage].emplace_back(info.func);
				break;
			}
			case SGS_THREAD_AFFINITY::Relaxed: {
				relaxed_tasks[stage].emplace_back(info.func);
				break;
			}
			default:
				break;
			}
		}
		stage++;
	}
	should_run = true;
	main_tasks.shrink_to_fit();
	relaxed_tasks.shrink_to_fit();
}

TaskScheduler::TaskScheduler(const uint32_t num_threads) :
	m_callbacks_arr(num_threads)
{
	for (uint32_t i = 0; i < num_threads; i++)
	{
		auto& cb_p = m_callbacks_arr[i];
		fu2::function<void()> callback = [&]() {
			cb_p++;
		};
		workers.emplace_back(std::make_shared<WorkerThread>([this]() {return should_run.load(); }, callback));
	}

	m_relaxed_q = data_reg.Create<TaskQueue>("current_task_queue"_h32);
}

void ErrorMsg(const char*) {}