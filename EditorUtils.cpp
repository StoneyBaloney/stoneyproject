#ifdef SP_USE_EDITOR

#include "EditorUtils.h"
namespace StoneyCore {
	std::string GetUniqueFilename(std::string filename) {
		unsigned next_name = 2;
		auto try_name = filename;
		for (;;) {
			std::filesystem::path path(try_name);

			if (!std::filesystem::exists(path))
			{
				return path.filename().string();
			}
			if (next_name == UINT_MAX)
			{
				next_name = 0;
				filename = Concat(filename, "OVERFLOW");
			}
			try_name = Concat(filename, "_", std::to_string(next_name));
			next_name++;
		}
	}
}
#endif // SP_USE_EDITOR