#pragma once
#include <chrono>
#include "Defines.h"
#include <function2/function2.hpp>
#include <c314Mpmc/concurrentqueue.h>
template <typename T>
using MpmcQueue = moodycamel::ConcurrentQueue<T>;

using TaskQueue = MpmcQueue<fu2::function<void(void)>>;

struct WorkerThread {
	WorkerThread() = delete;
	~WorkerThread() {
		if (m_thread.joinable())
		{
			m_thread.join();
		}
	}

	WorkerThread(fu2::function<bool(void)> should_run, fu2::function<void(void)> callback) :
		m_should_run(should_run),
		m_callback(callback)
	{
	}
	void Join() {
		if (m_thread.joinable())
		{
			m_thread.join();
		}
	}
	void Start(TaskQueue* relaxed_queue) {
		using namespace std;
		using namespace chrono_literals;
		if (m_thread.joinable())
		{
			m_thread.join();
		}
		if (relaxed_queue) {
		}

		m_thread = std::thread([=](TaskQueue* main_q, TaskQueue* my_queue) {
			if ((main_q != nullptr) && (my_queue != nullptr)) {
				moodycamel::ConsumerToken main_ctok(*main_q);
				moodycamel::ConsumerToken my_ctok(*my_queue);

				while (m_should_run() && main_q != nullptr) {
					fu2::function<void()> func;
					if (main_q->try_dequeue(main_ctok, func)) {
						func();
						m_callback();
					}
					else {
						std::this_thread::sleep_for(8ms);
					}
				}
			}
		}, relaxed_queue, &m_queue);
	}
	std::atomic<bool> m_run{ false };
	fu2::function<void(void)> m_callback;
	fu2::function<bool(void)> m_should_run;
	std::thread m_thread;
	TaskQueue m_queue;
};
