#include "InstanceRegistry.h"
namespace StoneyCore {
	struct InstanceData {
	};
	PrefabID InstanceRegistry::LoadModelsAndInitBatches(const PrefabDescription& description)
	{
		using namespace Renderer;
		auto& model = m_asset_manager.GetModelAssets(description.model_filename);
		stVector<uint32> batch_indices;
		stVector<ModelAssetDescTable> model_batch_descs;
		if (model.vertices.size()) {
			m_render_proxy.PrepareModelBatches(model, description, batch_indices, model_batch_descs);
		}
		for (auto& mesh : model.vertices) {
			//Should add to physics
		}
		//m_render_proxy.PrepareModelShaders(model);

		return CreateInstanceBufferObject(description, batch_indices, model, model_batch_descs);
	}
	PrefabID InstanceRegistry::CreateInstanceBufferObject(const PrefabDescription& description, const stVector<uint32>& batch_ids, const StoneyCore::ModelAsset& model, const stVector<ModelAssetDescTable>& model_batch_desc)
	{
		auto instance_buffer_p = m_instance_buffer_pool.construct();
		if (!instance_buffer_p)
		{
			LogErrorToFile("Failed to create instance buffer!");
			return 0;
		}
		auto& instance_buffer = *instance_buffer_p;

		bool is_indexed = (description.render_spec.vertex_type == VERTEX_SPEC_TYPE::Indexed);

		instance_buffer.indexed = is_indexed;
		num_indexed_batches += uint32(is_indexed * batch_ids.size());
		num_non_indexed_batches += uint32((!is_indexed) * batch_ids.size());
		instance_buffer.batch_infos.resize(model_batch_desc.size());
		uint32 info_index = 0;

		for (auto& batch_desc : model_batch_desc) {
			auto& batch_info = instance_buffer.batch_infos[info_index];
			batch_info.infos.resize(batch_desc.textured_mesh_groups.size());
			batch_info.num_renderables = uint32(batch_desc.textured_mesh_groups.size());
			uint32 mesh_group_index{};
			for (auto& info : batch_info.infos) {
				info.num_verts = 0;
				auto& mesh_group = batch_desc.textured_mesh_groups[mesh_group_index];
				for (auto& mesh_index : mesh_group) {
					info.num_verts += uint32(model.vertices[mesh_index].indices.size());
				}
				mesh_group_index++;
			}
			info_index++;
		}

		PrefabID&& ret = m_next_id++;
		while (m_prefab_map.find(ret) != m_prefab_map.end() && ret != UINT_MAX)
		{
			ret = m_next_id;
			m_next_id++;
		}
		m_prefab_map[ret] = instance_buffer_p;

		m_instance_buffers.emplace_back(instance_buffer_p);
		Append(batch_ids, instance_buffer.batch_ids);
		return ret;
	}
	void InstanceRegistry::BeginUpdatePhysicsScene(float delta) {
		m_physics_world->BeginUpdateScene(delta);
	}
	void InstanceRegistry::GetPhysicsResults() {
		m_physics_world->GetPhysicsResults();
	}
	InstanceRegistry::InstanceRegistry(fu2::function<void(void)> exit_callback) :
		m_exit_callback(exit_callback)
	{
		m_physics_world = m_data_reg.Create<StPxPhysics::StPxWorld>("px_physics"_h32);
	}
	InstanceRegistry::~InstanceRegistry() {
		m_render_proxy.Shutdown();
		m_physics_world->CleanupWorld();
		m_data_reg.ClearAll();
	}
	void InstanceRegistry::HandleRenderWindowResize(int x, int y) {
		m_screen_settings = { x,y };
		main_camera.m_aspect = (float(x) / float(y));
	}
	void InstanceRegistry::HandleEvent(const SDL_Event& evt) {
		static bool use_mouse{ true };
		static bool trap_mouse{ false };
		switch (evt.type) {
		case SDL_EventType::SDL_WINDOWEVENT: {
			switch (evt.window.event)
			{
			case SDL_WINDOWEVENT_CLOSE: {
				this->ShutdownRenderer();
				m_exit_callback();
				break;
			}
			case SDL_WINDOWEVENT_RESIZED: {
				HandleRenderWindowResize(evt.window.data1, evt.window.data2);
				break;
			}
			default:
				break;
			}
			break;
		}

		case SDL_EventType::SDL_QUIT:
		{
			this->ShutdownRenderer();

			break;
		}
		case SDL_EventType::SDL_MOUSEMOTION: {
			if (use_mouse)
			{
				main_camera.RotateCameraX(m_clock.delta_avg * ((double)-evt.motion.yrel * 2.f));
				main_camera.RotateCameraY(m_clock.delta_avg * ((double)evt.motion.xrel * 2.f));
			}
			break;
		}
		case SDL_EventType::SDL_MOUSEBUTTONDOWN: {
			break;
		}
		case SDL_EventType::SDL_MOUSEBUTTONUP: {
			break;
		}
		case SDL_EventType::SDL_KEYDOWN: {
			switch (evt.key.keysym.sym)
			{
			case SDLK_w: {
				main_camera.MoveForward(float(m_clock.delta_avg) * 800.2f * main_camera.m_lerp);
				break;
			}
			case SDLK_s: {
				main_camera.MoveBack(float(m_clock.delta_avg) * 800.2f * main_camera.m_lerp);
				break;
			}
			case SDLK_F11: {
				use_mouse = !use_mouse;
				break;
			}
			case SDLK_a: {
				break;
			}
			case SDLK_d: {
				break;
			}
		
			
			case SDLK_SPACE: {
				main_camera.MoveCameraY(10.2f);
				break;
			}

			case SDLK_y: {
				main_camera.m_lerp += 1.5f;

				break;
			}
			case SDLK_h: {
				main_camera.m_lerp -= 1.5f;

				break;
			}
			case SDLK_F10: {
				trap_mouse = !trap_mouse;
				SDL_bool val = (SDL_bool)trap_mouse;
				SDL_SetRelativeMouseMode(val);
				break;
			}
			default:
				break;
			}
			break;
		}

		default:
			break;
		}
	}
	bool InstanceRegistry::InitPhysics() {
		using namespace physx;
		bool ret;
		if (m_physics_world)
		{
			m_physics_world->InitWorld();
			ret = true;
		}
		else {
			ret = false;
		}

		return ret;
	}
	void InstanceRegistry::PrepareRenderBuffer(StoneyCore::Renderer::RenderBuffer& buffer)
	{
		buffer.indexed_batch_data.clear();
		buffer.batch_data.clear();
		buffer.batch_data.resize(num_non_indexed_batches);
		buffer.indexed_batch_data.resize(num_indexed_batches);
		buffer.deferred = true;
		uint32 next_indexed_index{};
		uint32 next_non_indexed_index{};
		for (size_t i = 0; i < m_instance_buffers.size(); i++)
		{
			auto& instance_buffer = *m_instance_buffers[i];
			auto next_index = instance_buffer.indexed ? next_indexed_index : next_non_indexed_index;
			for (size_t i = 0; i < instance_buffer.batch_ids.size(); i++)
			{
				auto batch_id = instance_buffer.batch_ids[i];
				auto& batch_info = instance_buffer.batch_infos[i];
				batch_info.batch_index = next_index;
				auto& render_buffer_batch = instance_buffer.indexed ? buffer.indexed_batch_data[next_index] : buffer.batch_data[next_index];
				render_buffer_batch.batch_id = batch_id;
				render_buffer_batch.transforms.resize(instance_buffer.num_instances);
				if (instance_buffer.indexed)
				{
					render_buffer_batch.indexed_indirects.resize(batch_info.num_renderables);
				}
				else {
					render_buffer_batch.indirects.resize(batch_info.num_renderables);
				}

				uint32 base_instance = 0;
				uint32 renderable_index = 0;
				uint32 offset = 0;

				next_index++;
				next_indexed_index += instance_buffer.indexed;
				next_non_indexed_index += (!instance_buffer.indexed);
			}
		}
		buffer.valid = true;
	}
	void InstanceRegistry::WriteSceneToRenderBuffer(StoneyCore::Renderer::RenderBuffer& buffer)
	{
		buffer.screen_x = m_screen_settings.width;
		buffer.screen_y = m_screen_settings.height;
		main_camera.SetDirectionVector();
		buffer.camera_mvp = main_camera.GetPerspectiveMat();
		buffer.camera_view = main_camera.GetViewMatrix();

		

		for (size_t i = 0; i < m_instance_buffers.size(); i++)
		{
			auto& instance_buffer = *m_instance_buffers[i];

			for (size_t i = 0; i < instance_buffer.batch_ids.size(); i++)
			{
				auto batch_id = instance_buffer.batch_ids[i];
				auto& batch_info = instance_buffer.batch_infos[i];

				auto& render_buffer_batch = instance_buffer.indexed ? buffer.indexed_batch_data[batch_info.batch_index] : buffer.batch_data[batch_info.batch_index];
				auto& transforms = instance_buffer.m_instance_reg.view<glm::mat4>();
				render_buffer_batch.transforms.resize(transforms.size());
				uint32 index = 0;
				transforms.each([&](EntID id, glm::mat4& mat) {
					render_buffer_batch.transforms[index] = mat;
					index++;
				});
				render_buffer_batch.version++;

				if (instance_buffer.indexed)
				{
					uint32 base_instance = 0;
					uint32 renderable_index = 0;
					uint32 offset = 0;

					for (auto& info : batch_info.infos) {
						auto& indirect = render_buffer_batch.indexed_indirects[renderable_index];
						indirect.baseInstance = 0;
						indirect.count = info.num_verts;
						indirect.baseVertex = 0;
						indirect.firstIndex = offset;
						indirect.instanceCount = instance_buffer.num_instances;
						base_instance++;
						offset += info.num_verts;
						renderable_index++;
					}
				}
				else {
					uint32 base_instance = 0;
					uint32 renderable_index = 0;
					uint32 offset = 0;

					for (auto& info : batch_info.infos) {
						auto& indirect = render_buffer_batch.indirects[renderable_index];
						indirect.baseInstance = 0;
						indirect.count = info.num_verts;
						indirect.first = offset;
						indirect.instanceCount = instance_buffer.num_instances;
						base_instance += instance_buffer.num_instances;
						offset += info.num_verts;
						renderable_index++;
					}
				}
				render_buffer_batch.valid = true;
				render_buffer_batch.batch_version = instance_buffer.batch_version;
			}
		}
	}
	void InstanceRegistry::UpdateRenderBuffer(Renderer::RenderBuffer& buffer) {
		if (buffer.version != render_buffer_version)
		{
			PrepareRenderBuffer(buffer);
		}
		WriteSceneToRenderBuffer(buffer);
	}
	void InstanceRegistry::SubmitRenderPasses() {
		ShaderLoadMessage geometry_pass;
		geometry_pass.shader_type = SHADER_PASS_TYPE::SG_Renderpass_Geometry;
		if (!AssetHandler::LoadShaderToLoadMsg(geometry_pass, "Shaders/SDefaultGeometryPass.vert", "Shaders/SDefaultGeometryPass.frag"))
		{
			LogErrorToFile("Failed to load default geometry pass!");
		}
		geometry_pass.program_id = m_shader_tracker.GetProgramID(geometry_pass.load_info);

		ShaderLoadMessage lighting_pass;
		lighting_pass.shader_type = SHADER_PASS_TYPE::SG_Renderpass_Lighting;
		if (!AssetHandler::LoadShaderToLoadMsg(lighting_pass, "Shaders/SDefaultLightingPass.vert", "Shaders/SDefaultLightingPass.frag"))
		{
			LogErrorToFile("Failed to load default lighting pass!");
		}
		lighting_pass.program_id = m_shader_tracker.GetProgramID(lighting_pass.load_info);

		m_render_proxy.SubmitShader(geometry_pass);
		m_render_proxy.SubmitShader(lighting_pass);
	}
	void InstanceRegistry::StartRenderer() {
		SubmitRenderPasses();
		m_render_proxy.StartRenderer();
	}
	void InstanceRegistry::SubmitPendingBatches() {
		m_render_proxy.SubmitPendingBatches();
	}
	void InstanceRegistry::ShutdownRenderer() {
		m_render_proxy.Shutdown();
	}
	void InstanceRegistry::AddInstance(const PrefabID& prefab_id, const Transform& transform) {
		auto elem = m_prefab_map.find(prefab_id);
		if (elem == m_prefab_map.end())
		{
			LogErrorToFile("Tried to create instance with invalid prefab id.");
			return;
		}
		else {
			auto& instance_buffer = *(*elem).second;
			instance_buffer.num_instances++;
			auto ent_id = instance_buffer.m_instance_reg.create();
			auto instance_id = instance_buffer.m_instance_reg.create();
			auto& inst_transform = instance_buffer.m_instance_reg.assign<Transform>(instance_id);
			auto& inst_mat = instance_buffer.m_instance_reg.assign<glm::mat4>(instance_id);
			inst_transform = transform;
			inst_mat = glm::translate(glm::mat4(1), inst_transform.location);
		}
	}
	PrefabID InstanceRegistry::AddPrefab(const PrefabDescription& description) {
		return LoadModelsAndInitBatches(description);
	}
	Camera InstanceRegistry::GetMainCamera() {
		return main_camera;
	}
	double InstanceRegistry::GetDelta() { 
		return m_clock.delta;
	}
	double InstanceRegistry::GetDeltaAvg() { 
		return m_clock.delta_avg;
	}
#ifdef SP_USE_EDITOR
	void InstanceRegistry::SinkRenderBuffer(Renderer::RenderBuffer* buffer) {
		m_render_proxy.SinkRenderBuffer(buffer);
	}
	bool InstanceRegistry::BeginSceneUpdate(Renderer::RenderBuffer*& buffer) {
		auto&& ret = m_render_proxy.BeginSceneUpdate(buffer, true);

		if (ret)
		{
			m_clock.SetDelta();
			UpdateRenderBuffer(*buffer);
		}

		return ret;
	}
	HWND InstanceRegistry::InitRendererEditor(const Renderer::RendererInitInfo& init_info) {
		return m_render_proxy.InitRendererEditor(init_info);
	}
#endif
	void DefaultExitCallback() {}
	// SP_USE_EDITOR
}