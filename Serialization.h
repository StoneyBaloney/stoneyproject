#pragma once
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <refl.hpp>
#include "GameData.h"
namespace boost {
	namespace serialization {
		ST_SERIALIZABLE(StoneyCore::DirectionalLightProperties);
		ST_SERIALIZABLE(StoneyCore::PrefabDescription);
		ST_SERIALIZABLE(StoneyCore::Material);
		ST_SERIALIZABLE(StoneyCore::RenderInfo);
		ST_SERIALIZABLE(StoneyCore::RenderSpec);
		ST_SERIALIZABLE(StoneyCore::ShaderLoadInfo);
		ST_SERIALIZABLE(glm::vec4);
		ST_SERIALIZABLE(glm::vec3);
		ST_SERIALIZABLE(glm::vec2);
		ST_SERIALIZABLE(glm::quat);
		ST_SERIALIZABLE_ARR_TYPE(glm::mat4, 4);
		ST_SERIALIZABLE_ARR_TYPE(glm::mat3, 3);
	}
}