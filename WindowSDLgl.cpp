#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include "Defines.h"
#ifdef USE_SDL
#include <vector>

#include "WindowSDLgl.h"
#include "Errors.h"

namespace WindowSDLgl {
	typedef stVector<SDL_Window*> WindowVector;
	typedef stVector<Uint32> WindowIDVector;
	typedef stVector<SDL_GLContext> ContextVector;

	bool sdl_init = false;

	WindowVector windows;
	WindowIDVector window_ids;
	ContextVector contexts;

	SDL_Window* MakeWindow(StoneyCore::WindowInitInfo _init_info) {
		if (!sdl_init) {
			if (SDL_InitSubSystem(SDL_INIT_VIDEO)) {       //Also initializes events subsystem)
				return nullptr;
			}
		}

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_Window* _window = SDL_CreateWindow("StoneyGame", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _init_info.m_width, _init_info.m_height, SDL_WINDOW_OPENGL);

		return _window;
	}

	GL_Context CreateContextAndMakeCurrent(SDL_Window* window)
	{
		auto context = SDL_GL_CreateContext(window);

		return context;
	}

	bool MakeContextCurrent(WindowHandle window, GLContext context)
	{
		if (0 == SDL_GL_MakeCurrent(window, context)) {
			return true;
		}
		return false;
	}

	void ClearContext(WindowHandle window, GLContext context)
	{
		SDL_GL_MakeCurrent(window, nullptr);
	}

	void Swap(WindowHandle window) {
		SDL_GL_SwapWindow(window);
	}

	void ToggleFullscreen(size_t window) {
	}
}
#endif // USE_SDL