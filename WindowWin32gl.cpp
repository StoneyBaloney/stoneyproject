#ifdef _WIN32
#ifndef USE_SDL
#include <thread>
#include <Windows.h>
#include "WindowsGlobals.h"
#include "WindowWin32gl.h"
#include "GalogenCore46.h"
#include "Errors.h"
#include "Threading.h"

namespace WindowWin32gl {
	void  CreateContext(HWND _hwnd)
	{
		PIXELFORMATDESCRIPTOR pixel_format = { sizeof(PIXELFORMATDESCRIPTOR),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
			PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
			32,                   // Colordepth of the framebuffer.
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			24,                   // Number of bits for the depthbuffer
			8,                    // Number of bits for the stencilbuffer
			0,                    // Number of Aux buffers in the framebuffer.
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
		};

		HDC device_context = GetDC(_hwnd);
		SetPixelFormat(device_context, ChoosePixelFormat(device_context, &pixel_format), &pixel_format);
	}

	LRESULT CALLBACK WndProc(HWND _hwnd, UINT _message, WPARAM _wparam, LPARAM _lparam)
	{
		switch (_message) {
		case WM_CREATE:
			WindowsData::SetHwnd(_hwnd);
			CreateContext(_hwnd);
			WindowsData::SetWinData();
			return DefWindowProc(_hwnd, _message, _wparam, _lparam);
			break;
		case WM_CLOSE:
			DestroyWindow(_hwnd);

			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(_hwnd, _message, _wparam, _lparam);
			break;
		}
		return  0;
	}

	void InitWindow(WindowInitInfo _init_info) {
		/*MSG msg = { 0 };
		WNDCLASS wc = { 0 };
		wc.lpfnWndProc = WndProc;
		wc.hInstance = GetModuleHandle(NULL);
		wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
		wc.lpszClassName = L"StoneyGame";
		wc.style = CS_OWNDC;
		if (!RegisterClass(&wc)) {
			ThrowErrorContinue("Failed to create window!");
		}
		CreateWindowW(wc.lpszClassName, L"StoneyGame", WS_OVERLAPPEDWINDOW | WS_VISIBLE, 0, 0, 640, 480, 0, 0, wc.hInstance, 0);
		*/
	};
};
#endif // !USE_SDL

#endif // _WIN32