#pragma once
#ifdef SP_USE_EDITOR

#include "AssetHandler.h"
#include "EditorWidgetShape.h"
#include "BatchRenderer3D.h"
namespace StoneyCore {
	namespace Renderer {
#ifdef SP_USE_EDITOR

		struct LightingDebugShapeState {
			GLuint program_id{ 0 };
			GLuint vao_id{ 0 };
			GLuint transform_vbo_id;
			GL_Indirect m_indirect;
		};

#endif // SP_USE_EDITOR

		void GenEditorGizmoBuffer(Editor::EditorWidgetRenderState& widget_state, stVector<GL_Indirect>& out_indirects);

		void GenLightingDebugState(Editor::EditorLightDebugState& render_state);
	}
}
#endif // SP_USE_EDITOR