#include "FramebufferGL.h"

namespace StoneyCore {
	namespace Renderer {
		DeferredGbufferGL::DeferredGbufferGL() : m_buffer_attachments{ GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 }
		{
		}

		DeferredGbufferGL::~DeferredGbufferGL()
		{
		}
	}
}