#pragma once
#ifdef _WIN32

#ifndef SP_USE_EDITOR
#include <windows.h>
#include "WindowsGlobals.h"
#include "StoneyMain.h"
#ifndef NDEBUG

#include <iostream>
#endif //NDEBUG

#include <SDL.h>

int main(int argc, char* argv[])
{
	StoneyMain::Start();

	return 0;
}
#endif // !SP_USE_EDITOR
#endif // _WIN32
