#pragma once
#ifdef SP_USE_EDITOR
#include <function2/function2.hpp>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "EditorWidgetShape.h"
namespace StoneyCore {
	namespace Editor {
		enum EDITOR_SCENE_WIDGET_CONTROL_TYPE {
			Translate,
			Rotate,
			Scale
		};
		using StControlType = EDITOR_SCENE_WIDGET_CONTROL_TYPE;
		struct EditorWidgetRenderState {
			GLuint vao_id{};
			GLuint shape_vbo{};
			GLuint transform_vbo{};
			GLuint colors_vbo{};
			GLuint program_id{};
		};
		struct EditorLightDebugState {
			GLuint program_id{};
			GLuint vao_id{};
			GLuint shape_vbo{};
			GLuint colors_vbo{};
			GLuint inst_trans_vbo{};
		};
		//constexpr auto GetShapeY();

		//constexpr auto GetShapeZ();

	//	constexpr auto GetShapeX();

		inline constexpr auto GetWidgetCube() {
			using namespace glm;

			std::array<vec3, 36> ret = {
			vec3(1,-1,-1),vec3(1,-1,1),vec3(-1,-1,1),
			vec3(1,-1,-1),vec3(-1,-1,1),vec3(-1,-1,-1),
			vec3(1,1,-0.999999),vec3(-1,1,-1),vec3(-1,1,1),
			vec3(1,1,-0.999999),vec3(-1,1,1),vec3(0.999999,1,1),
			vec3(1,-1,-1),vec3(1,1,-0.999999),vec3(0.999999,1,1),
			vec3(1,-1,-1),vec3(0.999999,1,1),vec3(1,-1,1),
			vec3(1,-1,1),vec3(0.999999,1,1),vec3(-1,1,1),
			vec3(1,-1,1),vec3(-1,1,1),vec3(-1,-1,1),
			vec3(-1,-1,1),vec3(-1,1,1),vec3(-1,1,-1),
			vec3(-1,-1,1),vec3(-1,1,-1),vec3(-1,-1,-1),
			vec3(1,1,-0.999999),vec3(1,-1,-1),vec3(-1,-1,-1),
			vec3(1,1,-0.999999),vec3(-1,-1,-1),vec3(-1,1,-1)
			};
			return ret;
		}

		inline constexpr auto GetWidgetColors() {
			using namespace glm;

			std::array<vec3, 3> ret{
			vec3(255.f, 0.f,0.f),
			vec3(0.f, 255.f,0.f),
			vec3(0.f, 0.f,255.f)
			};
			return ret;
		}

		struct EditorSceneWidget {
			StControlType m_control_state{ StControlType::Translate };
			glm::mat4 m_state{ glm::translate(glm::mat4(1), glm::vec3(20,20,20)) };
		};

		void DrawWidget(EditorWidgetRenderState& state, EditorSceneWidget& widget);
	}
}

#endif // SP_USE_EDITOR
