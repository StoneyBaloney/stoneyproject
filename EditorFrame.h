#pragma once
#ifdef SP_USE_EDITOR
#include <wx/wx.h>
#include <wx/aui/aui.h>

extern void IdleCallback(wxIdleEvent& evt);
class EditorFrame : public wxAuiMDIParentFrame
{
private:

protected:
	wxMenuBar* m_menubar;
	wxMenu* m_file_menu;
	wxMenu* m_help_window;

public:

	EditorFrame(wxWindow* parent = nullptr, wxWindowID id = wxID_ANY, const wxString& title = wxT("Stoney Editor"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(1280, 720), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);
	wxAuiManager m_mgr;

	~EditorFrame();
};

#endif // !1
