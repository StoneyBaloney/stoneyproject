#include "ShaderGenerator.h"
#include "SRendererGL.h"
namespace StoneyCore {
	namespace Renderer {
		std::array<float, 20> GetNormalizedQuadBufferGL() {
			std::array<float, 20> ret = {
				-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
				-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
				1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
				1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			};
			return ret;
		}
		//
		GLuint CreateVAOAndBind() {
			GLuint vao;
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);
			return vao;
		}
		void CleanupRenderState(RenderState& render_state)
		{
			if (render_state)
			{
				glBindVertexArray(render_state.vao_id);
				glDeleteBuffers(1, &render_state.element_buffer);
				glDeleteBuffers(1, &render_state.vbo_id);
				glDeleteBuffers(1, &render_state.material_vbo);
				glDeleteBuffers(1, &render_state.instance_vbo);
				for (auto& texture_id : render_state.texture_ids)
				{
					if (texture_id[1])
					{
						glDeleteTextures(1, &texture_id[1]);
					}
				}
				glBindVertexArray(0);
			}
		}
		GLuint CreateInstanceBuffer(const RenderBatch2& batch) {
			GLuint instance_buffer{};
			auto transform_binding = VERTEX_BINDING_TRANSFORM;
			glGenBuffers(1, &instance_buffer);
			glBindBuffer(GL_ARRAY_BUFFER, instance_buffer);
			glBufferData(GL_ARRAY_BUFFER, 0, NULL, batch.UpdateType());			
			for (GLint i = 0; i < 4; i++)
			{
				glVertexAttribPointer(transform_binding + i, 4, GL_FLOAT, GL_FALSE, 64, (void*)(i * 16));
				glEnableVertexAttribArray(transform_binding + i);
				glVertexAttribDivisor(transform_binding + i, 1);

			}


			glBindBuffer(GL_ARRAY_BUFFER, 0);
			return instance_buffer;
		}
	/*	GLuint CreateInstanceBuffer(RenderBatch2* batch) {
			GLuint instance_buffer{};
			glGenBuffers(1, &instance_buffer);
			glBindBuffer(GL_ARRAY_BUFFER, instance_buffer);
			glBufferData(GL_ARRAY_BUFFER, 0, NULL, batch->UpdateType());
			glVertexAttribPointer(12, 4, GL_FLOAT, GL_FALSE, 64, 0);
			glVertexAttribPointer(13, 4, GL_FLOAT, GL_FALSE, 64, (void*)16);
			glVertexAttribPointer(14, 4, GL_FLOAT, GL_FALSE, 64, (void*)32);
			glVertexAttribPointer(15, 4, GL_FLOAT, GL_FALSE, 64, (void*)48);
			glEnableVertexAttribArray(12);
			glEnableVertexAttribArray(13);
			glEnableVertexAttribArray(14);
			glEnableVertexAttribArray(15);
			glVertexAttribDivisor(12, 1);
			glVertexAttribDivisor(13, 1);
			glVertexAttribDivisor(14, 1);
			glVertexAttribDivisor(15, 1);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			return instance_buffer;
		}*/
		void CreateElementArray(stVector<GLuint>& out_array, const RenderBatch2& batch) {
			out_array.clear();
			uint32 index_offset{};
			for (const auto& batch_verts : batch.batch_verts)
			{
				stVector<uint32> index_array(batch_verts.indices);
				for (auto& index : index_array) {
					index += index_offset;
				}

				out_array.insert(out_array.end(), index_array.begin(), index_array.end());
				index_offset += index_array.size();
			}
		}
		GLuint CreateElementBuffer(const stVector<GLuint>& element_array) {
			GLuint element_buf{};
			if (!element_array.size())
			{
				return 0;
			}
			glGenBuffers(1, &element_buf);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buf);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * element_array.size(), &element_array[0], GL_STATIC_DRAW);

			return element_buf;
		}
		constexpr auto POSITION_OFFSET = 0;
		constexpr auto NORMAL_OFFSET = 12;
		constexpr auto UV_OFFSET = 24;
		constexpr auto TANGENT_OFFSET = 32;
		constexpr auto BITANGENT_OFFSET = 44;
		GLuint CreateVBO(const stVector<GLfloat>& buffer, GLuint update_type, const ShaderGenParams& params) {
			GLuint vbo_id{};
			if (!buffer.size())
			{
				return 0;
			}
			int8_t unique_channels{};
			auto vertex_size =  6;

			for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
			{
				if (!((1 << params.uv_channels[channel]) & unique_channels))
				{
					vertex_size += 2;
					if (channel == TEX_TYPE_NORM)
					{
						vertex_size += 6; // For the tangents

					}
				}
				unique_channels |= (1 << (params.uv_channels[channel]));
			}
			
			vertex_size *= sizeof(GLfloat);

			glGenBuffers(1, &vbo_id);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
			glBufferData(GL_ARRAY_BUFFER, sizeof(buffer[0]) * buffer.size(), &buffer[0], update_type);
		
			glVertexAttribPointer(VERTEX_BINDING_POSITIONS, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)POSITION_OFFSET);
			glEnableVertexAttribArray(VERTEX_BINDING_POSITIONS);
			
			glVertexAttribPointer(VERTEX_BINDING_NORMALS, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)NORMAL_OFFSET);
			glEnableVertexAttribArray(VERTEX_BINDING_NORMALS);
			unique_channels =0;
			int binding_location{ };
			
			for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
			{
				
				if (!((1 << params.uv_channels[channel]) & unique_channels))
				{
					glVertexAttribPointer(VERTEX_BINDING_UV_COORDS+ binding_location, 2, GL_FLOAT, GL_FALSE, vertex_size, (void*)(UV_OFFSET + (8 * binding_location)));
					glEnableVertexAttribArray(VERTEX_BINDING_UV_COORDS+ binding_location);
					binding_location++;
				}
				unique_channels |= (1 << (params.uv_channels[channel]));
			}
			if (params.texture_flags & (1 << TEX_TYPE_NORM))
			{
				auto tang_offset = (8 * (binding_location)) + UV_OFFSET;

			
				glVertexAttribPointer(VERTEX_BINDING_BITANGENTS, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)(tang_offset ));
				glEnableVertexAttribArray(VERTEX_BINDING_BITANGENTS);

				glVertexAttribPointer(VERTEX_BINDING_TANGENTS, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)(tang_offset + 12));
				glEnableVertexAttribArray(VERTEX_BINDING_TANGENTS);

			}

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			return vbo_id;
		}	

		void GenerateIndexedVertexArray(const RenderBatch2& batch, stVector<GLfloat>& buffer)
		{
			buffer.clear();
			buffer.reserve(size_t(batch.total_verts) * 8);
		
			stVector<GLfloat> insert_buffer;
			insert_buffer.reserve(16);
			for (auto& mesh : batch.batch_verts) {
			
			
				bool has_tangents = mesh.tangents.size() && (mesh.tangents.size() == mesh.bitangents.size()) && batch.shader_params.texture_flags & (1 << TEX_TYPE_NORM);
				for (size_t i = 0; i < mesh.indices.size(); i++)
				{
					//Current order is
					//Positions
					//Normals
					//UVcoords
					//Tangents
					//Bitangents

					if (!mesh.vertices.size())
					{
						continue;
					}
					float next[6] = {
						mesh.vertices[i * 3],
						mesh.vertices[i * 3 + 1],
						mesh.vertices[i * 3 + 2],
						mesh.normals[i * 3],
						mesh.normals[i * 3 + 1],
						mesh.normals[i * 3 + 2]						
					};
					const auto itr = buffer.end();
					buffer.insert(itr, next, &next[6]);
					
					

					//buffer.insert(buffer.end(), &mesh.vertices[i*3], &mesh.vertices[i * 3 + 3]);
					//buffer.insert(buffer.end(), &mesh.normals[i * 3], &mesh.normals[i * 3 + 3]);
					
					int8_t unique_channels{};
					for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
					{
						
						if (!((1 << batch.shader_params.uv_channels[channel]) & unique_channels))
						{
							insert_buffer.emplace_back(mesh.texcoords[channel][i * 2]);
							insert_buffer.emplace_back(mesh.texcoords[channel][i * 2 + 1]);
						}
						unique_channels |= (1 << (batch.shader_params.uv_channels[channel]));
					}
					buffer.insert(buffer.end(), insert_buffer.begin(), insert_buffer.end());
					if (has_tangents)
					{
						float next[6] = {
						mesh.bitangents[i * 3],
						mesh.bitangents[i * 3 + 1],
						mesh.bitangents[i * 3 + 2],
						mesh.tangents[i * 3],
						mesh.tangents[i * 3 + 1],
						mesh.tangents[i * 3 + 2]
						};
						buffer.insert(buffer.end(), next, &next[6]);
						
	
					}
			


					insert_buffer.clear();
				}
			}
		}
		void GenerateNonIndexedVertexArray2(const RenderBatch2& batch, stVector<GLfloat>& buffer)
		{
			buffer.clear();
			buffer.reserve(size_t(batch.total_verts) * 8);
			for (auto& mesh : batch.batch_verts) {
				for (auto& vert_index : mesh.indices) {
					float next[8] = {
						mesh.vertices[vert_index * 3],
						mesh.vertices[vert_index * 3 + 1],
						mesh.vertices[vert_index * 3 + 2],
						mesh.normals[vert_index * 3],
						mesh.normals[vert_index * 3 + 1],
						mesh.normals[vert_index * 3 + 2],
						mesh.texcoords[0][vert_index * 2],
						mesh.texcoords[0][vert_index * 2 + 1]
					};
					const auto itr = buffer.end();
					buffer.insert(itr, next, &next[8]);
				}
			}
		}

		void SetTexParam(StoneyCore::TEX_WRAP_MODE wrap_u, GLenum& tex_param)
		{
			switch (wrap_u)
			{
			case StoneyCore::TEX_WRAP_MODE::Clamp:
				tex_param = GL_CLAMP_TO_EDGE;
				break;
			case StoneyCore::TEX_WRAP_MODE::Wrap:
				tex_param = GL_REPEAT;
				break;
			case StoneyCore::TEX_WRAP_MODE::Decal:
				tex_param = GL_DECAL;
				break;
			case StoneyCore::TEX_WRAP_MODE::Mirror:
				tex_param = GL_MIRRORED_REPEAT;
				break;
			case StoneyCore::TEX_WRAP_MODE::Other:
			default:
				break;
			}
		}
		void GenerateTexture(const stVector<Texture>& batch_textures,const TextureInfo& texture_info, GLuint& texture_id)
		{
			if (!texture_info.num_components ||
				!texture_info.width ||
				!texture_info.height
				)
			{
				texture_id = 0;

			}

			

			glGenTextures(1, &texture_id);
			glBindTexture(GL_TEXTURE_2D_ARRAY, texture_id);

			GLint height = texture_info.height;
			GLint width = texture_info.width;
			GLenum tex_format;
			switch (texture_info.num_components)
			{
			case 1: {
				tex_format = GL_R;
				glTexStorage3D(GL_TEXTURE_2D_ARRAY, 4, GL_R8, width, height, batch_textures.size());
				break;
			}
			case 2: {
				tex_format = GL_RG;
				glTexStorage3D(GL_TEXTURE_2D_ARRAY, 4, GL_RG8, width, height, batch_textures.size());
				break;
			}
			case 3: {
				tex_format = GL_RGB;
				glTexStorage3D(GL_TEXTURE_2D_ARRAY, 4, GL_RGB8, width, height, batch_textures.size());
				break;
			}
			case 4:
			default:
			{
				tex_format = GL_RGBA;
				glTexStorage3D(GL_TEXTURE_2D_ARRAY, 4, GL_RGBA8, width, height, batch_textures.size());
				break;
			}
			}

			GLint index = 0;
			for (const auto& texture : batch_textures)
			{
				glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, index, width, height, 1, tex_format, GL_FLOAT, texture.image);
				index++;
			}
			auto wrap_u = texture_info.map_u;
			auto wrap_v = texture_info.map_v;
			GLenum tex_param_s{ GL_REPEAT };
			GLenum tex_param_t{ GL_REPEAT };
			SetTexParam(wrap_u, tex_param_s);
			SetTexParam(wrap_v, tex_param_t);

			glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, tex_param_s);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, tex_param_t);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
		}
		void GenBatchTexture2(const RenderBatch2& batch, RenderState& out_render_state) {

			out_render_state.texture_ids.clear();
			for (GLuint channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
			{
				if (batch.textures[channel].size())
				{
					auto& tex_id = out_render_state.texture_ids.emplace_back();	
					tex_id[0] = channel;
					GenerateTexture(batch.textures[channel], batch.texture_infos[channel] , tex_id[1]);
				}

			}

		}
		GLuint GenBatchTexture(RenderBatch2* batch) {
			GLuint texture_id{ 0 };
			glGenTextures(1, &texture_id);
			glBindTexture(GL_TEXTURE_2D_ARRAY, texture_id);

			GLint height = 0;
			GLint width = 0;

			for (auto& texture : batch->textures[TEX_TYPE_DIFFUSE]) {
				height = height < texture.height ? texture.height : height;
				width = width < texture.width ? texture.width : width;
			}

			glTexStorage3D(GL_TEXTURE_2D_ARRAY, 4, GL_RGBA8, width, height, GLsizei(batch->textures.size()));

			GLint index = 0;
			for (auto& texture : batch->textures[TEX_TYPE_DIFFUSE])
			{
				glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, index, texture.width, texture.height, 1, GL_RGBA, GL_FLOAT, texture.image);
				index++;
			}
			glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
			return texture_id;
		}
		bool VerifyBatch2(const StoneyCore::RenderState& batch_state, const Renderer::RenderBatch2& batch)
		{
			if (!batch_state) {
				return false;
			}
			else
			{
				return true;
			}
		}
		bool VerifyBatch(StoneyCore::RenderState& batch_state, Renderer::RenderBatch2* batch)
		{
			if (!batch_state) {
				return false;
			}
			else
			{
				return true;
			}
		}
		GLuint CreateShader(const std::string& vert_shader, const std::string& frag_shader, const char* vert_filename, const char* frag_filename) {
			GLuint program_id = glCreateProgram();
			GLuint vert_shader_id = glCreateShader(GL_VERTEX_SHADER);
			GLuint frag_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

			if (program_id == 0) {
				LogErrorToFile("Program ID was 0");
				return 0;
			}
			if (vert_shader_id == 0) {
				LogErrorToFile("VertexShader failed to be created.");
				return 0;
			}

			if (frag_shader_id == 0) {
				LogErrorToFile("FragmentShader failed to be created.");
				return 0;
			}

			CompileShader(vert_shader, vert_shader_id, program_id, vert_filename);

			CompileShader(frag_shader, frag_shader_id, program_id, frag_filename);

			LinkShaders(program_id, vert_shader_id, frag_shader_id);
			return program_id;
		}
		GLuint CreateShader(const StoneyCore::ShaderLoadMessage& load_msg) {
			return CreateShader(load_msg.vert_shader, load_msg.frag_shader, load_msg.load_info.vs_filename.c_str(), load_msg.load_info.fs_filename.c_str());
		}
		void LinkShaders(const GLuint& ProgramID, const GLuint& VertexShaderID, const GLuint& FragmentShaderID)
		{
			glLinkProgram(ProgramID);

			GLint isLinked = 0;
			glGetProgramiv(ProgramID, GL_LINK_STATUS, (int*)& isLinked);

			if (isLinked == GL_FALSE) {
				GLint maxLength = 0;

				glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &maxLength);
				std::string errorLog;
				errorLog.resize(maxLength);

				if (maxLength != 0) {
					glGetProgramInfoLog(ProgramID, maxLength, &maxLength, &errorLog[0]);
					LogErrorToFile(errorLog);
				}

				glDeleteProgram(ProgramID);

				glDeleteShader(VertexShaderID);
				glDeleteShader(FragmentShaderID);
			}
			glDetachShader(ProgramID, VertexShaderID);
			glDetachShader(ProgramID, FragmentShaderID);
		}
		void CompileShader(const std::string& Contents, const GLuint& ID, GLuint ProgramID, const char* filename)
		{
			const char* GLSL = Contents.c_str();
			glShaderSource(ID, 1, &GLSL, nullptr);
			glCompileShader(ID);

			GLint success = 0;
			glGetShaderiv(ID, GL_COMPILE_STATUS, &success);

			if (success == GL_FALSE) {
				GLint maxLength = 0;
				glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &maxLength);

				// The maxLength includes the NULL character
				stVector<char> errorLog(maxLength);
				glGetShaderInfoLog(ID, maxLength, &maxLength, &errorLog[0]);

				glDeleteShader(ID); // Don't leak the shader.

				auto err = std::string("Failed to create shader ");
				if (filename)
				{
					err += "'";
					err += filename;
					err += "' \n";
				}
				const char* err_output = &errorLog[0];
				err += err_output;
				LogErrorToFile(err);
			}
			glAttachShader(ProgramID, ID);
		}

		void HandleWindowResizeGL(int x, int y) {
			glViewport(0, 0, x, y);
		}
		void DrawBuffer_Forward(GLuint& cam_ubo, RenderBuffer* render_buffer, BigBitField<MAX_RENDER_STATES>& valid_batches, RenderStateArray& render_states)
		{
			glClearDepth(1.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glBufferSubData(GL_UNIFORM_BUFFER, 0, 64, &render_buffer->camera_mvp[0][0]);
			glBufferSubData(GL_UNIFORM_BUFFER, 64, 64, &render_buffer->camera_view[0][0]);
			glBindBufferRange(GL_UNIFORM_BUFFER, 0, cam_ubo, 0, sizeof(glm::mat4) * 2);
			for (auto& batch_data : render_buffer->batch_data) {
				if (valid_batches.IsSet(batch_data.batch_id) && batch_data.indirects.size())
				{
					auto& render_state = render_states[batch_data.batch_id - 1];
					DrawBatch_Forward(render_state, batch_data);
				}
			}
		}

		void DrawBatch_Forward(StoneyCore::RenderState& render_state, StoneyCore::Renderer::BatchRenderData& batch_data)
		{
			if (render_state.batch_version == batch_data.batch_version)
			{
				glUseProgram(render_state.program_id);

				UpdateAndDrawBatch(render_state, batch_data);
			}
			else {
				std::string err = Concat("Log: Renderer skipped batch number ID ", std::to_string(batch_data.batch_id), " because it was outdated.");
				LogErrorToFile(err);
			}
		}
		void DrawBatchIndexed(StoneyCore::RenderState& render_state, StoneyCore::Renderer::BatchRenderData& batch_data) {
			glBindVertexArray(render_state.vao_id);
			if ((render_state.version != batch_data.version))
			{
				if (batch_data.transforms.size()) {
					glBindBuffer(GL_ARRAY_BUFFER, render_state.instance_vbo);
					glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * batch_data.transforms.size(), &batch_data.transforms[0][0], GL_STREAM_DRAW);
					for (GLint i = 0; i < 4; i++)
					{
						glVertexAttribPointer(VERTEX_BINDING_TRANSFORM + i, 4, GL_FLOAT, GL_FALSE, 64, (void*)(i * 16));
						glEnableVertexAttribArray(VERTEX_BINDING_TRANSFORM + i);
						glVertexAttribDivisor(VERTEX_BINDING_TRANSFORM + i, 1);

					}
				}
				render_state.version = batch_data.version;
			}
			glBindBuffer(GL_ARRAY_BUFFER, render_state.vbo_id);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, render_state.element_buffer);
			
			for (auto& texture_id : render_state.texture_ids) {
				glActiveTexture(GL_TEXTURE0 + texture_id[0]);				
				glBindTexture(GL_TEXTURE_2D_ARRAY, texture_id[1]);				
				
			}
			glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, &batch_data.indexed_indirects[0], batch_data.indexed_indirects.size(), 0);
			glActiveTexture(GL_TEXTURE0);
			glBindVertexArray(0);
		}
		void UpdateAndDrawBatch(StoneyCore::RenderState& render_state, StoneyCore::Renderer::BatchRenderData& batch_data)
		{
			glBindVertexArray(render_state.vao_id);
			if ((render_state.version != batch_data.version))
			{
				if (batch_data.transforms.size()) {
					glBindBuffer(GL_ARRAY_BUFFER, render_state.instance_vbo);
					glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * batch_data.transforms.size(), &batch_data.transforms[0][0], GL_STREAM_DRAW);
					for (GLint i = 0; i < 4; i++)
					{
						glVertexAttribPointer(VERTEX_BINDING_TRANSFORM + i, 4, GL_FLOAT, GL_FALSE, 64, (void*)(i * 16));
						glEnableVertexAttribArray(VERTEX_BINDING_TRANSFORM + i);
						glVertexAttribDivisor(VERTEX_BINDING_TRANSFORM + i, 1);

					}
				}
				render_state.version = batch_data.version;
			}

			//glBindBuffer(GL_ARRAY_BUFFER, render_state.material_vbo);

			for (auto& texture_id : render_state.texture_ids) {
				glActiveTexture(GL_TEXTURE0 + texture_id[0]);
				glBindTexture(GL_TEXTURE_2D_ARRAY, texture_id[1]);
			}
			glMultiDrawArraysIndirect(GL_TRIANGLES, batch_data.indirects.data(), render_state.num_renderables, 0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}
		GLuint GenerateMaterialBuffer(const stVector<float>& materials) {
			
			//Not sure if should use s or t for material index, or if it matters.
			GLuint ret{};

			if (materials.size() % 12) {
				return ret;
			}
				
			if (GLsizei width = GLsizei(materials.size()/2)) {
				

				GLsizei height = (width * RG_NUM_MATERIALS);

				glGenTextures(1, &ret);

				glBindTexture(GL_TEXTURE_2D, ret);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, &materials[0]);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glBindTexture(GL_TEXTURE_2D, 0);
			}
			return ret;
		}
		bool InitDeferredFrameBufferGL(DeferredGbufferGL& framebuffer, RendererInitInfo& init_info)
		{
			if (framebuffer.framebuffer_id)
			{
				glDeleteFramebuffers(1, &framebuffer.framebuffer_id);
			}
			glGenFramebuffers(1, &framebuffer.framebuffer_id);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer.framebuffer_id);

			glGenTextures(2, framebuffer.m_buffer_textures);

			//Lets try packing all the color info together. diffue,ambient, etc/

			glBindTexture(GL_TEXTURE_2D, framebuffer.col_tex_id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, init_info.window_width, init_info.window_height, 0, GL_RGBA_INTEGER, GL_INT, NULL); // fourth component will be used for material ID
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			glBindTexture(GL_TEXTURE_2D, framebuffer.normal_tex_id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, init_info.window_width, init_info.window_height, 0, GL_RGBA, GL_FLOAT, NULL); // fourth component will be used for depth
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffer.col_tex_id, 0);

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, framebuffer.normal_tex_id, 0);

			glDrawBuffers(2, framebuffer.m_buffer_attachments);

			glGenRenderbuffers(1, &framebuffer.depth_buffer_id);
			glBindRenderbuffer(GL_RENDERBUFFER, framebuffer.depth_buffer_id);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, init_info.window_width, init_info.window_height);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, framebuffer.depth_buffer_id);
			
			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			{
				LogErrorToFile("Framebuffer initialization failed!");
				return false;
			}
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			return true;
		}

		bool RendererConstArgs::IsValid() {
			return
				batch_queue &&
				free_batch_queue &&
				render_buffer_queue &&
				free_buffer_queue &&
				render_queue;
		}
#ifdef SP_USE_EDITOR
		void DrawEditorGizmo_Forward(StoneyCore::Editor::EditorWidgetRenderState& widget_state, StoneyCore::Renderer::RenderBuffer* render_buffer, stVector<StoneyCore::Renderer::GL_Indirect>& indirects)
		{
			glUseProgram(widget_state.program_id);
			glBindBufferRange(GL_UNIFORM_BUFFER, 4, widget_state.transform_vbo, 0, sizeof(glm::mat4));
			glBufferSubData(GL_UNIFORM_BUFFER, 0, 64, &render_buffer->control_widget.m_state[0][0]);
			glBindVertexArray(widget_state.vao_id);
			glMultiDrawArraysIndirect(GL_TRIANGLES, &indirects[0], 3, 0);
		}
#endif //SP_USE_EDITOR
	}
}