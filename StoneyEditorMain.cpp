#ifdef SP_USE_EDITOR
#include "StoneyEditorMain.h"
StoneyEditorMain::StoneyEditorMain()
{
	m_property_panel = new StPropertyPanel(this);
	m_mgr.AddPane(m_property_panel, wxAuiPaneInfo().Left().Caption(wxT("Prefab Property Panel")).CloseButton(false).MaximizeButton(true).PinButton(true).Dock().Resizable().FloatingSize(wxSize(330, 400)).BestSize(wxSize(330, 400)).MinSize(wxSize(50, -1)).Layer(1));
	m_mgr.Update();
}

StoneyEditorMain::~StoneyEditorMain()
{
}
#endif //SP_USE_EDITOR