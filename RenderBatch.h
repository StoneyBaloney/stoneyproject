#pragma once

#include "pch.h"
#include "AssetHandler.h"
#include <atomic>

namespace StoneyCore {
	namespace Renderer {
		struct ModelInfo {
			uint32_t num_verts{};
		};
		enum TEXTURE_CHANNEL {
		};
		struct RenderBatch2
		{

			uint32_t batch_id{ 1 };
			uint32_t batch_version{ 0 };
			uint32_t total_verts{ 0 };
			int64_t shader_id{ 0 };
			Renderer::ShaderGenParams shader_params;

			std::string vertex_shader{};
			std::string frag_shader{};

			uint32 batch_type{ 0 };
			RenderSpec render_spec;

			TextureInfo texture_info;
			stVector<ModelVertices> batch_verts; // All the mesh vertices

			//stVector<Texture> textures;
			std::array<stVector<Texture>, 8> textures;
			std::array<TextureInfo, 8> texture_infos; // 8 channels
			stVector<Material> materials;

			stVector<ModelInfo> model_infos;
			GLuint UpdateType() const { return render_spec.GetUpdateTypeGL(); }
			bool IsCompatible(const PrefabDescription& description) const {
				return render_spec == description.render_spec;
			}
			int64_t ShaderID() const {
				return shader_id;
			}

			RenderBatch2();
			~RenderBatch2();
		};

		void LoadAssetToBatch(RenderBatch2& out_batch, const StoneyCore::ModelAsset& model, const ModelAssetDescTable& asset_desc);
		void SortModelAssets(const StoneyCore::ModelAsset& model, stVector<ModelAssetDescTable>& out_asset_desc);
		void InitModelBatches(const stVector<RenderBatch2*>& batches, const StoneyCore::ModelAsset& model, const stVector<ModelAssetDescTable>& asset_desc);

		stVector<RenderBatch2*> AllocBatches(uint32 num_batches, DataRegistrar<>& reg, uint32 next_id, stVector<uint32>& out_indices);

		struct RenderBatchInfo
		{
		};
	}
}