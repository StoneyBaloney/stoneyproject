#ifdef SP_USE_EDITOR
#include <boost/chrono/chrono.hpp>
#include <glm/gtx/quaternion.hpp>
#include "EditorSceneManager.h"

namespace StoneyCore
{
	namespace Editor
	{
		EditorSceneManager::EditorSceneManager(fu2::function<void(void)> exit_callback) :
			m_entity_manager2(exit_callback)
		{
			//if (m_renderer_handle.m_queues.IsValid())
			//{
			//	m_batch_renderer = data_reg.Create<Renderer::BatchRenderer3D>(
			//		"m_batch_renderer"_h32, m_renderer_handle.m_queues);
			//}
			//if (!m_batch_renderer)
			//{
			//	LogErrorToFile("Failed to start renderer!");
			//}
			//m_physics_world = data_reg.Create<StPxPhysics::StPxWorld>("px_physics"_h32);
		}

		void EditorSceneManager::SetPropertyPanel(StPropertyPanel* prop_panel) {
			m_property_panel = prop_panel;
			m_prefab_panel = prop_panel->m_prefab_panel;
			m_lighting_panel = prop_panel->m_lighting_panel;

			m_shader_panel = prop_panel->m_shader_panel;
			m_editor_prop_mgr = prop_panel->m_prefab_panel->m_property_mgr;
			glsl_scint = prop_panel->m_shader_panel->m_scintilla3;
			m_shader_choices = m_shader_panel->m_shader_choices;
		}

		void EditorSceneManager::HandleEventsEditor() {
			SDL_Event test_event;
			//	static bool use_mouse = true;

			while (SDL_PollEvent(&test_event)) {
				switch (test_event.type) {
				case SDL_EventType::SDL_WINDOWEVENT: {
					switch (test_event.window.event)
					{
					case SDL_WINDOWEVENT_CLOSE: {
						m_entity_manager2.ShutdownRenderer();

						break;
					}
					case SDL_WINDOWEVENT_RESIZED: {
						break;
					}
					default: {
						break;
					}
					}
				}
				case SDL_EventType::SDL_KEYDOWN: {
					switch (test_event.key.keysym.sym)
					{
					case SDLK_t: {
						test_light_move_speed += 0.05f;
						break;
					}
					case SDLK_g: {
						test_light_move_speed -= 0.05f;
						break;
					}
					case SDLK_u: {
						test_light_diffuse += 0.05f;
						break;
					}
					case SDLK_j: {
						test_light_diffuse -= 0.05f;
						break;
					}
					case SDLK_i: {
						test_light_constant += 0.5f;
						break;
					}
					case SDLK_k: {
						test_light_constant -= 0.5f;
						break;
					}
					case SDLK_o: {
						test_light_linear += 0.05f;
						break;
					}
					case SDLK_l: {
						test_light_linear -= 0.05f;
						break;
					}
					case SDLK_p: {
						test_light_quadratic += 0.005f;
						break;
					}
					case SDLK_SEMICOLON: {
						test_light_quadratic -= 0.005f;
						break;
					}

					default: {
						break;
					}
					}
				default: {
					break;
				}
				}
				}


				m_entity_manager2.HandleEvent(test_event);
			}
		}

		void EditorSceneManager::UpdateAll() {
			HandleEventsEditor();
			UpdateRendererScene();
			PrintErrorQueue(10);
		}

		EditorSceneManager::~EditorSceneManager() {
		}

		void EditorSceneManager::HandleApplyShaderChanges(wxCommandEvent& evt) {
		}

		void EditorSceneManager::SetWidgetPose()
		{
			using namespace physx;
			auto& widget_loc = widget_transform.location;

			m_widget_px_location = PxVec3(widget_loc.x, widget_loc.y, widget_loc.z);
			widget_px_transform.p = m_widget_px_location;
			widget_actor_x->setGlobalPose(widget_px_transform);
			widget_actor_y->setGlobalPose(widget_px_transform);
			widget_actor_z->setGlobalPose(widget_px_transform);
		}

		void EditorSceneManager::StartRenderer() {
			m_entity_manager2.StartRenderer();
		}

		void EditorSceneManager::SubmitPendingBatches() {
			m_entity_manager2.SubmitPendingBatches();
		}

		void EditorSceneManager::ShutdownRenderer() {
			m_entity_manager2.ShutdownRenderer();
		}

		bool EditorSceneManager::InitPhysics() {
			auto&& ret = m_entity_manager2.InitPhysics();

			//Do any editor physics stuff here

			return ret;
		}

		void EditorSceneManager::UpdateRendererScene() {
			Renderer::RenderBuffer* buffer{};
			if (!m_entity_manager2.BeginSceneUpdate(buffer)) {
				return;
			}

			//Do editor render update here

			//TESTING
			{
			if (!buffer->m_dynamic_lighting.point_lights.size())
			{
				auto& light_properties = buffer->m_dynamic_lighting.point_lights.emplace_back();

				light_properties.diffuse = vec3(0.7, 0.7, 0.7);
				light_properties.specular = vec3(0, 0, 0);
				light_properties.m_point_light.x = 1.f;
				light_properties.m_point_light.y = 0.0001f;
			}


			static float test_light_anim = 0.f;
			while (test_light_anim > 360.f)
			{
				test_light_anim -= 360.f;

			}
			auto& light = buffer->m_dynamic_lighting.point_lights[0];
			test_light_anim += m_entity_manager2.GetDeltaAvg() * 0.05f;
			
			light.position = m_entity_manager2.GetMainCamera().Position() + vec3(200.f * sin(test_light_anim * test_light_move_speed), 20.f, 0.f);
			//light.ambient = vec3(glm::max(glm::min(test_val, 0.1f), 0.f));
			light.diffuse = vec3(test_light_diffuse);
			light.specular = vec3(0, 0, 0);
			light.m_point_light.x = 1.f;
			light.m_point_light.y = 0.0001f;
			light.m_point_light.z = 0.0001f;


		}

			m_entity_manager2.SinkRenderBuffer(buffer);	//Submits the buffer to the renderer
		}

		void EditorSceneManager::HandlePropertyChangeEvent(wxCommandEvent& evt) {
		}

		HWND EditorSceneManager::InitRendererEditor(Renderer::RendererInitInfo init_info) {
			return m_entity_manager2.InitRendererEditor(init_info);
		}

		PrefabID EditorSceneManager::AddPrefab(PrefabDescription& description) {
			auto&& ret = m_entity_manager2.AddPrefab(description);
			auto page = new StPropertyPage();
			m_editor_prop_mgr->AddPage(wxEmptyString, wxNullBitmap, page);
			StGenerateProperties(page, description);
			m_editor_property_pages.emplace_back(page);

			return ret;
		}

		void DefaultExitCB() {}
	}
}
#endif // SP_USE_EDITOR