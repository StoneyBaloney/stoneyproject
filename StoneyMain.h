#pragma once
#ifndef SP_USE_EDITOR

#include <stdint.h>

#include "Defines.h"
#ifdef USE_OPENGL

#endif // USE_OPENGL

namespace StoneyMain {
	void Start();

	void Shutdown();

	void HandleEvents();
};
#endif // !SP_USE_EDITOR
