#ifndef SP_USE_EDITOR

#include <chrono>
#include <ctime>
#include <random>
#include <thread>
#include <glm/gtx/transform.hpp>
#include "pch.h"
#include "Transform.h"
#include "Input.h"
#include "TaskScheduler.h"
#include "Tester.h"

#include "SceneHandler.h"

#include <entt/core/ident.hpp>
#include <entt/entity/registry.hpp>

namespace StoneyMain {
	bool trap_mouse = false;

	bool has_started = false;
	bool should_run = false;
	constexpr auto width = 1620;
	constexpr auto height = 1050;
	static StoneyCore::Camera g_camera = StoneyCore::Camera(70, width / height, 0.2f, 100.f);

	void InitSystems()
	{
	}

	void Cleanup() {
		SDL_Quit();
	}

	void MainLoop() {
		using namespace StoneyCore;
		RenderSpec render_spec1;

		render_spec1.uses_material = false;
		render_spec1.use_texture = true;
		RenderSpec render_spec2;

		render_spec1.uses_material = false;
		render_spec1.use_texture = true;
		RenderInfo my_custom_render_info(
			{ "Shaders/InstancedTexturedLitModel.vert", "Shaders/InstancedTexturedLitModel.frag", {"MyAwesomeShader"} }
		, UPDATE_TYPE::Dynamic, VERTEX_SPEC_TYPE::NonIndexed);

		StoneyCore::PrefabDescription my_prefab;
		my_prefab.name = "my awesome prefab";
		my_prefab.model_filename = "Models/Cube.obj";
		my_prefab.texture_filename = "Textures/Ground_Dirt_02_COLOR.bmp";

		my_prefab.render_spec = render_spec1;

		my_prefab.render_info = my_custom_render_info;
		StoneyCore::PrefabDescription my_prefab2 = {
		"MyAwesomePrefab2",
		"Models/Cube.obj",
		"Textures/redstuff.bmp",
		my_custom_render_info,
		render_spec1
		};
		StoneyCore::PrefabDescription my_prefab3 = {
		"MyAwesomePrefab3",
		"Models/Cube.obj",
		"Textures/redstuff.bmp",
		my_custom_render_info,
		render_spec1
		};

		glm::vec4 location = {};
		Renderer::RendererInitInfo renderer_init = { width , height };
		fu2::function<void(void)> exit = [&]() {should_run = false; };
		SceneHandler handler(exit);

		handler.InitPhysics();
		auto prefab1 = handler.AddPrefab(my_prefab);
		auto prefab2 = handler.AddPrefab(my_prefab2);
		auto prefab3 = handler.AddPrefab(my_prefab3);
		handler.SortBatches();
		handler.VerifyBatches();
		handler.InitRenderer(renderer_init);
		handler.StartRenderer();
		handler.SubmitPendingBatches();
		PrefabIndex my_prefab_id;
		stVector<InstanceHandle> instances;
		instances.reserve(1000);

		Transform tester;
		for (size_t i = 0; i < 10; i++) {
			for (size_t j = 0; j < 10; j++) {
				for (size_t k = 0; k < 10; k++) {
					tester.location.x = 3.f * i;
					tester.location.z = 3.f * j;
					tester.location.y = 3.f * k;
					instances.emplace_back(handler.AddInstance(prefab1, tester));

					tester.location.x = 6.f * i;
					tester.location.z = 6.f * j;
					tester.location.y = 6.f * k;
					instances.emplace_back(handler.AddInstance(prefab2, tester));

					tester.location.x = 12.f * i;
					tester.location.z = 12.f * j;
					tester.location.y = 12.f * k;
					instances.emplace_back(handler.AddInstance(prefab3, tester));
				}
			}
		}

		handler.UpdateRendererScene();

		DataRegistrar<> data_reg;

		TaskScheduler& task_reg = *data_reg.Create<TaskScheduler>("Task_scheduler."_h32);
		task_reg.RegisterTask(HandleEvents, SGS_THREAD_AFFINITY::MainOnly, "TaskInput"_h32);

		task_reg.SortGraph();
		task_reg.GenPipelines();
		task_reg.StartWorkers();

		while (should_run) {
			//task_reg.ExecuteAll();
			handler.HandleEvents();

			handler.UpdateRendererScene();
		}
		task_reg.StopAllWorkers();

		data_reg.ClearAll();
	}

	void Shutdown()
	{
		should_run = false;
	}

	void HandleEvents()
	{
		SDL_Event test_event;
		static bool use_mouse = true;
		while (SDL_PollEvent(&test_event)) {
			switch (test_event.type) {
			case SDL_EventType::SDL_WINDOWEVENT: {
				switch (test_event.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE: {
					Shutdown();

					break;
				}
				default:
					break;
				}
				break;
			}

			case SDL_EventType::SDL_QUIT:
			{
				Shutdown();

				break;
			} case SDL_EventType::SDL_MOUSEMOTION: {
				if (use_mouse)
				{
					g_camera.RotateCameraX((float)-test_event.motion.yrel);
					g_camera.RotateCameraY((float)test_event.motion.xrel);
				}

				break;
			}case SDL_EventType::SDL_KEYDOWN: {
				switch (test_event.key.keysym.sym)
				{
				case SDLK_w: {
					g_camera.MoveForward(0.2f);
					break;
				}
				case SDLK_s: {
					g_camera.MoveBack(0.2f);
					break;
				}
				case SDLK_F11: {
					use_mouse = !use_mouse;
					break;
				}
				case SDLK_a: {
					break;
				}
				case SDLK_d: {
					break;
				}
				case SDLK_c: {
					break;
				}
				case SDLK_SPACE: {
					g_camera.MoveCameraY(0.2f);
					break;
				}

				case SDLK_PLUS: {
					if (test_event.key.keysym.mod == SDLK_LSHIFT || test_event.key.keysym.mod == SDLK_RSHIFT) {
						g_camera.m_lerp += 0.5f;
					}
				}
				case SDLK_F10: {
					trap_mouse = !trap_mouse;
					SDL_bool val = (SDL_bool)trap_mouse;
					SDL_SetRelativeMouseMode(val);
					break;
				}
				default:
					break;
				}
				break;
			}

			default:
				break;
			}
		}
	}
	void Start()
	{
		if (!has_started) {
			has_started = true;
			should_run = true;

			InitSystems();

			MainLoop();
		}
	}
}

#endif // !SP_USE_EDITOR