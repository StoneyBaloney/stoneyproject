#include "GLErrorHandler.h"

//if you want you can rename myCallback; This is just an example - Original Author
constexpr auto buffer_info_header = "Buffer info:";
void DebugOutput::myCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* msg, const void* data)
{
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		break;
	default:

		if (msg[0] == 'B' && msg[7] == 'i')
		{
			break;
		}

		std::string err = Concat(
			"Source: ", getStringForSource(source), "\n",
			"Type: ", getStringForType(type), "\n",
			"Severity: ", getStringForSeverity(severity), "\n",
			"Output: ", msg, "\n"
		);
		LogErrorToFile(err);
		break;
	}
}