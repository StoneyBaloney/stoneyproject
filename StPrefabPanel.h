#pragma once
#ifdef SP_USE_EDITOR

#include "PrefabPanelBase.h"
namespace StoneyCore {
	namespace Editor { class EditorSceneHandler; class EditorSceneManager; }
}
class StPrefabPanel : public PrefabPanelBase
{
	friend class EditorApp;
	friend class StoneyCore::Editor::EditorSceneHandler;
	friend class StoneyCore::Editor::EditorSceneManager;
public:
	StPrefabPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(471, 597), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~StPrefabPanel();
};

#endif // SP_USE_EDITOR
