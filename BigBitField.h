#pragma once

#include <stdint.h>
#include <vector>
#include<iostream>

template <unsigned int bitfield_size>
struct BigBitField {
	template <typename IntType = uint64_t>
	constexpr auto SectionIndex(IntType _bit) const {
		if (_bit)
		{
			return ((_bit - 1) % 64);
		}
		else {
			return IntType(0);
		}
	}
	template <typename IntType = size_t>
	constexpr IntType GetIndex(const uint64_t _bit) const {
		return  IntType((_bit - SectionIndex(_bit)) / float(bitfield_size) * Size());
	}

	void Clear() {
		for (auto& sec : m_bits) {
			sec = 0;
		}
	}

	static constexpr uint64_t Size() {
		constexpr auto ret = (bitfield_size / 64);
		return ret;
	};

	static_assert(bitfield_size % 64 == 0);
	template <typename IterType>
	void Insert(IterType begin, IterType end) {
		for (auto& i = begin; i != end; i++)
		{
			SetBit(uint64_t(*i));
		}
	}
	void SetBits(...) {
	}
	template <typename IntType>
	void SetBits(const IntType int_type) {
		SetBit(uint64_t(int_type));
	}
	template <typename IntType, typename Next, typename...Remaining>
	void SetBits(IntType first, Next next, Remaining...remaining) {
		SetBits(first);
		SetBits(next, remaining...);
	}

	template <typename...IntTypes>
	BigBitField(IntTypes...ints) {
		SetBits(ints...);
	}
	BigBitField() = default;
	BigBitField(unsigned int _bit) {
		size_t index = GetIndex(_bit);
		m_bits[index] = uint64_t(1) << SectionIndex(_bit);
	}
	void UnsetBit(const size_t _bit) {
		if (_bit == 0) {
			return;
		}
		auto ind = GetIndex(_bit);
		auto sec_ind = SectionIndex(_bit);
		auto& sec = m_bits[ind];
		sec &= static_cast<uint64_t>(~(uint64_t(1) << (sec_ind)));
	}
	void SetBit(const size_t _bit) {
		if (_bit == 0) {
			return;
		}
		auto ind = GetIndex(_bit);
		auto sec_ind = SectionIndex(_bit);
		auto& sec = m_bits[ind];
		sec = sec | int64_t(1) << (sec_ind);
	}

	BigBitField operator &(const BigBitField& other) {
		BigBitField ret;
		for (size_t i = 0; i < Size(); i++)
		{
			ret.m_bits[i] = other.m_bits[i] & m_bits[i];
		}
		return ret;
	}
	void operator |= (const BigBitField& other) {
		auto* m_sec = m_bits;
		for (auto& other_sec : other.m_bits)
		{
			*m_sec |= other_sec;
			m_sec++;
		}
	}
	void operator&=(const BigBitField& other) {
		auto* m_sec = m_bits;
		for (auto& other_sec : other.m_bits)
		{
			*m_sec &= other_sec;
			m_sec++;
		}
	}
	template <typename Other>
	bool AnyBitsSet(const Other& bitfield) {
		return *this & bitfield;
	}

	template <typename OtherBitfield, typename Next, typename...Remaining>
	bool AnyBitsSet(const OtherBitfield& first, const Next& next, const Remaining& ...remaining) {
		return AnyBitsSet(first) || AnyBitsSet(next, remaining...);
	}

	bool AllBitsSet(const BigBitField& other) {
		if (!other) {
			return true;
		}
		auto* m_sec = m_bits;
		for (auto& other_sec : other.m_bits)
		{
			if ((*m_sec & other_sec) != other_sec) {
				return false;
			}
			m_sec++;
		}
		return true;
	}
	template <typename IntType>
	uint64_t IsSet(IntType _bit) {
		return operator&(uint64_t(_bit));
	}
	BigBitField operator~() {
		BigBitField ret;
		size_t index = 0;
		for (auto& sec : m_bits) {
			ret.m_bits[index] = ~sec;
			index++;
		}
		return ret;
	}

	template <typename IntType = uint32_t>
	IntType LowestUnset() {
		size_t index = 0;

		for (auto& sec : m_bits) {
			auto&& val = (~sec);
			val = (val & ((~val) + 1));
			if (val) {
				auto ret = static_cast<IntType>((std::log2(val)) + (index * 64) + 1);
				return ret;
			}
			index++;
		}

		return IntType(bitfield_size) + 1;
	}
	BigBitField operator-() {
		BigBitField ret;
		size_t index = 0;
		for (auto& sec : m_bits) {
			ret.m_bits[index] = ~sec + 1;
			index++;
		}
		return ret;
	}
	class BitIterator {
	public:

		BitIterator(BigBitField<bitfield_size>& _bitfield, uint32_t index = bitfield_size + 1) :
			m_index(_bitfield.Next(index)),
			bitfield(_bitfield)
		{}
		BitIterator(const BigBitField<bitfield_size>& _bitfield, uint32_t index = bitfield_size + 1) :
			m_index(_bitfield.Next(index)),
			bitfield(_bitfield)
		{}

		BitIterator end() const {
			return BitIterator(bitfield);
		}
		BitIterator& operator++() {
			m_index = bitfield.Next(m_index + 1);
			return *this;
		}
		BitIterator& operator+(unsigned int val) {
			m_index = bitfield.Next(m_index + val);
			return *this;
		}
		const BitIterator& operator++() const {
			m_index = bitfield.Next(m_index + 1);
			return *this;
		}
		const BitIterator& operator+(unsigned int val) const {
			m_index = bitfield.Next(m_index + val);
			return *this;
		}

		bool operator!=(const BitIterator& other) const {
			return m_index != other.m_index;
		}
		uint32_t& operator*() { return m_index; }
		const size_t& operator*() const { return m_index; }
		uint32_t m_index{ bitfield_size + 1 };
	private:

		const BigBitField<bitfield_size>& bitfield;
	};
	uint32_t Next(const uint32_t _index) const {
		uint32_t first = GetIndex<uint32_t>(_index) * 64;
		for (uint32_t i = GetIndex<uint32_t>(_index); i < Size(); i++)
		{
			auto sec_ind = _index > first ? SectionIndex(_index) : 0;
			auto sec = m_bits[i] >> (sec_ind);
			sec = sec & (((~(sec)) + 1));
			if (sec)
			{
				auto ret = uint32_t(std::log2(sec)) + first + sec_ind + 1;
				return ret;
			}
			first += 64;
		}
		return bitfield_size + 1;
	}
	BitIterator begin() const {
		return BitIterator(*this, 1);
	}
	BitIterator end() const {
		return BitIterator(*this);
	}
	BigBitField operator|(const BigBitField& other) {
		auto ret = other;
		for (size_t i = 0; i < Size(); i++)
		{
			ret.m_bits[i] = m_bits[i] | other.m_bits[i];
		}
		return ret;
	}
	bool OpAnd(const BigBitField& other) {
		for (size_t i = 0; i < Size(); i++)
		{
			if (m_bits[i] & other.m_bits[i]) {
				return true;
			}
		}
		return false;
	}

	operator bool() const {
		for (auto sec : m_bits) {
			if (sec) {
				return true;
			}
		}
		return false;
	}
	uint64_t m_bits[bitfield_size / 64] = {};
};

using BitField128 = BigBitField<128>;