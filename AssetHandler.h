#pragma once
#include <filesystem>
#include <unordered_map>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "pch.h"
#include "EntityManager.h"
namespace StoneyCore {
	enum ASSET_LOAD_CODE {
		LOAD_CODE_success = 0,
		LOAD_CODE_invalid_filename,
		LOAD_CODE_invalid_format,
		LOAD_CODE_unknown_failure
	};

	class ShaderEntry {
		std::unordered_map<std::string, uint32_t> m_shaders;
		uint32_t m_next_id{ 0 };
	public:
		uint32_t GetShaderID(const std::string& filename) {
			std::filesystem::path vs_path(filename);
			std::string path_str = Concat(vs_path.parent_path().string(), "/", vs_path.filename().string());

			if (m_shaders.find(path_str) == m_shaders.end())
			{
				auto&& ret = m_next_id++;
				m_shaders[path_str] = ret;
				return ret;
			}
			return m_shaders[path_str];
		}
	};
	inline bool operator & (ShaderLoadInfo& first, ShaderLoadInfo& second) {
		std::filesystem::path vs_path = first.vs_filename;
		std::filesystem::path fs_path = first.fs_filename;

		std::filesystem::path other_vs_path = second.vs_filename;
		std::filesystem::path other_fs_path = second.fs_filename;

		bool ret = false;
		std::error_code err;
		if (first.fs_filename.size() && second.fs_filename.size())
		{
			ret = std::filesystem::equivalent(fs_path, other_fs_path, err) ? true : ret;

			if (err)
			{
				LogErrorToFile(Concat("Error comparing paths ", first.fs_filename, ", and ", second.fs_filename, ".\n"));
			}
		}
		if (first.vs_filename.size() && second.vs_filename.size())
		{
			ret = std::filesystem::equivalent(vs_path, other_vs_path, err) ? true : ret;
			if (err)
			{
				LogErrorToFile(Concat("Error comparing paths ", first.vs_filename, ", and ", second.vs_filename, ".\n"));
			}
		}

		return  ret;
	}
	class ShaderTracker {
		ShaderEntry m_vert_shaders;
		ShaderEntry m_frag_shader;
		uint32_t m_next_id{ 0 };
		std::map<std::pair<uint32_t, uint32_t>, uint32_t> m_program_ids;

	public:

		uint32_t GetProgramID(const std::string& vert_filename, const std::string& frag_filename) {
			auto&& vert_id = m_vert_shaders.GetShaderID(vert_filename);
			auto&& frag_id = m_frag_shader.GetShaderID(frag_filename);
			auto&& program_key = std::make_pair(vert_id, frag_id);

			if (m_program_ids.find(program_key) == m_program_ids.end())
			{
				m_program_ids[program_key] = m_next_id++;
			}
			return m_program_ids[program_key];
		}

		uint32_t GetProgramID(ShaderLoadInfo shader_stages) {
			return GetProgramID(shader_stages.vs_filename, shader_stages.fs_filename);
		}
	};

	struct AssetMeta
	{
		bool valid{ false };
		std::string filename;
	};

	//	using namespace Renderer;
	class AssetHandler {
	public:
		static bool LoadTextFileToString(std::string& output_string, const std::string& _filename);
		static bool LoadTextFileToString(std::string& output_string, const char* _filename);
		static void FreeTexture(StoneyCore::Texture& texture);
		static ASSET_LOAD_CODE LoadTexture(StoneyCore::Texture& texture, const char* _filename, int desired_components, std::string& warn);

		static bool GetTextureInfo(const char* filename, StoneyCore::TextureInfo& info);
		static bool GetTextureInfo(const char* filename, int* width, int* height, int* num_components);

		static bool LoadShaderToLoadMsg(ShaderLoadMessage& load_msg);
		static bool LoadShaderToLoadMsg(ShaderLoadMessage& load_msg, const char* vs_filename, const char* fs_filename);
		//static void LoadTextureInfos(stVector<TextureInfo>& out_infos,const stVector<StoneyCore::AssetMeta> texture_paths);
		static ASSET_LOAD_CODE LoadMesh(StoneyCore::ModelAsset& model_data, stVector<StoneyCore::AssetMeta>& texture_paths, const char* _filename, unsigned int flags);
		static void SaveTextOverwrite(const stString& filename, const stString& text);
	};
	class AssetCache
	{
		using ModelAsset = StoneyCore::ModelAsset;
		using TextureAsset = StoneyCore::Texture;

		AssetHandler m_handler;
		entt::registry m_assets;
		std::unordered_map<std::string, EntType> m_texture_map;
		std::unordered_map<std::string, EntType> m_model_map;

		void LogModelLoadError(const std::string& filename, std::string& err, std::string& warn)
		{
			LogErrorToFile(Concat("Asset handler failed to load model ", filename, "\nWarning", warn, ". \nError: ", err));
		}

	public:

		static auto GetFilenameWithParent(const std::string& filename) {
			std::filesystem::path vs_path(filename);

			return Concat(vs_path.parent_path().string(), "/", vs_path.filename().string());
		}

		auto GetValidAssetID(const std::string& file, std::unordered_map<std::string, EntType>& asset_map) {
			auto&& filename = GetFilenameWithParent(file);
			auto val = asset_map.find(filename);
			if (val == asset_map.end())
			{
				auto asset_id_ret = m_assets.create();
				auto& meta = m_assets.assign<StoneyCore::AssetMeta>(asset_id_ret);
				meta.filename = filename;
				asset_map[filename] = asset_id_ret;
				return asset_id_ret;
			}
			else {
				if (m_assets.valid((*val).second))
				{
					return (*val).second;
				}
				else {
					asset_map.erase(val);
					return GetValidAssetID(file, asset_map);
				}
			}
		}

		auto& GetTexture(const std::string& texture_filename) {
			auto&& asset_id = GetValidAssetID(texture_filename, m_texture_map);
			auto& meta = m_assets.get<StoneyCore::AssetMeta>(asset_id);
			auto& texture = m_assets.get_or_assign<StoneyCore::Texture>(asset_id);
			if (!meta.valid)
			{
				std::string err;
				if (m_handler.LoadTexture(texture, texture_filename.c_str(), 3, err))
				{
					LogErrorToFile(Concat("Failed to load texture ", texture_filename, "\nError: ", err, "\n"));
				}
				else {
					meta.valid = true;
				}
			}
			return texture;
		}

		StoneyCore::ModelAsset& GetModelAssets(const std::string& model_filename);
		AssetCache() {};
		~AssetCache() {
			m_assets.view<Texture>().each([](entt::entity entity, Texture& tex) {
				AssetHandler::FreeTexture(tex);
				tex.num_components = 0;
			});
		};

	private:
	};

	//void GetTextureInfo(const aiMaterial* material, aiTextureType& type, std::array<StoneyCore::TextureInfo, 8Ui64> & tex_infos, stVector<StoneyCore::AssetMeta>& out_asset_metas, const std::string& model_directory, StoneyCore::MeshData& out_mesh, StoneyCore::TextureInfo& tex_info);

	//void LoadMeshUVCoords(const aiMaterial* material, const aiMesh* mesh, StoneyCore::ModelVertices& out_vertices, const size_t& num_verts, float  insert_buffer[3]);
}