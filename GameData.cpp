#include <map>
#include <mutex>
#include "Transform.h"
#include "GameData.h"

namespace StoneyCore {
	RenderInfo::RenderInfo(const RenderInfo& _other) :
		shader_stages(_other.shader_stages),
		render_type(_other.render_type),
		vertex_type(_other.vertex_type)
	{
	}

	namespace Renderer {
	}
}