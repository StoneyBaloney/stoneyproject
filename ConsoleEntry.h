#pragma once
#ifdef _CONSOLE

#include "StoneyMain.h"

int main(int argc, char* argv[]) {
	StoneyMain::Start();
}
#endif // _CONSOLE