#pragma once
#ifdef SP_USE_EDITOR

#include <wx/wx.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/manager.h>
#include <wx/stc/stc.h>
#include <wx/aui/auibook.h>

class PropertyPanelBase : public wxPanel
{
private:

protected:
	wxAuiNotebook* m_main_notebook;

public:

	PropertyPanelBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(1213, 704), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~PropertyPanelBase();
};
#endif // SP_USE_EDITOR
