#pragma once
#include <vector>

template<typename DataType>
struct StoneyArray : stVector<DataType> {
	static constexpr auto IsContainer() { return true; }
};