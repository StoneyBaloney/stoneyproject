#pragma once
#ifdef SP_USE_EDITOR

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/dataview.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/manager.h>
#include <wx/propgrid/advprops.h>
#include <wx/aui/auibook.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class LightingPanelBase
///////////////////////////////////////////////////////////////////////////////
class LightingPanelBase : public wxPanel
{
private:

protected:
	wxAuiNotebook* m_auinotebook;
	wxPanel* m_scene_lights_panel;
	wxDataViewTreeCtrl* m_lighting_tree_data;
	wxChoice* m_light_types;
	wxPanel* m_lights_panel;
	wxPropertyGridManager* m_property_mgr;
	wxButton* m_create_light_button;
	wxButton* m_save_light_button;
	wxPanel* m_ctrl_panel;
	wxButton* m_add_light_button;
	wxChoice* m_light_choices;

public:

	LightingPanelBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(500, 535), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~LightingPanelBase();
};
#endif // SP_USE_EDITOR
