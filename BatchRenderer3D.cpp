#include <SDL_syswm.h>
#include "BatchRenderer3D.h"

#ifdef SP_USE_EDITOR

#include "EditorRenderUtils.h"

#endif // SP_USE_EDITOR

namespace StoneyCore {
	namespace Renderer {
		using namespace StoneyCore;
		void BatchRenderer3D::Start() {
			if (is_init && should_run == false)
			{
				Window::ClearContext(main_window, main_context);
				should_run = true;
				m_thread = std::thread([this]() {
					RenderLoop();
				});
			}
		}

		void BatchRenderer3D::ProcessBatchShaderState(const StoneyCore::Renderer::RenderBatch2& batch, StoneyCore::RenderState& batch_state)
		{
			auto render_type = batch.render_spec.render_type;

			switch (render_type)
			{			
			case StoneyCore::Renderer::BATCH_RENDER_TYPE::Deferred_Default:
			{
				if (program_id_map.find(batch.shader_id) != program_id_map.end())
				{
					batch_state.geometry_pass_id = shader_infos[program_id_map[batch.shader_id]].program_id;
				}

				break;
			}

			case StoneyCore::Renderer::BATCH_RENDER_TYPE::Forward_Custom: {
				if (program_id_map.find(batch.ShaderID()) == program_id_map.end())
				{
					program_id_map[batch.ShaderID()] = uint32_t(shader_infos.size());
					auto& shader_info = shader_infos.emplace_back();
					shader_info.program_id = CreateShader(batch.vertex_shader, batch.frag_shader);
				}
				batch_state.program_id = shader_infos[program_id_map[batch.ShaderID()]].program_id;
				break;
			}
			case StoneyCore::Renderer::BATCH_RENDER_TYPE::Deferred_Custom:
			default: {
				batch_state.program_id = 0; // no support, 0 will cause verify to batch not to be verified and thus not rendered

				break;
			}
			}
		}

		void BatchRenderer3D::ProcessBatch2(const Renderer::RenderBatch2& batch)
		{
			if (batch_id_map.find(batch.batch_id) == batch_id_map.end()) // If we havent seen this batch before, create the render state object
			{
				if (!batch.batch_verts.size())
				{
					return;
				}

				auto&& m_id = uint32_t(render_states.size());
				batch_id_map[batch.batch_id] = m_id;
				auto& render_state = render_states.emplace_back();
				render_state.batch_version = batch.batch_version;

				if (GenerateRenderState2(render_state, batch)) {
					valid_states.SetBit(m_id);
					valid_batch_indices.SetBit(batch.batch_id);
				}
				else {
					LogErrorToFile(Concat("Failed to create batch render state for batch number ", std::to_string(batch.batch_id), ".\n"));
				}
				ProcessBatchShaderState(batch, render_state);
			}
			else { // Otherwise respec the current render state object
				if (!batch.batch_verts.size())
				{
					valid_batch_indices.UnsetBit(batch.batch_id);
					valid_states.UnsetBit(batch_id_map[batch.batch_id]);
					return;
				}
				auto batch_index = batch_id_map[batch.batch_id];
				auto& render_state = render_states[batch_index];
				render_state.batch_version = batch.batch_version;
				CleanupRenderState(render_state);
				if (GenerateRenderState2(render_state, batch)) {
					valid_states.SetBit(batch_id_map[batch.batch_id]);
					valid_batch_indices.SetBit(batch.batch_id);
				}
				else {
					LogErrorToFile(Concat("Failed to create batch render state for batch number ", std::to_string(batch.batch_id), ".\n"));
					valid_batch_indices.UnsetBit(batch.batch_id);
					valid_states.UnsetBit(batch_id_map[batch.batch_id]);
				}
				ProcessBatchShaderState(batch, render_state);
			}
		}

		void CreateVertexBuffer2(RenderState& batch_state, const RenderBatch2& batch) {
			stVector<GLfloat> buffer;

			switch (batch.render_spec.vertex_type)
			{
			case VERTEX_SPEC_TYPE::NonIndexed:
			{
				GenerateNonIndexedVertexArray2(batch, buffer);
				batch_state.vbo_id = Renderer::CreateVBO(buffer, batch.render_spec.GetUpdateTypeGL(),batch.shader_params);
				break;
			}
			case VERTEX_SPEC_TYPE::Indexed: {
				stVector<GLuint> element_arr;
				GenerateIndexedVertexArray(batch, buffer);
				
				batch_state.vbo_id = Renderer::CreateVBO(buffer, batch.render_spec.GetUpdateTypeGL(),batch.shader_params);
				CreateElementArray(element_arr, batch);
				batch_state.element_buffer = CreateElementBuffer(element_arr);
				break;
			}
			default:
				break;
			}
		}
		GLuint CreateInstanceBuffer2(const RenderBatch2 batch) {
			return 0;
		}

		bool BatchRenderer3D::GenerateRenderState2(RenderState& batch_state, const RenderBatch2& batch)
		{
			
			batch_state.vao_id = CreateVAOAndBind();

			if (batch.render_spec.use_texture)
			{
				GenBatchTexture2(batch,batch_state);
			}
			
			CreateVertexBuffer2(batch_state, batch);


			batch_state.instance_vbo = CreateInstanceBuffer(batch);
			batch_state.render_type = batch.render_spec.draw_type;
			batch_state.update_type = batch.UpdateType();
			batch_state.num_renderables = uint32_t(batch.batch_verts.size());
			batch_state.batch_version = batch.batch_version;
			glBindVertexArray(0);
			return VerifyBatch2(batch_state, batch);
		}

		void BatchRenderer3D::CheckForBatches() {
			RenderBatch2* batch = nullptr;
			while (m_queues.batch_queue->try_dequeue(batch))
			{
				if (batch)
				{
					if (batch->batch_type == 1)
					{
						ProcessBatch2(*batch);
					}
					else {
					}
				}
				m_queues.free_batch_queue->enqueue(free_batch_ptok, batch);
			}
		}

		BatchRenderer3D::BatchRenderer3D(RendererConstArgs init_data) :

			m_queues(init_data),
			free_ptok(*init_data.free_buffer_queue),
			free_batch_ptok(*init_data.free_batch_queue)
		{
			if (!init_data.IsValid())
			{
				LogErrorToFile("Invalid arguments passed to renderer constructor. Ensure that all pointers to queues are valid.");
			}
		}

		void BatchRenderer3D::Init(RendererInitInfo init_info) {
			WindowInitInfo window_init;
			m_init_info = init_info;
			window_init.m_width = init_info.window_width;
			window_init.m_height = init_info.window_height;

			main_window = Window::MakeWindow(window_init);

			if (!main_window) {
				LogErrorToFile("Failed to create renderer window!");
				is_init = false;
				return;
			}

			main_context = Window::CreateContextAndMakeCurrent(main_window);

			if (!main_context) {
				LogErrorToFile("Failed to initialize main opengl context!");
				is_init = false;
				return;
			}
			is_init = true;
		}

#ifdef SP_USE_EDITOR

		HWND BatchRenderer3D::InitInEditor(RendererInitInfo init_info) {
			WindowInitInfo window_init;
			m_init_info = init_info;
			window_init.m_width = init_info.window_width;
			window_init.m_height = init_info.window_height;
			if (!SDL_WasInit(SDL_INIT_VIDEO))
			{
				SDL_Init(SDL_INIT_VIDEO);
			}

			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

			main_window = SDL_CreateWindow("StoneyGame", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL);

			SDL_SetWindowResizable(main_window, SDL_TRUE);
			main_context = SDL_GL_CreateContext(main_window);

			SDL_SysWMinfo wmInfo;
			SDL_VERSION(&wmInfo.version);
			SDL_GetWindowWMInfo(main_window, &wmInfo);
			HWND hwnd = wmInfo.info.win.window;
			is_init = true;
			return hwnd;
		}
#endif // SP_USE_EDITOR

		void BatchRenderer3D::DrawAll() {
		}

		void BatchRenderer3D::DrawBufferForward(StoneyCore::Renderer::RenderBuffer* render_buffer)
		{
			DrawBuffer_Forward(main_camera_ubo, render_buffer, valid_batch_indices, render_states);
#ifdef SP_USE_EDITOR
			DrawEditorGizmo_Forward(m_editor_widget_state, render_buffer, m_editor_widget_indirects);

#endif // SP_USE_EDITOR
		}
		void BatchRenderer3D::UpdateLightBuffer(StoneyCore::Renderer::RenderBuffer& render_buffer)
		{
			{
				const auto sizeof_point_light_buffer = sizeof(vec4) + sizeof(PointLightProperties);
				GLuint one_light = 1;
				PointLightProperties* lights{};
				if (render_buffer.m_dynamic_lighting.point_lights.size())
				{
					GLuint num_lights = GLuint(render_buffer.m_dynamic_lighting.point_lights.size());
					glBindBufferRange(GL_UNIFORM_BUFFER, 8, m_lighting_state.point_light_ubo, 0, sizeof_point_light_buffer);
					glBufferData(GL_UNIFORM_BUFFER, sizeof_point_light_buffer, NULL, GL_STREAM_DRAW);
					glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(GLuint), &num_lights);
					glBufferSubData(GL_UNIFORM_BUFFER, sizeof(vec4), num_lights * sizeof(PointLightProperties), &render_buffer.m_dynamic_lighting.point_lights[0]);
				}
				else {
					PointLightProperties light;
					light.ambient = glm::vec3(12, 12, 12);
					light.diffuse = glm::vec3(1, 1, 1);
					light.position = glm::vec3(0, 2, 0);
					light.m_point_light.x = 0.002f;
					light.m_point_light.y = 0.001f;
					light.m_point_light.z = 0.001f;
					glBindBufferRange(GL_UNIFORM_BUFFER, 8, m_lighting_state.point_light_ubo, 0, sizeof_point_light_buffer);
					glBufferData(GL_UNIFORM_BUFFER, sizeof_point_light_buffer, NULL, GL_STREAM_DRAW);
					glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(GLuint), &one_light);
					glBufferSubData(GL_UNIFORM_BUFFER, sizeof(vec4), sizeof(PointLightProperties), &light);
				}
			}
		}
		void BatchRenderer3D::UpdateCameraBuffers(StoneyCore::Renderer::RenderBuffer& render_buffer)
		{
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer.framebuffer_id);
			glUseProgram(m_pipeline_state.geometry_pass_shader);
			glBindBufferRange(GL_UNIFORM_BUFFER, 0, main_camera_ubo, 0, sizeof(glm::mat4) * 2);
			glBufferSubData(GL_UNIFORM_BUFFER, 0, 64, &render_buffer.camera_mvp[0][0]);
			glBufferSubData(GL_UNIFORM_BUFFER, 64, 64, &render_buffer.camera_view[0][0]);
		}
		void BatchRenderer3D::HandleRenderWindowResize(StoneyCore::Renderer::RenderBuffer& render_buffer)
		{
			if (m_init_info.window_width != render_buffer.screen_x || m_init_info.window_height != render_buffer.screen_y)
			{
				m_init_info.window_width = render_buffer.screen_x;
				m_init_info.window_height = render_buffer.screen_y;
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer.framebuffer_id);
				glBindTexture(GL_TEXTURE_2D, m_framebuffer.col_tex_id);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, render_buffer.screen_x, render_buffer.screen_y, 0, GL_RGBA_INTEGER, GL_INT, NULL);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				glBindTexture(GL_TEXTURE_2D, m_framebuffer.normal_tex_id);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, render_buffer.screen_x, render_buffer.screen_y, 0, GL_RGBA, GL_FLOAT, NULL);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_framebuffer.col_tex_id, 0);

				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_framebuffer.normal_tex_id, 0);

				glDrawBuffers(2, m_framebuffer.m_buffer_attachments);
				glBindRenderbuffer(GL_RENDERBUFFER, m_framebuffer.depth_buffer_id);
				glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, render_buffer.screen_x, render_buffer.screen_y);
				glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_framebuffer.depth_buffer_id);
				glViewport(0, 0, render_buffer.screen_x, render_buffer.screen_y);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			}
		}
		void BatchRenderer3D::UpdateAndDrawScene() {
			RenderBuffer* render_buffer_p;
			if (m_queues.render_buffer_queue->try_dequeue(render_buffer_p)) {
				if (render_buffer_p)
				{
					auto& render_buffer = *render_buffer_p;
					if (render_buffer.valid)
					{
						//Handle screen resize
						glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer.framebuffer_id);
						HandleRenderWindowResize(render_buffer);
						if (render_buffer.deferred)
						{
							glDisable(GL_DITHER);
							if (!m_pipeline_state.is_valid)
							{
								LogErrorToFile("Requested deferred shading, but deferred pipeline is invalid.");
								return;
							}
							
							UpdateLightBuffer(render_buffer);
							UpdateCameraBuffers(render_buffer);

							glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
							constexpr GLfloat clearcolor[] = { 0,0,0,0 };				
							
							
							for (auto& batch_data : render_buffer.indexed_batch_data) {
								if (batch_data.valid && valid_batch_indices.IsSet(batch_data.batch_id) && batch_data.indexed_indirects.size())
								{
									auto& render_state = render_states[batch_data.batch_id - 1];
									glUseProgram(render_state.geometry_pass_id);
									DrawBatchIndexed(render_state, batch_data);
								}
							}
							for (auto& batch_data : render_buffer.batch_data) {
								if (batch_data.valid && valid_batch_indices.IsSet(batch_data.batch_id) && batch_data.indirects.size())
								{
									auto& render_state = render_states[batch_data.batch_id - 1];

									UpdateAndDrawBatch(render_state, batch_data);
								}
							}

#ifdef SP_USE_EDITOR
							DrawEditorGizmo_Forward(m_editor_widget_state, render_buffer_p, m_editor_widget_indirects);

#endif // SP_USE_EDITOR
							glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
							glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

							//Lighting pass

							{
								GLuint tex = 0;
								for (auto& tex_id : m_framebuffer.m_buffer_textures) {
									glActiveTexture(GL_TEXTURE0 + tex);
									glBindTexture(GL_TEXTURE_2D, tex_id);
									tex++;
								}
							}
							glUseProgram(m_pipeline_state.lighting_pass_shader);

							glBindVertexArray(m_pipeline_state.screen_quad_vao);
							glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
							Window::Swap(main_window);
							glBindVertexArray(0);
							glActiveTexture(GL_TEXTURE0);
							glEnable(GL_DITHER);
						}
						else {
							DrawBufferForward(render_buffer_p);
							Window::Swap(main_window);
						}
					}
				}
				m_queues.free_buffer_queue->enqueue(render_buffer_p);
			}
		}
		void BatchRenderer3D::CheckForShaderUpdates() {
			if (m_queues.shader_queue->try_dequeue(shader_msg))
			{
				GLuint new_program_id = 0;
				if (program_id_map.find(shader_msg.program_id) == program_id_map.end())
				{
					program_id_map[shader_msg.program_id] = uint32_t(shader_infos.size());
					auto& shader_info = shader_infos.emplace_back();
					shader_info.program_id = CreateShader(shader_msg);
					new_program_id = shader_info.program_id;
				}
				else {
					auto& shader_info = shader_infos[program_id_map[shader_msg.program_id]];
					auto old_program_id = shader_info.program_id;
					new_program_id = CreateShader(shader_msg.vert_shader, shader_msg.frag_shader);
					shader_info.program_id = new_program_id;

					for (auto& render_state : render_states) {
						if (render_state.program_id == old_program_id)
						{
							render_state.program_id = shader_msg.shader_type == SHADER_PASS_TYPE::SG_Renderpass_Forward ? new_program_id: render_state.program_id;
							render_state.geometry_pass_id = shader_msg.shader_type == SHADER_PASS_TYPE::SG_Renderpass_Geometry ? new_program_id : render_state.geometry_pass_id;
						}
					}

					glDeleteProgram(old_program_id);
				}
				RenderCallback cb;
				cb.identifier = shader_msg.program_id;
				cb.msg_type = RENDER_CB_MSG_TYPE::LoadShader;
				if (shader_msg.shader_type == SHADER_PASS_TYPE::SG_Renderpass_Lighting)
				{
					m_pipeline_state.lighting_pass_shader = new_program_id;

				}
				if (m_pipeline_state.lighting_pass_shader && m_pipeline_state.screen_quad_vao && m_pipeline_state.m_framebuffer.framebuffer_id)
				{
					m_pipeline_state.is_valid = true;
				}
				else {
					m_pipeline_state.is_valid = false;
				}

				cb.msg = RENDER_CB_MSG::Sucess;
				m_queues.render_callback_queue->enqueue(cb);
			}
		}
		void BatchRenderer3D::RenderLoop() {
			auto buffers = 0;
			while (buffers < MAX_RENDER_BUFFERS)
			{
				RenderBuffer* buffer;
				if (m_queues.render_buffer_queue->try_dequeue(buffer))
				{
					m_queues.free_buffer_queue->enqueue(free_ptok, buffer);

					buffers++;
				}
			}

			if (!Window::MakeContextCurrent(main_window, main_context)) {
				return;
			}

#if _DEBUG
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif // NDEBUG
			if (glewInit())
			{
				return;
			}
#ifdef SP_USE_EDITOR
			glDebugMessageCallback(DebugOutput::myCallback, NULL);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
				GL_DONT_CARE, 0, NULL, GL_TRUE);
#endif // NDEBUG
			glewExperimental = true;
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glClearColor(0.0f, 0.0f, 0.0f, 1.f);
			glClearDepth(1.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glGenBuffers(1, &main_camera_ubo);
			glBindBuffer(GL_UNIFORM_BUFFER, main_camera_ubo);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * 2, NULL, GL_STREAM_DRAW);
			glBindBufferRange(GL_UNIFORM_BUFFER, 0, main_camera_ubo, 0, sizeof(glm::mat4) * 2);
#ifdef SP_USE_EDITOR
			{
				std::string vert_shader(Editor::GetDebugVertShader());
				std::string frag_shader(Editor::GetDebugFragShader());
				auto&& debug_prog = CreateShader(vert_shader, frag_shader);
				m_light_debug_state.program_id = debug_prog;
				m_editor_widget_state.program_id = debug_prog;
				GenLightingDebugState(m_light_debug_state);
				GenEditorGizmoBuffer(m_editor_widget_state, m_editor_widget_indirects);
			}
#endif // SP_USE_EDITOR
			InitDeferredFrameBufferGL(m_framebuffer, m_init_info);
			{
				auto screen_quad = GetNormalizedQuadBufferGL();
				GLuint screen_quad_vbo = 0;
				glGenVertexArrays(1, &m_pipeline_state.screen_quad_vao);
				glGenBuffers(1, &screen_quad_vbo);
				glBindVertexArray(m_pipeline_state.screen_quad_vao);
				glBindBuffer(GL_ARRAY_BUFFER, screen_quad_vbo);
				glBufferData(GL_ARRAY_BUFFER, sizeof(screen_quad[0]) * screen_quad.size(), &screen_quad[0], GL_STATIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
				glEnableVertexAttribArray(0);
				glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)12);
				glEnableVertexAttribArray(1);
				glBindVertexArray(0);
			}
			{// To be removed
				GLuint zero_lights = 0;
				constexpr auto sizeof_point_light_ub = sizeof(GLuint) + sizeof(PointLightProperties);
				glGenBuffers(1, &m_lighting_state.point_light_ubo);
				glBindBuffer(GL_UNIFORM_BUFFER, m_lighting_state.point_light_ubo);
				glBufferData(GL_UNIFORM_BUFFER, sizeof_point_light_ub, NULL, GL_STREAM_DRAW);
				glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(GLuint), &zero_lights);
				glBindBufferRange(GL_UNIFORM_BUFFER, 8, m_lighting_state.point_light_ubo, 0, sizeof(GLuint));
			}
			Window::Swap(main_window);
			while (should_run) {
				UpdateAndDrawScene();
				CheckForShaderUpdates();
				CheckForRenderTask();
				CheckForBatches();
			}
			for (auto& render_state : render_states) {
				CleanupRenderState(render_state);
			}
		}
		void BatchRenderer3D::CheckForRenderTask() {
			RendererTask task;

			while (m_queues.render_queue->try_dequeue(task))
			{
				task.m_task();
			}
		}
		void BatchRenderer3D::Shutdown() {
			should_run = false;
			if (m_thread.joinable())
			{
				m_thread.join();
			}
		}

		BatchRenderer3D::~BatchRenderer3D()
		{
			should_run = false;
			if (m_thread.joinable())
			{
				m_thread.join();
			}
		}
	}
}