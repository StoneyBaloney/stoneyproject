#pragma once
#include <stdint.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include"stVector.h"
using uint32 = uint32_t;

using string = std::string;

template <typename...Params>
using stVector = StoneyCore::stVectorWrapper<Params...>;

using stString = std::string;
template <typename...Params>
using stMap = std::map<Params...>;

template <typename...Params>
using stUnorderedMap = std::unordered_map<Params...>;

template <typename...Params>
using stSet = std::set<Params...>;