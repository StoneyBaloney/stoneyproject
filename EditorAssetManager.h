#pragma once
#ifdef SP_USE_EDITOR

//#include <ostream>
#include <refl.hpp>
#include <fstream>
#include <filesystem>
#include <unordered_map>
#include <entt/entity/registry.hpp>
#include "pch.h"
#include "Lighting.h"
#include "Serialization.h"
//namespace StoneyCore {
//	namespace Editor {
//
//		struct EditorAssetsSaveDefintion;
//	}
//}
//class Saver {
//public:
//	static void SaveAll(StoneyCore::Editor::EditorAssetsSaveDefintion& asset_manager) {
//		auto& am = asset_manager;
//		std::ofstream ofs("EditorAssets.staa");
//		boost::archive::binary_oarchive ar(ofs);
//
//		ar & asset_manager;
//
//
//
//	}
//};

namespace StoneyCore {
	namespace Editor {
		struct EditorAssetsSaveDefintion {
			std::map<std::string, uint32>		m_point_light_map;
			stVector<DirectionalLightProperties>	m_point_lights;
			std::map<std::string, uint32>		m_dir_light_map;
			stVector<DirectionalLightProperties>		m_dir_lights;
			std::map<std::string, uint32>		m_prefab_map;
			stVector<PrefabDescription>		m_prefabs;
		};
		struct EditorAssetRegistryArchive {
			std::map<std::string, entt::entity>		m_point_light_map;
			std::map<std::string, entt::entity>		m_dir_light_map;
			std::map<std::string, entt::entity>		m_prefab_map;
		};

		class EditorAssetManager
		{
		public:
			EditorAssetManager();
			~EditorAssetManager();
		private:
			// Lights
			EditorAssetsSaveDefintion m_assets;

		public:

			bool SaveAll() {
				//	Saver::SaveAll(m_assets);
			}

			bool SavePrefab(std::string& prefab_name);;

			bool GetPrefab(std::string& prefab_name, PrefabDescription& out_prefab);

			bool TryAddPrefab(PrefabDescription& prefab);

		private:

			friend class Serializer;
			friend class boost::serialization::access;
		};
	}
}
REFL_AUTO(
	type(StoneyCore::Editor::EditorAssetsSaveDefintion),
	field(m_point_light_map),
	field(m_point_lights),
	field(m_dir_light_map),
	field(m_dir_lights),
	field(m_prefab_map),
	field(m_prefabs)
)

namespace boost {
	namespace serialization {
		ST_SERIALIZABLE(StoneyCore::Editor::EditorAssetsSaveDefintion);
	}
}
#endif // SP_USE_EDITOR
