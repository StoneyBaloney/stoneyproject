#ifndef SP_USE_EDITOR
#include <magic_enum.hpp>

#include "SceneHandler.h"
namespace StoneyCore {
	void SceneHandler::HandleEvents()
	{
		SDL_Event test_event;
		static bool use_mouse = true;

		while (SDL_PollEvent(&test_event)) {
			switch (test_event.type) {
			case SDL_EventType::SDL_WINDOWEVENT: {
				switch (test_event.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE: {
					this->ShutdownRenderer();
					m_exit_callback();
					break;
				}
				case SDL_WINDOWEVENT_RESIZED: {
					HandleRenderWindowResize(test_event.window.data1, test_event.window.data2);
					break;
				}
				default:
					break;
				}
				break;
			}

			case SDL_EventType::SDL_QUIT:
			{
				this->ShutdownRenderer();

				break;
			} case SDL_EventType::SDL_MOUSEMOTION: {
				if (use_mouse)
				{
					main_camera.RotateCameraX((float)-test_event.motion.yrel);
					main_camera.RotateCameraY((float)test_event.motion.xrel);
				}

				break;
			}
			case SDL_EventType::SDL_MOUSEBUTTONDOWN: {
				int x, y;
				SDL_GetMouseState(&x, &y);

				if (x <= m_screen_settings.width && y <= m_screen_settings.height)
				{
				}

				break;
			}
			case SDL_EventType::SDL_KEYDOWN: {
				switch (test_event.key.keysym.sym)
				{
				case SDLK_w: {
					main_camera.MoveForward(0.2f);
					break;
				}
				case SDLK_s: {
					main_camera.MoveBack(0.2f);
					break;
				}
				case SDLK_F11: {
					use_mouse = !use_mouse;
					break;
				}
				case SDLK_a: {
					break;
				}
				case SDLK_d: {
					break;
				}
				case SDLK_c: {
					break;
				}
				case SDLK_SPACE: {
					main_camera.MoveCameraY(0.2f);
					break;
				}

				case SDLK_PLUS: {
					if (test_event.key.keysym.mod == SDLK_LSHIFT || test_event.key.keysym.mod == SDLK_RSHIFT) {
						main_camera.m_lerp += 0.5f;
					}
				}
				case SDLK_F10: {
					trap_mouse = !trap_mouse;
					SDL_bool val = (SDL_bool)trap_mouse;
					SDL_SetRelativeMouseMode(val);
					break;
				}
				default:
					break;
				}
				break;
			}

			default:
				break;
			}
		}
	}

	void SceneHandler::SetTransform(const InstanceHandle& instance_handle, const Transform& transform) {
		auto& batch_index = instance_handle.prefab_handle.batch_index;
		auto& renderable_index = instance_handle.prefab_handle.renderable_index;
		auto& instance_index = instance_handle.instance_index;

		m_graph.batches[batch_index].renderables[renderable_index].transforms[instance_index].m_transform = transform;
		m_graph.batches[batch_index].renderables[renderable_index].mats[instance_index] = glm::translate(glm::mat4(1), transform.location);
	}

	void SceneHandler::WriteSceneToRenderBuffer(StoneyCore::Renderer::RenderBuffer* buffer)
	{
		uint32_t next = 0;
		uint32_t batch_index = 0;
		main_camera.SetProjectionMat(buffer->camera_mvp);
		buffer->screen_x = m_screen_settings.width;
		buffer->screen_y = m_screen_settings.height;

		if (m_graph.version != buffer->version)
		{
			buffer->batch_data.resize(m_graph.batches.size());

			auto batch_index = 0;
			for (auto& batch : buffer->batch_data) {
				auto& m_batch = m_graph.batches[batch_index];
				if (batch.version != m_batch.version) {
					if (!m_batch.renderables.size())
					{
						batch.indirects.clear();
						batch.transforms.clear();
						batch.indirects.shrink_to_fit();
						batch.transforms.shrink_to_fit();
					}
					batch.indirects.resize(m_batch.renderables.size());
					batch.batch_id = m_batch.id;
					for (auto& renderable : m_batch.renderables) {
						batch.transforms.resize(renderable.mats.size());
						batch.transforms.shrink_to_fit();
					}
				}
				batch_index++;
			}
			buffer->batch_data.shrink_to_fit();
			buffer->version = m_graph.version;
		}
		batch_index = 0;
		for (auto& batch : buffer->batch_data) {
			auto& m_batch = m_graph.batches[batch_index];

			batch.batch_version = m_batch.batch_version;
			if (batch.version != m_batch.version) {
				auto base_instance = 0;
				auto renderable_index = 0;
				auto offset = 0;
				batch.transforms.clear();
				auto itr = batch.transforms.begin();
				for (auto& renderable : m_batch.renderables) {
					batch.transforms.insert(itr, renderable.mats.begin(), renderable.mats.end());
					itr = batch.transforms.end();
					auto& indirect = batch.indirects[renderable_index];

					indirect.baseInstance = base_instance;
					indirect.count = renderable.num_verts;
					indirect.first = offset;
					indirect.instanceCount = GLuint(renderable.mats.size());
					base_instance += GLuint(renderable.mats.size());
					renderable_index++;
					offset += renderable.num_verts;
				}
				batch.version = m_batch.version;
			}
			batch_index++;
		}

		buffer->valid = true;
	}

	void SceneHandler::UpdateRendererScene() {
		RenderBuffer* buffer = nullptr;

		if (m_queues.free_buffer_queue->try_dequeue(buffer))
		{
			if (!buffer)
			{
				return;
			}

			WriteSceneToRenderBuffer(buffer);
			m_queues.render_buffer_queue->enqueue(m_renderer_handle.buffer_ptok, buffer);
		}
	}

	InstanceHandle SceneHandler::AddInstance(uint32_t prefab_id, const Transform transform) {
		if (pending_prefab_ids.find(prefab_id) != pending_prefab_ids.end())
		{
			SortBatches();
			SubmitPendingBatches();
		}
		if (pending_prefab_ids.find(prefab_id) != pending_prefab_ids.end())
		{
			return InstanceHandle();
		}
		if (prefab_ids.IsSet(prefab_id))
		{
			const auto& instance = m_batch_map[prefab_id];
			auto batch_index = instance.batch_index;
			auto renderable_index = instance.renderable_index;
			auto& transforms = m_graph.batches[batch_index].renderables[renderable_index].transforms;
			auto& mats = m_graph.batches[batch_index].renderables[renderable_index].mats.emplace_back(glm::translate(glm::mat4(1), transform.location));
			auto& node = transforms.emplace_back();

			node.id = m_next_instance_id++;
			node.m_transform = transform;
			auto model_id = m_model_map[m_prefab_descriptions[prefab_id - 1].model_filename];
			auto& instance_handle = m_instance_map[node.id];
			instance_handle.prefab_handle = instance;
			instance_handle.instance_index = uint32_t(transforms.size() - 1);
			instance_handle.physics_handle = m_physics_world->AddInstance(m_batch_map[prefab_id].m_physics_handle, transform);
			instance_handle.prefab_handle.prefab_index = prefab_id - 1;
			m_physics_instance_map[instance_handle.physics_handle] = node.id;

			return instance_handle;
		}

		return InstanceHandle();
	}

	uint32_t SceneHandler::AddPrefab(PrefabDescription& prefab) {
		if (m_prefab_map.find(prefab.name) == m_prefab_map.end())
		{
			auto&& id = uint32_t(m_prefab_descriptions.size() + 1);

			if (id < *prefab_ids.end() && !prefab_ids.IsSet(id))
			{
				prefab_ids.SetBit(id);
				m_prefab_map[prefab.name] = id - 1;
				m_prefab_descriptions.emplace_back(prefab);
				pending_prefab_ids.emplace(id);

				if (prefab.render_spec.use_texture)
				{
					if (m_texture_infos.find(prefab.texture_filename) == m_texture_infos.end())
					{
						info;
						if (asset_handler.GetTextureInfo(prefab.texture_filename.c_str(), info))
						{
							m_texture_infos[prefab.texture_filename] = info;
						}
					}
				}
				//#ifdef SP_USE_EDITOR
				//
				//			/*	fu2::function<void(wxPropertyGridEvent&)> prop_change_func = [this](wxPropertyGridEvent& evt) {
				//					HandlePropertyChangeEvent(evt);
				//				};*/
				//
				//
				//				auto page = new StPropertyPage();
				//				m_editor_prop_mgr->AddPage(wxEmptyString, wxNullBitmap,page);
				//				m_editor_prefab_page_map[page] = uint32(m_prefab_descriptions.size() - 1);
				//
				//				page->Bind(wxEVT_PG_CHANGED, [this,page](wxPropertyGridEvent& evt) {
				//					HandlePropertyChangeEvent(page);
				//				});
				//
				//
				//
				//				StGenerateProperties(page,prefab);
				//				m_editor_property_pages.emplace_back(page);
				//
				//#endif // SP_USE_EDITOR

				return id;
			}
		}

		return 0;
	}

	SceneHandler::SceneHandler(fu2::function<void(void)> exit_callback) :
		m_renderer_handle(data_reg),
		m_exit_callback(exit_callback)
	{
		if (m_renderer_handle.m_queues.IsValid())
		{
			m_batch_renderer = data_reg.Create<Renderer::BatchRenderer3D>(
				"m_batch_renderer"_h32, m_renderer_handle.m_queues);
		}
		if (!m_batch_renderer)
		{
			LogErrorToFile("Failed to start renderer!");
		}

		for (auto& buffer : m_renderer_handle.m_render_buffers) {
			if (!buffer)
			{
				LogErrorToFile("Failed to create render buffers!");
			}
		}

		m_physics_world = data_reg.Create<StPxPhysics::StPxWorld>("px_physics"_h32);

		m_models.reserve(1000);
	}

	void SceneHandler::LoadModelToBatch(RenderBatch2* batch, const PrefabDescription& prefab_description)
	{
		//auto& model = batch->model_datas.emplace_back();

		auto& m_model = m_models[m_model_map[prefab_description.model_filename]];
		auto& batch_model = batch->model_datas.emplace_back(m_model);
		auto& model_info = batch->model_infos.emplace_back();
		model_info.num_verts = 0;
		for (auto& shape : batch_model.shapes) {
			model_info.num_verts += uint32_t(shape.mesh.indices.size());
		}

		batch->total_verts += model_info.num_verts;
	}

	void SceneHandler::LoadTextureToBatch(RenderBatch2* batch, const PrefabDescription& prefab_description)
	{
		if (textures.find(prefab_description.texture_filename) == textures.end())
		{
			auto& tex = textures[prefab_description.texture_filename];
			std::string warn{};
			if (asset_handler.LoadTexture(tex, prefab_description.texture_filename.c_str(), 4, warn)) {
				LogErrorToFile(warn);

				if (asset_handler.LoadTexture(tex, "Textures/StoneyDefaultTexture.bmp", 4, warn))
				{
					FatalError(warn);
				}
				stVector<StoneyCore::ModelFileData> test_models;
				stVector<StoneyCore::TextureInfo> test_texture_infos;
				auto ret = AssetHandler::LoadMesh(test_models, test_texture_infos, "Models/Cube.obj", aiProcess_Triangulate);

				LogErrorToFile(std::to_string(ret));
			}
		}
		batch->textures.emplace_back(textures[prefab_description.texture_filename]);
	}

	bool SceneHandler::SortBatches() {
		for (const auto prefab_id : pending_prefab_ids) {
			AddPrefabToBatchSystem(prefab_id);
		}
		pending_prefab_ids.clear();

		return true;
	}

	bool SceneHandler::IsBatchCompatible(RenderBatch2* batch, const PrefabDescription& description) {
		if (batch->render_spec.use_texture != description.render_spec.use_texture)
		{
			return false;
		}
		else if (description.render_spec.use_texture == true) {
			if (m_texture_infos.find(description.texture_filename) != m_texture_infos.end())
			{
				if (!(batch->texture_info == m_texture_infos[description.texture_filename])) {
					return false;
				}
			}
			else {
				LogErrorToFile("No texture info exist for prefab.");
				return false;
			}
		}
		if (batch->shader_id != m_shader_tracker.GetProgramID(description))
		{
			return false;
		}
		return batch->render_spec == description.render_spec;
	}
	RenderBatch2* SceneHandler::AddPrefabToBatchSystem(const uint32_t& prefab_id)
	{
		bool found = false;
		const auto prefab_index = prefab_id - 1;
		const auto& prefab = m_prefab_descriptions[prefab_id - 1];
		if (m_model_map.find(prefab.model_filename) == m_model_map.end())
		{
			auto& load_model = m_models.emplace_back();
			auto&& model_index = uint32(m_models.size()) - 1;
			m_model_map[prefab.model_filename] = model_index;
			std::string warn, err;
			if (!asset_handler.LoadModel(load_model, prefab.model_filename.c_str(), warn, err))
			{
				warn += " ";
				warn += err;
				LogErrorToFile(warn);
				return nullptr;
			}
		}
		auto&& model_index = m_model_map[prefab.model_filename];
		if (m_model_phys_handle_map.find(model_index) == m_model_phys_handle_map.end())
		{
			StPxPhysics::StPxObjectParams params;
			m_model_phys_handle_map[model_index] = m_physics_world->AddObject(m_models[model_index], params);
		}
		auto& batch_info = m_batch_map[prefab_id];
		batch_info.m_physics_handle = m_model_phys_handle_map[m_model_map[prefab.model_filename]];
		// Check existing batches for a compatibile one and if so, add the prefab to one
		for (const auto& batch_id : batch_ids) {
			auto batch_index = batch_id - 1;
			auto batch = batches[batch_index];

			if (batch) {
				auto& prefab_description = m_prefab_descriptions[prefab_index];

				if (IsBatchCompatible(batch, prefab)) {
					AddPrefabToBatch(batch, prefab);
					found = true;
					pending_batches.emplace_back(batch);
					m_graph.batches[batch_index].renderables.emplace_back().num_verts = batch->model_infos.back().num_verts;
					batch_info.renderable_index = uint32_t(m_graph.batches[batch_index].renderables.size() - 1);
					batch_info.batch_index = batch_index;

					return batch;
				}
			}
			else {
				LogErrorToFile("Looks like we lost a batch!");
			}
		} // No compatible batch was found, so make a new one and add the prefab to that one
		if (!found)
		{
			uint32_t batch_id = uint32_t(batches.size() + 1);

			batch_ids.SetBit(batch_id);
			auto* batch = data_reg.Create<RenderBatch2>(batch_id);
			if (batch)
			{
				batch->batch_id = batch_id;

				InitBatchFromPrefab(batch, prefab);
				pending_batches.emplace_back(batch);
				batches.emplace_back(batch);
				m_graph.batches.emplace_back().renderables.emplace_back().num_verts = batch->model_infos.back().num_verts;
				m_graph.batches.back().id = batch_id;
				batch_info.renderable_index = 0;
				batch_info.batch_index = uint32_t(m_graph.batches.size() - 1);
			}
			return batch;
		}
		return nullptr;
	}
	void DefaultExitCB() {
	}
}

#endif // !SP_USE_EDITOR