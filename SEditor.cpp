#ifdef SP_USE_EDITOR

#include <wx/nativewin.h>
#include<function2/function2.hpp>
#include "SEditor.h"
#include <SDL_syswm.h>
#include "ShaderGenerator.h"
wxIMPLEMENT_APP(EditorApp);

//int main(int argc, char *argv[]) {
//
//
//
//
//
//	return 0;
//
//}
//helper for aui panes- standard pane for editor window (not render window)
//
//  wxAuiPaneInfo() .PinButton( true ).Float().Resizable().BestSize( wxSize( 300,400 ) ).MinSize( wxSize( 106,64 ) ).Layer( 1 )
//
//
void IdleCallback(wxIdleEvent& evt) {
}
void EditorApp::ClickPane(wxAuiManagerEvent& evt) {
}
void EditorApp::ToggleGameWindowDock(wxSizeEvent& evt) {
	if (m_wx_main_window->m_mgr.GetPane(wxString("Game_Window")).IsFloating())
	{
	}
}
void EditorApp::EditorFocus(wxCommandEvent& evt) {
	//m_editor_panel->SetFocus();
}
void EditorApp::RenderTimerCallback(wxTimerEvent& evt) {
	scene_handler.UpdateAll();
}
void EditorApp::IdleEvent(wxIdleEvent& evt)
{
	scene_handler.UpdateAll();
	evt.RequestMore(true);
	evt.Skip(false);
}
void EditorApp::GameViewResize(wxSizeEvent& evt) {
	//	m_editor_panel_dummy->Fit();
}
bool EditorApp::OnInit() {
	using namespace StoneyCore;
	using namespace Renderer;

	{
		using namespace std::filesystem;
		path project_dir = SDL_GetBasePath();
		path new_dir = project_dir.parent_path().parent_path().parent_path();
		new_dir.append("StoneyProject");
		current_path(new_dir);
	}
	m_wx_main_window = reg_managed.Create< StoneyEditorMain>("m_main_frame"_h32);

	auto text_logger = reg_managed.Create < wxTextCtrl>("debug_ctrl"_h32, m_wx_main_window, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxHSCROLL | wxTE_READONLY);
	text_logger->SetEditable(false);
	m_output_redirector = data_reg.Create <wxStreamToTextRedirector>("debug_redir"_h32, text_logger);

	int width{ 800 }, height{ 600 };
	Renderer::RendererInitInfo renderer_init = { width , height };

	{// Windows only
		game_hwnd = scene_handler.InitRendererEditor(renderer_init);

		m_editor_panel = new wxNativeContainerWindow(game_hwnd);

		m_editor_panel->Reparent(m_wx_main_window);

		SetWindowLong(game_hwnd, GWL_STYLE, GetWindowLong(game_hwnd, GWL_STYLE) & ~(WS_POPUP | WS_CAPTION | WS_BORDER | WS_SIZEBOX));
	}

	m_wx_main_window->Show();

	m_wx_main_window->m_mgr.AddPane(m_editor_panel, wxAuiPaneInfo().Name("TestPanel").CloseButton(false).PinButton(true).Movable(true));
	m_wx_main_window->m_mgr.AddPane(text_logger, wxAuiPaneInfo().Left().Caption(wxT("Debug Output")).CloseButton(false).MaximizeButton(true).PinButton(true).Dock().Resizable().FloatingSize(wxSize(330, 400)).BestSize(wxSize(330, 400)).MinSize(wxSize(50, -1)).Layer(1));
	m_wx_main_window->m_mgr.Update();
	scene_handler.InitPhysics();

	scene_handler.StartRenderer();
	fu2::function<void(wxTimerEvent&)> UpdateFunc = [this](wxTimerEvent& evt) {
		RenderTimerCallback(evt);
	};
	fu2::function<void(wxIdleEvent&)> UpdateFunc2 = [this](wxIdleEvent& evt) {
		IdleEvent(evt);
	};
	fu2::function<void(wxCommandEvent&)>  EditorClick = [this](wxCommandEvent& evt) {
		EditorFocus(evt);
	};
	fu2::function<void(wxAuiManagerEvent&)>  PaneSelect = [this](wxAuiManagerEvent& evt) {
		ClickPane(evt);
	};

	m_editor_panel->Bind(wxEVT_COMMAND_LEFT_CLICK, EditorClick, m_editor_panel->GetId());
	m_wx_main_window->m_mgr.Connect(wxEVT_AUI_PANE_ACTIVATED, wxAuiManagerEventHandler(EditorApp::ClickPane), NULL, this);
	m_wx_main_window->m_mgr.SetEvtHandlerEnabled(true);

	m_render_timer = data_reg.Create <StUpdateTimer>("render_timer"_h32);
	m_render_timer->SetOwner(this);
	Bind(wxEVT_TIMER, UpdateFunc);
	Bind(wxEVT_IDLE, UpdateFunc2);
	m_render_timer->Start(0);

	scene_handler.SetPropertyPanel(m_wx_main_window->m_property_panel);

	// auto ads = false;	nobody likes ads

	//Code below is just programmer art, will be replaced by loading a scene, new project prompt, etc.

	
		


	RenderSpec render_spec1;

	render_spec1.uses_material = false;
	render_spec1.use_texture = true;
	render_spec1.vertex_type = VERTEX_SPEC_TYPE::Indexed;

	StoneyCore::PrefabDescription my_prefab;
	my_prefab.name = "my awesome prefab";
	my_prefab.model_filename = "../extern/SponzaModel/sponza.obj";
	my_prefab.render_spec = render_spec1;

	StoneyCore::PrefabDescription my_prefab2;
	my_prefab2.name = "my awesome prefab2";
	my_prefab2.model_filename = "../extern/SpaceStation/obj/Space Station Scene.obj";
	my_prefab2.render_spec = render_spec1;

	auto sponza_test = scene_handler.AddPrefab(my_prefab);
	//auto space_station_test = scene_handler.AddPrefab(my_prefab2);
	StoneyCore::Transform origin;
	origin.location = glm::vec3(0, 0, 0);
	scene_handler.AddInstance(sponza_test, origin);

	origin.location += vec3(5000, 0, 0);
	//scene_handler.AddInstance(space_station_test, origin);

	scene_handler.SubmitPendingBatches();

	return true;
}

#endif // SP_USE_EDITOR