#pragma once
#include <type_traits>
#include "Defines.h"
#include "WorkerThread.h"
#include "DataManager.h"
#include "Tasks.h"
#include "TaskDag.h"

enum class SCHEDULER_STATE {
	Ready,
	Errors,
	Incomplete
};

void ErrorMsg(const char*);;
using PipelinePhase = stVector<Task>;
class TaskScheduler
{
	DAG m_graph;
	TaskIDField tasks;
	DataIDField data_ids;
	DataRegistrar<> data_reg;
	std::atomic<bool> should_run{ false };
	stVector<ALIGNED_(64) std::atomic<unsigned int>> m_callbacks_arr{};
	stVector <fu2::function<void()>> task_funcs;
	stVector<TaskInfo> m_infos;
	stVector<std::shared_ptr<WorkerThread>> workers;
	stVector< PipelinePhase> relaxed_tasks;
	stVector<PipelinePhase> main_tasks;
	std::mutex pipeline_lock;
	TaskQueue* m_relaxed_q{ nullptr };
	std::unordered_map<uint32_t, uint32_t> task_id_map;
	std::unordered_map<uint32_t, uint32_t> data_id_map;
	SCHEDULER_STATE status{ SCHEDULER_STATE::Incomplete };
	void(*PipelineErrorCB)(const char*) { ErrorMsg };

public:
	TaskScheduler(const uint32_t num_threads = 1);
	bool SortGraph();
	void GenPipelines();;
	void StartWorkers() {
		for (auto& worker : workers) {
			worker->Start(m_relaxed_q);
		}
	}
	void ExecuteAll() {
		if (!m_relaxed_q) {
			return;
		}
		bool in_flight = true;
		bool relaxed_complete = false;
		bool main_complete = false;
		size_t stage = 0;
		auto& relaxed = relaxed_tasks;

		while (in_flight) {
			if (stage < relaxed_tasks.size()) {
				auto& relaxed_stage = relaxed_tasks[stage];

				m_relaxed_q->enqueue_bulk(relaxed_stage.begin(), relaxed_stage.size());
			}
			else {
				relaxed_complete = true;
			}

			if (stage < main_tasks.size())
			{
				auto& main_stage = main_tasks[stage];

				for (auto& task : main_stage) {
					task();
				}
			}
			else {
				main_complete = true;
			}

			if (!relaxed_complete) {
				auto& relaxed_stage = relaxed_tasks[stage];
				unsigned int my_counter = 0;
				auto CountCallbacks = [&]() {
					unsigned int counter = 0;
					for (auto& cb_counter : m_callbacks_arr) {
						counter += cb_counter;
					}
					return counter;
				};
				while (my_counter < relaxed_stage.size()) {
					my_counter += CountCallbacks();

					Task task;
					while (m_relaxed_q->try_dequeue(task)) {
						task();
						my_counter++;
					}
				}
				for (auto& cb_counter : m_callbacks_arr) {
					cb_counter = 0;
				}
			}
			else {
				relaxed_complete = true;
			}
			stage++;
			if (relaxed_complete && main_complete)
			{
				in_flight = false;
			}
		}
	}

	void StopAllWorkers() {
		should_run = false;
		for (auto& worker : workers) {
			if (worker) {
				worker->Join();
			}
		}
	}

	void SetErrorMsg(void(*_PipelineErrorCB)(const char*)) {
		PipelineErrorCB = _PipelineErrorCB;
	}
	template <typename...>
	void SetUsageBits(...) {
	}

	template <typename Data_ID>
	void SetUsageBits(DataIDField& read_field, DataIDField& write_field) {
		if constexpr (Data_ID::AccessType() == SGS_ACCESS_TYPE::ReadOnly) {
			read_field.SetBit(GetOrMakeDataID(Data_ID::Name()));
		}
		else {
			write_field.SetBit(GetOrMakeDataID(Data_ID::Name()));
		}

		if (!data_reg.GetData<Data_ID::m_type>(Data_ID::Name())) {
			data_reg.Create<Data_ID::m_type>(Data_ID::Name());
		}
	}
	template <typename First, typename Next, typename...Remaining>
	void SetUsageBits(DataIDField& read_field, DataIDField& write_field) {
		SetUsageBits<First>(read_field, write_field);
		SetUsageBits<Next, Remaining...>(read_field, write_field);
	}

	template <typename Callable, typename...DataTypes>
	auto GetTaskLambda(Callable callable, DataTypes* ...data) {
		auto ret = [=]() {
			callable(*data...);
		};

		return ret;
	}

	uint32_t GetTaskID(uint32_t task_id) {
		if (task_id_map.find(task_id) == task_id_map.end()) {
			auto&& ret = tasks.LowestUnset<uint32_t>();
			task_id_map[task_id] = ret;
			return ret;
		}
		return task_id_map[task_id];
	}

	uint32_t GetOrMakeDataID(const uint32_t hash_name) {
		if (data_id_map.find(hash_name) == data_id_map.end())
		{
			auto&& ret = data_ids.LowestUnset();
			data_id_map[hash_name] = ret;
			return ret;
		}
		else return data_id_map[hash_name];
	}

	template <typename...DataIDs, typename...Dependencies, typename Callable>
	bool RegisterTask(Callable func, const SGS_THREAD_AFFINITY thread_affinity, uint32_t task_id, Dependencies...dependencies) {
		static_assert(std::is_invocable<decltype(func), decltype(AccessHelper<DataIDs::m_type, DataIDs::AccessType()>::AddConstToReadOnly(*data_reg.GetData<DataIDs::m_type>(DataIDs::Name())))...>::value,
			"Task function parameters must be const for read only access type."
			);

		auto&& id = GetTaskID(task_id);
		if (id < *tasks.end() && !tasks.IsSet(id)) {
			tasks.SetBit(id);
			TaskInfo info;

			info.dependencies.SetBits(GetTaskID(dependencies)...);
			info.m_id = id;
			info.thread_affinity = thread_affinity;
			SetUsageBits<DataIDs...>(info.m_readable_data_ids, info.m_writable_data_ids);

			info.func = GetTaskLambda(func, data_reg.GetData<DataIDs::m_type>(DataIDs::Name())...);

			task_id_map[task_id] = id;
			if (m_infos.size() == id - 1) {
				m_infos.emplace_back(info);
			}
			else {
				m_infos[id - 1] = info;
			}
			return true;
		}
		return false;
	}

	void TestTask(TASK_ID id) {
		m_infos[id - 1].func();
	}
};
