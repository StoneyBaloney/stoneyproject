#ifdef SP_USE_EDITOR

#include "PrefabPanelBase.h"

PrefabPanelBase::PrefabPanelBase(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	wxBoxSizer* bSizer91;
	bSizer91 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxVERTICAL);

	m_property_mgr = new wxPropertyGridManager(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxPGMAN_DEFAULT_STYLE | wxPG_DESCRIPTION | wxTAB_TRAVERSAL);
	m_property_mgr->SetExtraStyle(wxPG_EX_MODE_BUTTONS);
	bSizer10->Add(m_property_mgr, 1, wxALL | wxEXPAND, 5);

	bSizer91->Add(bSizer10, 16, wxEXPAND, 5);

	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer(wxHORIZONTAL);

	m_show_prefab_button = new wxButton(this, wxID_ANY, wxT("Show"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer23->Add(m_show_prefab_button, 0, wxALL, 5);

	wxArrayString m_prefab_choiceChoices;
	m_prefab_choice = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_prefab_choiceChoices, 0);
	m_prefab_choice->SetSelection(0);
	bSizer23->Add(m_prefab_choice, 1, wxALL | wxEXPAND, 5);

	m_apply_prefab_button = new wxButton(this, wxID_ANY, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer23->Add(m_apply_prefab_button, 0, wxALL, 5);

	bSizer91->Add(bSizer23, 1, wxEXPAND, 5);

	this->SetSizer(bSizer91);
	this->Layout();
}

PrefabPanelBase::~PrefabPanelBase()
{
}
#endif // SP_USE_EDITOR