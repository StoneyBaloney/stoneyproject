#ifdef SP_USE_EDITOR

#include "EditorRenderUtils.h"

void StoneyCore::Renderer::GenEditorGizmoBuffer(Editor::EditorWidgetRenderState& widget_state, stVector<GL_Indirect>& out_indirects)
{
	if (widget_state.program_id)
	{
		glGenVertexArrays(1, &widget_state.vao_id);
		glBindVertexArray(widget_state.vao_id);

		glGenBuffers(1, &widget_state.shape_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, widget_state.shape_vbo);
		constexpr auto widget_shape = Editor::GetShapeAll();
		glBufferData(GL_ARRAY_BUFFER, sizeof(widget_shape[0]) * widget_shape.size(), widget_shape.data(), GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(widget_shape[0]), 0);
		glEnableVertexAttribArray(0);

		constexpr auto colors = Editor::GetWidgetColors();

		glGenBuffers(1, &widget_state.colors_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, widget_state.colors_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(colors[0]) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(colors[0]), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribDivisor(1, 1);

		auto origin = glm::translate(glm::mat4(1), glm::vec3(0, 0, 0));
		glGenBuffers(1, &widget_state.transform_vbo);
		glBindBuffer(GL_UNIFORM_BUFFER, widget_state.transform_vbo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), NULL, GL_STREAM_DRAW);
		glBindBufferRange(GL_UNIFORM_BUFFER, 4, widget_state.transform_vbo, 0, sizeof(glm::mat4));

		const auto indirects = Editor::GetWidgetIndirects();
		out_indirects.insert(out_indirects.begin(), indirects.begin(), indirects.end());
	}
	else {
		LogErrorToFile(std::string("Failed to create editor widget shader state"));
	}

	glEnableVertexAttribArray(0);
	//glVertexBindingDivisor()

	glBindVertexArray(0);
}

void StoneyCore::Renderer::GenLightingDebugState(Editor::EditorLightDebugState& render_state) {
	glGenVertexArrays(1, &render_state.vao_id);
	glBindVertexArray(render_state.vao_id);

	glGenBuffers(1, &render_state.shape_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, render_state.shape_vbo);
	constexpr auto widget_shape = Editor::GetCubeShape();
	glBufferData(GL_ARRAY_BUFFER, sizeof(widget_shape[0]) * widget_shape.size(), widget_shape.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(widget_shape[0]), 0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &render_state.inst_trans_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, render_state.inst_trans_vbo);
	glGenBuffers(1, &render_state.colors_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, render_state.colors_vbo);

	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
}
#endif // SP_USE_EDITOR