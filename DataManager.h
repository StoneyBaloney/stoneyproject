#pragma once
#include <stdint.h>
#include <atomic>
#include <mutex>
#include <type_traits>
#include <unordered_map>
#include <entt/core/ident.hpp>
#include <entt/core/family.hpp>
#include <function2/function2.hpp>
#include "Errors.h"
#include "Defines.h"
#include "HashedString.h"
#include "BigBitField.h"
using component_family = entt::family<struct internal_registry_component_family>;

enum class SGS_ACCESS_TYPE : bool {
	ReadOnly,
	ReadWrite
};
template <typename DataType, HashName name, SGS_ACCESS_TYPE access_type >
struct DataInfo {
	static constexpr auto Name() {
		return name;
	}
	using m_type = DataType;

	static constexpr auto AccessType() {
		return access_type;
	}
};
template <typename DataType, SGS_ACCESS_TYPE access_type>
struct AccessHelper {
	static const auto& AddConstToReadOnly(DataType& _data) {
		return _data;
	}
};

template <typename DataType>
struct AccessHelper<DataType, SGS_ACCESS_TYPE::ReadWrite> {
	static DataType& AddConstToReadOnly(DataType& _data) {
		return _data;
	}
};

struct StoneyAllocFunc {
	template <typename T, typename...Args>
	static T* Alloc(Args& ...args) {
		auto* ret = new T(args...);
		return ret;
	}

	template<typename T>
	static void DeAlloc(T* data) {
		delete data;
		data = nullptr;
	}
};
struct MemNode {
	bool is_empty{ true };
	void* data{ nullptr };
	operator bool() {
		return (!is_empty) && (data);
	}
	template <typename T>
	operator T* () {
		return static_cast<T*>(data);
	}
};
struct DataMeta {
	template <typename Deleter>
	DataMeta(const Deleter _deleter) {
		deleter = _deleter;
	}

	void Deleter(const size_t index) {
		if (index < data.size()) {
			if (data[index]) {
				deleter(data[index]);
				data[index].is_empty = true;
				data[index].data = nullptr;
			}
		}
	}

	stVector<MemNode> data;
	fu2::function<void(MemNode)> deleter;
};
struct DataHandle {
	uint32_t type_index{ 0 };
	uint32_t data_index{ 0 };
};
static uint32_t next{ 1 };

template <typename AllocFunc = StoneyAllocFunc >
class DataRegistrar {
	std::unordered_map<uint32_t, uint32_t> type_reg;
	std::unordered_map<uint32_t, DataHandle> data_registry;		//key is user defined, val is internal id
	BigBitField<MAX_DATA_TYPES> m_type_ids;
	std::atomic<unsigned int> m_next{ 1 };
	stVector<DataMeta> meta_datas;
	std::recursive_mutex m_lock;

private:
	template <typename DataType, typename...Args>
	MemNode alloc(Args...args) {
		MemNode ret;
		ret.data = AllocFunc::template Alloc<DataType>(args...);
		if (ret.data)
		{
			ret.is_empty = false;
		}
		return ret;
	}
public:

	template <typename DataType>
	uint32_t GetTypeID() {
		const auto&& ret = uint32_t(component_family::type<DataType>);
		if (type_reg.find(ret) == type_reg.end())
		{
			type_reg[ret] = m_next++;
		}
		return type_reg[ret];
	}
	void ClearAll() {
		auto clear_vec = data_registry;
		for (auto& data_id : clear_vec) {
			Delete(data_id.first);
		}
	}
	void Delete(const uint32_t data_name) {
		if (data_registry.find(data_name) != data_registry.end())
		{
			auto ids = data_registry[data_name];
			auto& meta_data = meta_datas[ids.type_index];
			meta_data.Deleter(ids.data_index);

			data_registry.erase(data_name);
		}
	}
	template <typename DataType, typename...Args>
	DataType* Create(const uint32_t data_name, Args...args) {
		std::lock_guard<std::recursive_mutex> lock(m_lock);
		auto&& t_id = GetTypeID<DataType>();
		uint32_t type_index = 0;
		if (!m_type_ids.IsSet(t_id)) {
			type_index = uint32_t(meta_datas.size());
			meta_datas.emplace_back([](MemNode node) {
				if (node)
				{
					DataType* ptr = static_cast<DataType*>(node.data);
					AllocFunc::template DeAlloc<DataType>(ptr);
				}
			}
			);
			m_type_ids.SetBit(t_id);
		}
		else {
			type_index = t_id - 1;
		}

		if (data_registry.find(data_name) == data_registry.end())
		{
			auto meta_index = t_id - 1;
			auto data_index = meta_datas[meta_index].data.size();
			auto mem_node = alloc<DataType>(args...);
			meta_datas[meta_index].data.emplace_back();
			data_registry[data_name].type_index = type_index;
			data_registry[data_name].data_index = uint32_t(meta_datas[meta_index].data.size() - 1);

			if (mem_node)
			{
				meta_datas[meta_index].data[data_index] = mem_node;
				return static_cast<DataType*>(mem_node.data);
			}
		}
		else {
			LogErrorToFile("Tried to use the same data id twice.");
		}

		return nullptr;
	}

	template <typename DataType>
	DataType* GetData(const uint32_t data_name) {
		std::lock_guard<std::recursive_mutex> lock(m_lock);

		if (data_registry.find(data_name) != data_registry.end())
		{
			auto&& type_index = data_registry[data_name].type_index;
			auto data_index = data_registry[data_name].data_index;

			return static_cast<DataType*>(meta_datas[type_index].data[data_index].data);
		}

		return nullptr;
	}

	~DataRegistrar() {
		ClearAll();
	}
};