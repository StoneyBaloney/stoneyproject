#pragma once
#ifdef SP_USE_EDITOR
#include "EditorFrame.h"
#include "StPropertyPanel.h"
#include "SceneTreePanel.h"

class StoneyEditorMain : public EditorFrame
{
	StPropertyPanel* m_property_panel{ nullptr };

public:
	StoneyEditorMain();
	~StoneyEditorMain();
	friend class EditorApp;
};

#endif // SP_USE_EDITOR
