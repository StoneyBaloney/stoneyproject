#ifdef SP_USE_EDITOR

#include "ShaderPanelBase.h"
ShaderPanelBase::ShaderPanelBase(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer(wxVERTICAL);

	m_auinotebook = new wxAuiNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_TAB_EXTERNAL_MOVE | wxAUI_NB_TAB_MOVE | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TOP | wxAUI_NB_WINDOWLIST_BUTTON);
	m_shader_info_panel = new wxPanel(m_auinotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer912;
	bSizer912 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer102;
	bSizer102 = new wxBoxSizer(wxVERTICAL);

	m_property_mgr = new wxPropertyGridManager(m_shader_info_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxPGMAN_DEFAULT_STYLE | wxTAB_TRAVERSAL);
	m_property_mgr->SetExtraStyle(wxPG_EX_MODE_BUTTONS);
	bSizer102->Add(m_property_mgr, 1, wxALL | wxEXPAND, 5);

	bSizer912->Add(bSizer102, 16, wxEXPAND, 5);

	m_shader_info_panel->SetSizer(bSizer912);
	m_shader_info_panel->Layout();
	bSizer912->Fit(m_shader_info_panel);
	m_auinotebook->AddPage(m_shader_info_panel, wxT("Shader Info"), true, wxNullBitmap);
	m_shader_panel = new wxPanel(m_auinotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer911;
	bSizer911 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer* bSizer101;
	bSizer101 = new wxBoxSizer(wxHORIZONTAL);

	m_scintilla3 = new wxStyledTextCtrl(m_shader_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, wxEmptyString);
	bSizer101->Add(m_scintilla3, 1, wxEXPAND | wxALL, 5);

	bSizer911->Add(bSizer101, 16, wxEXPAND, 8);

	m_shader_panel->SetSizer(bSizer911);
	m_shader_panel->Layout();
	bSizer911->Fit(m_shader_panel);
	m_auinotebook->AddPage(m_shader_panel, wxT("GLSL Source Editor"), false, wxNullBitmap);

	bSizer18->Add(m_auinotebook, 16, wxEXPAND | wxALL, 5);

	wxBoxSizer* bSizer50;
	bSizer50 = new wxBoxSizer(wxVERTICAL);

	m_ctrl_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer231;
	bSizer231 = new wxBoxSizer(wxHORIZONTAL);

	m_apply_button = new wxButton(m_ctrl_panel, wxID_ANY, wxT("Apply Live"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer231->Add(m_apply_button, 0, wxALL | wxEXPAND, 5);

	wxArrayString m_shader_choicesChoices;
	m_shader_choices = new wxChoice(m_ctrl_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_shader_choicesChoices, 0);
	m_shader_choices->SetSelection(0);
	bSizer231->Add(m_shader_choices, 1, wxALL | wxEXPAND, 5);

	m_load_button = new wxButton(m_ctrl_panel, wxID_ANY, wxT("Load"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer231->Add(m_load_button, 0, wxALL | wxEXPAND, 5);

	m_save_button = new wxButton(m_ctrl_panel, wxID_ANY, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer231->Add(m_save_button, 0, wxALL | wxEXPAND, 5);

	m_saveas_button = new wxButton(m_ctrl_panel, wxID_ANY, wxT("Save As"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer231->Add(m_saveas_button, 0, wxALL | wxEXPAND, 5);

	m_ctrl_panel->SetSizer(bSizer231);
	m_ctrl_panel->Layout();
	bSizer231->Fit(m_ctrl_panel);
	bSizer50->Add(m_ctrl_panel, 1, wxEXPAND | wxALL, 5);

	bSizer18->Add(bSizer50, 1, wxEXPAND, 5);

	this->SetSizer(bSizer18);
	this->Layout();
}

ShaderPanelBase::~ShaderPanelBase()
{
}
#endif // SP_USE_EDITOR