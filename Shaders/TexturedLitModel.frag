#version 450



in vec3 FNormal;
in vec3 FragCoord;
in vec2 FTexCoord;
layout (std140, binding = 0) uniform LightProperties{
vec4 Ambient;
vec4 LightPos;
vec4 LightColor;
};
uniform sampler2D ourTexture;
out vec4 FragmentColor;


void main(){
   


float cosTheta =  clamp(dot(normalize(FNormal),normalize(LightPos.xyz-FragCoord)),0,1) ;
vec4 Helper = normalize(vec4(abs(LightPos.x*FragCoord.y/LightPos.y),abs(FragCoord.x*FragCoord.x/LightPos.y),abs(LightPos.x*LightPos.x/FragCoord.y),1.0));
cosTheta = cosTheta * cosTheta;



float Distance = (LightPos.x-FragCoord.x)* (LightPos.x-FragCoord.x)+(LightPos.y-FragCoord.y)*(LightPos.y-FragCoord.y)+(LightPos.z-FragCoord.z)*(LightPos.z-FragCoord.z);
Distance=7/Distance;
FragmentColor =  vec4((Ambient*texture(ourTexture,FTexCoord) +(texture(ourTexture,FTexCoord) *Helper*77 * cosTheta*(Distance))).xyz,1.0);




}