#version 460

layout(location = 0)in vec3 VertexPosition;
layout(location = 1)in vec3 Normal;
layout (location = 2)in vec2 TexCoord;
layout (location = 4)in mat4 MTransform;
layout (binding = 0) uniform _Matrices{
mat4 MVP;
mat4 MView;
} Matrices;
out flat vec4 cam_pos;
out vec3 frag_pos;
out vec3 frag_normal;
out vec2 frag_diff_uv;
out float DrawID;


void main(){
    
    vec4 world_pos = MTransform * vec4(VertexPosition,1.0);
    frag_pos = world_pos.xyz;
    frag_normal = vec4(vec4(Normal,1.0)* MTransform).xyz;
    frag_diff_uv = TexCoord; 
   
vec4 ray_start = inverse(Matrices.MVP * Matrices.MView) *
					vec4((vec2(0.5,0.5)*2)-1,
						-1.0,
						1.0f);
				ray_start /= ray_start.w;
    vec4 g_pos = Matrices.MVP *Matrices.MView* world_pos;
cam_pos =ray_start;
    DrawID = gl_DrawID;
  
    

    gl_Position = g_pos;
}