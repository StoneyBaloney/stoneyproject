#version 450

layout(location = 0)in vec3 VertexPosition;

layout(location = 1)in vec3 Normal;

layout (location = 2)in vec2 TexCoord;

layout (location = 3)in mat4 MTransform2;

layout(location = 7)in mat4 MVP2;

uniform mat4 MVP;
uniform mat4 MTransform;

out vec3 FNormal;
out vec3 FragCoord;
out vec2 FTexCoord;


void main(){
    gl_Position = MVP * MTransform* vec4(VertexPosition,1.0);
   


    FNormal = (MTransform*vec4(Normal,1.0)).xyz;
        
    FTexCoord = TexCoord;
    FragCoord =(MTransform*vec4(VertexPosition,1.0)).xyz;
}