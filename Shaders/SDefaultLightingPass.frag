#version 460

struct PointLightProperties {

	vec3 point_light_arr[5];
	// index 0 is ambient
	// index 1 is position
	// index 2 is diffuse
	// index 3 is specular
	// index 4 is attenuation values, x is constant, y is linear, and z is quadratic
};
#define LIGHT_AMBIENT 0
#define LIGHT_POSITION 1
#define LIGHT_DIFFUSE 2
#define LIGHT_SPECULAR 3
#define LIGHT_ATTENUATION 4
struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 shininess;
};
layout (binding = 0) uniform _Matrices{
mat4 MVP;
mat4 MView;
} Matrices;

layout (shared, binding = 8) uniform LightProperties{
uint num_point_lights;
PointLightProperties point_lights[100];
} lights;


layout (binding = 0)uniform isampler2D TexColor;
layout (binding = 1)uniform sampler2D TexNormal;

in vec2 FTexCoord;
in vec3 FPos;

out vec4 GL_OutColor;

vec4 UnpackInt(int val){
	
	return vec4(val & (255),(val & (255 << 8)) >> 8,(val & (255 << 16)) >> 16,(val &(255 << 24)) >>24) / 255;
}
vec3 WorldPosFromDepth(float depth) {
    vec4 ray_start = inverse(Matrices.MVP * Matrices.MView) *
					vec4((FTexCoord*2)-1,
						-1.0,
						1.0f);

				ray_start /= ray_start.w;

				vec4 ray_eye = inverse(Matrices.MVP) *
					vec4((FTexCoord*2)-1,
						-1.0,
						1.0);

				vec4 ray_dir = normalize((inverse(Matrices.MView) * vec4(ray_eye.x, ray_eye.y, -1.0, 0.0)));

				return vec4((ray_start+((ray_dir)*depth))).xyz; 

}
vec3 CalculatePointLight(PointLightProperties point_light,vec3 frag_pos,vec3 frag_normal,vec3 color ){


vec3 light_dir = normalize(point_light.point_light_arr[LIGHT_POSITION] - frag_pos);
    // diffuse shading
    float diff = max(dot(frag_normal, light_dir), 0.0);
    // specular shading
    vec3 ref_dir = reflect(-light_dir, frag_normal);
    
    // attenuation
    float distance    = length(point_light.point_light_arr[LIGHT_POSITION] - frag_pos);
    float attenuation = 1.0 / (point_light.point_light_arr[LIGHT_ATTENUATION].x + point_light.point_light_arr[LIGHT_ATTENUATION].y * distance + 
  			     point_light.point_light_arr[LIGHT_ATTENUATION].z * (distance * distance));    
    // combine results
    vec3 ambient  = point_light.point_light_arr[LIGHT_AMBIENT]  * color;
    vec3 diffuse  = point_light.point_light_arr[LIGHT_DIFFUSE]  * color * diff;
    
    ambient  *= attenuation;
    diffuse  *= attenuation;
   
    return (ambient + diffuse);
}


void main(){

	ivec4 packed_colors = texture(TexColor,FTexCoord);
	vec4 diffuse_roughness = ::UnpackInt(packed_colors.x);
	vec4 ambient_opacity = ::UnpackInt(packed_colors.y);
	vec4 specular = ::UnpackInt(packed_colors.z);
	vec4 emissive = ::UnpackInt(packed_colors.w);
	vec4 normal = texture(TexNormal,FTexCoord);
	float depth = normal.w;
	vec3 frag_pos = WorldPosFromDepth(depth);
 
	GL_OutColor = vec4(0.0);// + vec4(diffuse_roughness.xyz,0);

	for ( uint i = 0; i < lights.num_point_lights; i ++){
   		const uint ind = i;
   		GL_OutColor += vec4(CalculatePointLight(lights.point_lights[ind],frag_pos,normal.xyz,diffuse_roughness.xyz),1.0);  

    
	}
	//GL_OutColor += vec4(depth*0.0001,depth*0.0001,depth*0.0001,1.0);
	//GL_OutColor = vec4(normal.xyz,0);
}