#version 460

layout(location = 0) out ivec4 OutFragmentColor;
layout(location = 1) out vec4 OutNormal;
in vec4 cam_pos;
in vec3 frag_pos;
in vec3 frag_normal;
in vec2 frag_diff_uv;
in float DrawID;
uniform sampler2DArray diff_textures;

void main(){


OutFragmentColor.xyz =   ivec3(texture(diff_textures,vec3(frag_diff_uv.xy,DrawID)).xyz); 
float FDist = distance (cam_pos.xyz,frag_pos);
OutNormal = vec4(frag_normal,FDist);








}