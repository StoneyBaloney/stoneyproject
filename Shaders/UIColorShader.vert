#version 450

layout (location = 0) in vec2 Vertex; // <vec2 pos, vec2 tex>
layout (location = 2) in vec2 TexCoords;
out vec2 texCoords;
out vec2 fun;
uniform vec3 transform;
uniform mat4 projection;

void main()
{
    gl_Position =  projection * vec4((Vertex.x*transform.z)+transform.x,(Vertex.y*transform.z)+transform.y,-1.0,1.0);
    texCoords = TexCoords;
    fun = (projection * vec4((Vertex.x*transform.z)+transform.x,(Vertex.y*transform.z)+transform.y,-1.0,1.0)).xy*2;
}  