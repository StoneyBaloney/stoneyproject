#version 460
layout(location = 0)in vec3 VertexPosition;
layout (location = 1)in vec2 TexCoord;
//layout (location = 2)in vec2 RenderSpecs;
out vec2 FTexCoord;
out noperspective vec3 FPos;

void main(){

    
     
    FPos = VertexPosition;
    FTexCoord = TexCoord;
    gl_Position =  vec4(VertexPosition,1.0);
   
}