#version 460
layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec3 Normal;
layout (location = 6) in vec2 TexCoord0;
layout (location = 2) in mat4 MTransform;
layout (location = 14) in vec3 Bitangent;
layout (location = 15) in vec3 Tangent;
layout (binding = 0) uniform _Matrices{
mat4 MVP;
mat4 MView;
} Matrices;
out vec3 frag_pos;
out vec2 uv_channel0;
out mat3 TBN;out flat vec4 cam_pos;
out float DrawID;
void SetTBN(vec3 normal){
mat3 normal_matrix = mat3(MTransform);
TBN = mat3(Tangent, Bitangent, Normal) * normal_matrix;
}
void main (){
vec4 world_pos = MTransform * vec4((VertexPosition),1.0);
vec4 g_pos = Matrices.MVP *Matrices.MView* world_pos;
gl_Position = g_pos;
frag_pos = world_pos.xyz;
uv_channel0 = TexCoord0;
::SetTBN((normalize(Normal)));
vec4 ray_start = inverse(Matrices.MVP * Matrices.MView) *
vec4((vec2(0.5, 0.5) * 2) - 1,
	-1.0,
	1.0f);
ray_start /= ray_start.w;
cam_pos =ray_start;
DrawID = gl_DrawID;
}
