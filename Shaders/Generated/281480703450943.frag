#version 460
layout(location = 0) out ivec4 OutFragmentColor;
layout(location = 1) out vec4 OutNormal;
in vec3 frag_pos;
in vec3 frag_normal;
in vec2 uv_channel0;
in vec4 cam_pos;
in float DrawID;
layout (binding = 0)uniform sampler2DArray diffuse_textures;
int PackVec4(vec4 in_vec) {
in_vec *= 255.0;
int retx = (int(in_vec.r) | (int(in_vec.g) << 8) | (int(in_vec.b) << 16) | (int(in_vec.a) << 24));
return  retx;
}
void main (){
vec4 out_channel_1 = vec4(0,0,0,1);
vec4 out_channel_2	= vec4(0,0,0,1);
vec4 out_channel_3	= vec4(0,0,0,1);
vec4 out_channel_4	= vec4(0,0,0,1);
out_channel_1.xyz = texture(diffuse_textures,vec3(uv_channel0,DrawID)).xyz;
OutNormal.xyz = frag_normal;
OutNormal.w = distance (cam_pos.xyz,frag_pos);
OutFragmentColor =	ivec4(::PackVec4(out_channel_1),::PackVec4(out_channel_2),::PackVec4(out_channel_3),::PackVec4(out_channel_4));
}
