#version 460

layout(location = 0)in vec3 VertexPosition;
layout(location = 1)in vec3 Normal;
layout (location = 2)in vec2 TexCoord;
layout (location = 4)in mat4 MTransform;
layout (binding = 0) uniform _Matrices{
mat4 MVP;
} Matrices;

out vec3 FNormal;
out vec2 FTexCoord;

out float DrawID;

void main(){
    
    DrawID = gl_DrawID;
    FNormal = Normal;
    FTexCoord = TexCoord;
    gl_Position =  Matrices.MVP * MTransform * vec4(VertexPosition,1.0);  
   
}