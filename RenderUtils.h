#pragma once
#include "SRendererGL.h"

namespace StoneyCore {
	namespace Renderer {
		//Helper for renderer initialization. Allocates all queues through the supplied data reg
		struct RenderInitializer {
			RendererConstArgs m_queues;

			moodycamel::ProducerToken batch_ptok;
			moodycamel::ProducerToken render_queue_ptok;
			moodycamel::ProducerToken buffer_ptok;
			moodycamel::ProducerToken shader_ptok;
			Renderer::RenderBuffer* m_render_buffers[MAX_RENDER_BUFFERS]{};

			RenderInitializer() = delete;
			RenderInitializer(const RenderInitializer& other) = delete;
			RenderInitializer(DataRegistrar<>& data_reg);
		};
	}
}