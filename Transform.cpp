#include<vector>
#include "Transform.h"

namespace StoneyCore {
	Camera::Camera(float _FOV, float _AspectRatio, float _ZNear, float _ZFar) :
		m_fov(_FOV),
		m_aspect(_AspectRatio),
		m_near(_ZNear),
		m_far(_ZFar)

	{
	}
}