#ifdef SP_USE_EDITOR

#include "GameData.h"
#include "EditorWidgetShape.h"
std::array<StoneyCore::Renderer::GL_Indirect, 3> StoneyCore::Editor::GetWidgetIndirects() {
	using namespace StoneyCore::Renderer;
	auto num_verts_x = GLuint(GetShapeX().size());
	auto num_verts_y = GLuint(GetShapeY().size());
	auto num_verts_z = GLuint(GetShapeZ().size());
	std::array<GL_Indirect, 3> ret = {
		GL_Indirect(num_verts_x,1,0,0),
		GL_Indirect(num_verts_y,1,num_verts_x,1),
		GL_Indirect(num_verts_z,1, num_verts_x + num_verts_y,2)
	};
	return ret;
}

#endif // SP_USE_EDITOR