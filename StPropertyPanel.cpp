#ifdef SP_USE_EDITOR

#include "EditorTypes.h"
#include "StPropertyPanel.h"

StPropertyPanel::StPropertyPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : PropertyPanelBase(parent, id, pos, size, style, name)

{
	m_shader_panel = new StShaderPanel(this, wxID_ANY, pos, size, style, wxString("Shaders"));
	m_prefab_panel = new StPrefabPanel(this, wxID_ANY, pos, size, style, wxString("Prefabs"));
	m_lighting_panel = new StLightEditorPanel(this, wxID_ANY, pos, size, style, wxString("Lighting"));
	m_main_notebook->AddPage(m_shader_panel, wxString("Shaders"));
	m_main_notebook->AddPage(m_prefab_panel, wxString("Prefabs"));
	m_main_notebook->AddPage(m_lighting_panel, wxString("Lighting"));
	Show();
}

StPropertyPanel::~StPropertyPanel()
{
}
#endif // SP_USE_EDITOR