#ifdef SP_USE_EDITOR

#include "StLightEditorPanel.h"

StLightEditorPanel::StLightEditorPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : LightingPanelBase(parent, id, pos, size, style, name)
{
}

StLightEditorPanel::~StLightEditorPanel()
{
}
#endif // SP_USE_EDITOR