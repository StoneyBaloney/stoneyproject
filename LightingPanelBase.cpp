#ifdef SP_USE_EDITOR

#include "LightingPanelBase.h"

///////////////////////////////////////////////////////////////////////////

LightingPanelBase::LightingPanelBase(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer(wxVERTICAL);

	m_auinotebook = new wxAuiNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_TAB_EXTERNAL_MOVE | wxAUI_NB_TAB_MOVE | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TOP | wxAUI_NB_WINDOWLIST_BUTTON);
	m_scene_lights_panel = new wxPanel(m_auinotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer912;
	bSizer912 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer102;
	bSizer102 = new wxBoxSizer(wxVERTICAL);

	m_lighting_tree_data = new wxDataViewTreeCtrl(m_scene_lights_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
	bSizer102->Add(m_lighting_tree_data, 2, wxALL | wxEXPAND, 5);

	wxBoxSizer* bSizer231;
	bSizer231 = new wxBoxSizer(wxHORIZONTAL);

	wxArrayString m_light_typesChoices;
	m_light_types = new wxChoice(m_scene_lights_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_light_typesChoices, 0);
	m_light_types->SetSelection(0);
	bSizer231->Add(m_light_types, 1, wxALL | wxEXPAND, 5);

	bSizer102->Add(bSizer231, 0, wxEXPAND, 5);

	bSizer912->Add(bSizer102, 16, wxEXPAND, 5);

	m_scene_lights_panel->SetSizer(bSizer912);
	m_scene_lights_panel->Layout();
	bSizer912->Fit(m_scene_lights_panel);
	m_auinotebook->AddPage(m_scene_lights_panel, wxT("Scene Lighting"), true, wxNullBitmap);
	m_lights_panel = new wxPanel(m_auinotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer91;
	bSizer91 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxVERTICAL);

	m_property_mgr = new wxPropertyGridManager(m_lights_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxPGMAN_DEFAULT_STYLE | wxPG_DESCRIPTION | wxTAB_TRAVERSAL);
	m_property_mgr->SetExtraStyle(wxPG_EX_MODE_BUTTONS);
	bSizer10->Add(m_property_mgr, 1, wxALL | wxEXPAND, 5);

	bSizer91->Add(bSizer10, 16, wxEXPAND, 5);

	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer(wxHORIZONTAL);

	m_create_light_button = new wxButton(m_lights_panel, wxID_ANY, wxT("Create Light Type"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer23->Add(m_create_light_button, 0, wxALL | wxEXPAND, 5);

	m_save_light_button = new wxButton(m_lights_panel, wxID_ANY, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer23->Add(m_save_light_button, 0, wxALL, 5);

	bSizer91->Add(bSizer23, 1, wxEXPAND, 5);

	m_lights_panel->SetSizer(bSizer91);
	m_lights_panel->Layout();
	bSizer91->Fit(m_lights_panel);
	m_auinotebook->AddPage(m_lights_panel, wxT("Lights"), false, wxNullBitmap);

	bSizer18->Add(m_auinotebook, 16, wxEXPAND | wxALL, 5);

	wxBoxSizer* bSizer50;
	bSizer50 = new wxBoxSizer(wxVERTICAL);

	m_ctrl_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer2311;
	bSizer2311 = new wxBoxSizer(wxHORIZONTAL);

	m_add_light_button = new wxButton(m_ctrl_panel, wxID_ANY, wxT("Add Light"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer2311->Add(m_add_light_button, 0, wxALL | wxEXPAND, 5);

	wxArrayString m_light_choicesChoices;
	m_light_choices = new wxChoice(m_ctrl_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_light_choicesChoices, 0);
	m_light_choices->SetSelection(0);
	bSizer2311->Add(m_light_choices, 1, wxALL | wxEXPAND, 5);

	m_ctrl_panel->SetSizer(bSizer2311);
	m_ctrl_panel->Layout();
	bSizer2311->Fit(m_ctrl_panel);
	bSizer50->Add(m_ctrl_panel, 1, wxEXPAND | wxALL, 0);

	bSizer18->Add(bSizer50, 1, wxEXPAND, 5);

	this->SetSizer(bSizer18);
	this->Layout();
}

LightingPanelBase::~LightingPanelBase()
{
}
#endif //SP_USE_EDITOR