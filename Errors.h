#pragma once

#include <iostream>
#include "SEngineTypes.h"
template <typename T>
inline void ConcatStr(std::string& output, T next) {
	output += next;
}
template <typename First, typename Next, typename...Remaining>
inline void ConcatStr(std::string& output, First first, Next next, Remaining...remaining) {
	ConcatStr<First>(output, first);
	ConcatStr<Next, Remaining...>(output, next, remaining...);
}
template <typename...Strings>
inline std::string Concat(Strings...strings) {
	std::string ret;
	ConcatStr<Strings...>(ret, strings...);
	return ret;
}
class Errors
{
public:
	Errors();
	~Errors();
};

void PrintErrorQueue(int num_errors);

void LogErrorToFile(std::string err);

inline extern void FatalError(const char* _error_msg);

inline extern void FatalError(stVector<char>& CharVector);
inline extern void FatalError(std::string _errorString);
inline extern void WarnMessage(const std::string& message);
inline extern void LogMessage(std::string _message)
{
	std::cout << _message << std::endl;
}
inline extern void ThrowErrorContinue(const std::string& _message)
{
	FatalError(_message);
}

inline extern void TestErrorContinue(bool test, const char* _message)
{
	if (test) {
		FatalError(_message);
	}
}

//inline extern void LogValidationMessage(const char* _message)
//{
//#ifdef _DEBUG
//
//	std::cout << "Validation layer: " << _message << std::endl;
//
//#endif
//}
