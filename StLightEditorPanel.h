#ifdef SP_USE_EDITOR

#pragma once
#include "LightingPanelBase.h"
namespace StoneyCore {
	namespace Editor { class EditorSceneHandler; class EditorSceneManager; }
}
class StLightEditorPanel :
	public LightingPanelBase
{
	friend class StoneyCore::Editor::EditorSceneManager;

public:
	StLightEditorPanel(wxWindow* parent,
		wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxSize(500, 535),
		long style = wxTAB_TRAVERSAL,
		const wxString& name = wxEmptyString);
	~StLightEditorPanel();
};

#endif // SP_USE_EDITOR