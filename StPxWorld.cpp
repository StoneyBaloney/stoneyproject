#include "Errors.h"
#include "StPxWorld.h"
namespace StoneyCore {
	namespace StPxPhysics {
		StPxWorld::StPxWorld()
		{
		}

		StPxWorld::~StPxWorld()
		{
		}
		void* StPxAllocator::allocate(size_t size, const char* typeName, const char* filename, int line)
		{
			return platformAlignedAlloc(size); // just return alloc from physx header
		}
		void StPxAllocator::deallocate(void* ptr)
		{
			platformAlignedFree(ptr);
		}
		void StPxErrorCallback::reportError(PxErrorCode::Enum code, const char* message, const char* file, int line)
		{
			std::string error = "";
			error += "Physx error callback code ";
			error += code;
			error += ". File: '";
			error += file;
			error += "' Line: '";
			error += line;
			error += "' Message was: \n";
			error += message;
			FatalError(message);
		}
		PxVec3 MakeVec(glm::vec3 in_vec) {
			return PxVec3(in_vec.x, in_vec.y, in_vec.z);
		}
	}
}