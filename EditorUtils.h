#pragma once
#ifdef SP_USE_EDITOR

#include <filesystem>
#include <string>
#include <map>
#include <unordered_map>
#include "pch.h"
#include "Errors.h"
#include "EditorTypes.h"
#include "StPropertyPanel.h"
#include "Lighting.h"
#include "Reflection.h"

namespace StoneyCore {
	extern std::string GetUniqueFilename(std::string filename);
	inline void SetLightPropertyPageProps(StPropertyPage* page, DirectionalLightProperties& light_props, LightType light_type) {
		page->Clear();

		StGenerateProperties(page, light_props);

		auto lt_name = wxString("Light Type");
		auto type_label = wxString("Type");
		auto property = GenerateEnumProperty(type_label, light_type);
		auto lt_category = new wxStringProperty(lt_name, lt_name);
		page->Append(lt_category);
		lt_category->AppendChild(property);
		switch (light_type)
		{
		case StoneyCore::LightType::Directional: {
			page->GetPropertyByLabel(wxString(refl::reflect<DirectionalLightProperties>().name))->GetPropertyByName(wxString("LightPos"))->SetLabel(wxString("Light Direction"));
			break;
		}
		case StoneyCore::LightType::Point: {
			page->GetPropertyByLabel(wxString(refl::reflect<DirectionalLightProperties>().name))->GetPropertyByName(wxString("LightPos"))->Hide(true);
			break;
		}
		case StoneyCore::LightType::Spot: {
			page->GetPropertyByLabel(wxString(refl::reflect<DirectionalLightProperties>().name))->GetPropertyByName(wxString("LightPos"))->SetLabel(wxString("Light Direction"));
			break;
		}
		default: {
			break;
		}
		}
	}
	template <typename MapType>
	inline std::string GetUniqueName(std::string name, const MapType& map) {
		bool found = false;
		unsigned next_name = 2;
		auto try_name = name;
		for (;;) {
			auto val = map.find(try_name);

			if (val == map.end())
			{
				return try_name;
			}
			else {
				try_name = Concat(name, "_", std::to_string(next_name));
				if (next_name == UINT_MAX)
				{
					next_name = 0;
					name = Concat(name, "OVERFLOW", std::to_string(next_name));
				}
				next_name++;
			}
		}
	}
}
#endif // SP_USE_EDITOR
