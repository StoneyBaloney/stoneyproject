#pragma once

#include <GL/glew.h>

namespace StoneyCore {
	namespace Renderer {
		struct DeferredGbufferGL
		{
		public:
			GLuint m_buffer_textures[2]{};
			GLenum m_buffer_attachments[2]{ GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
			GLuint& col_tex_id = m_buffer_textures[0];
			GLuint& normal_tex_id = m_buffer_textures[1];

			GLuint depth_buffer_id{ 0 };
			GLuint framebuffer_id{ 0 };

			DeferredGbufferGL();
			~DeferredGbufferGL();
		};
	}
}
