#pragma once
#include <array>
#include <boost/chrono/chrono.hpp>
#include "Defines.h"
namespace StoneyCore {
	struct stClock
	{
		boost::chrono::high_resolution_clock::time_point last;
		double delta{ 0 };
		double delta_avg{ 0 };
		double physics_step_helper{ 0 };
		unsigned int delta_stepper{ 0 };
		std::array<float, MAX_RENDER_BUFFERS> delta_accum{ 0.06f };
		double SetDelta();
	};
}