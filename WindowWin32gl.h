#pragma once
#include "Defines.h"
#ifdef _WIN32
#ifndef USE_SDL
#include <Windows.h>
#include "Types.h"

#include "WindowData.h"

namespace WindowWin32gl {
	void InitWindow(WindowInitInfo _init_info);

	void CreateContext(HWND _hwnd);
};
#endif // !USE_SDL

#endif // _WIN32