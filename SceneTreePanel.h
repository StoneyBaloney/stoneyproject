#pragma once
#ifdef SP_USE_EDITOR

#include "SceneTreePanelBase.h"
#include "GameData.h"
//#include <map>
struct EditorInstanceProxy {
};
class SceneTreePanel : public SceneTreePanelBase
{
	std::map<wxDataViewItem, int> items;
public:
	SceneTreePanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(268, 520), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~SceneTreePanel();
	void PopulateSceneTree(StoneyCore::SceneGraph& scene);
};
#endif // SP_USE_EDITOR
