#pragma once
#ifdef SP_USE_EDITOR

#include <wx/wx.h>
#include <wx/treectrl.h>
#include <wx/dataview.h>
class SceneTreePanelBase : public wxPanel
{
private:

protected:
	wxDataViewTreeCtrl* m_scene_tree{ nullptr };
	friend class StoneyEditorMain;
public:

	SceneTreePanelBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(268, 520), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~SceneTreePanelBase();
};
#endif // SP_USE_EDITOR
