#ifdef SP_USE_EDITOR

#include "SceneTreePanel.h"
SceneTreePanel::SceneTreePanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : SceneTreePanelBase(parent, id, pos, size, style, name)
{
}

SceneTreePanel::~SceneTreePanel()
{
}

void SceneTreePanel::PopulateSceneTree(StoneyCore::SceneGraph& scene) {
	stVector<wxDataViewItem> items;

	for (auto& batch : scene.batches)
	{
		for (auto& renderable : batch.renderables)
		{
			for (auto& transform : renderable.transforms)
			{
			}
		}
	}
}
#endif // SP_USE_EDITOR