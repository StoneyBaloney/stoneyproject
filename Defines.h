#pragma once

//#define USE_SDL
#define USE_OPENGL
//#define STONEYDLL extern "C" __declspec(dllexport)

#if defined(_MSC_VER)
#define ALIGNED_(x) __declspec(align(x))
#else
#if defined(__GNUC__)
#define ALIGNED_(x) __attribute__ ((aligned(x)))
#endif
#endif

#define MAX_TEXTURE_ARRAY_ELEMENTS 16

#ifndef STONEY_MAX_TASKS
#define STONEY_MAX_TASKS 2048
constexpr auto MAX_TASKS = STONEY_MAX_TASKS;
#endif // !STONEY_MAX_TASKS

#ifndef STONEY_MAX_DATA_TYPES
#define STONEY_MAX_DATA_TYPES 2048
constexpr auto MAX_DATA_TYPES = STONEY_MAX_DATA_TYPES;
#endif // !STONEY_MAX_DATA_TYPES

#ifndef STONEY_MAX_RENDER_BUFFERS
#define STONEY_MAX_RENDER_BUFFERS 3
constexpr auto MAX_RENDER_BUFFERS = STONEY_MAX_RENDER_BUFFERS;
#endif // !STONEY_MAX_DATA_TYPES

#ifndef STONEY_MAX_RENDER_STATES
#define STONEY_MAX_RENDER_STATES 2048
constexpr auto MAX_RENDER_STATES = STONEY_MAX_RENDER_STATES;
#endif // !STONEY_MAX_RENDER_STATES

#ifndef STONEY_CURRENT_EDITOR_SAVE_VERSION
#define STONEY_CURRENT_EDITOR_SAVE_VERSION 0
constexpr auto CURRENT_EDITOR_SAVE_VERSION = STONEY_MAX_RENDER_BUFFERS;
#endif // STONEY_CURRENT_EDITOR_SAVE_VERSION

#define STONEY_MAX_BATCH_VERTS 100000000
constexpr unsigned int MAX_BATCH_VERTS = 100000000;
constexpr unsigned int MAX_PROGRAMS = 1020;
constexpr unsigned int MAX_VERTEX_BUFFERS = 1020;
constexpr unsigned int MAX_TEXTURE_BUFFERS = 1020;
constexpr unsigned int MAX_VERTEX_ARRAYS = 1020;
constexpr unsigned int MAX_RENDER_FRAMES = 3;

#define ST_SERIALIZABLE(X)\
template <typename Archive>\
inline void serialize(Archive& ar, X& data, unsigned int version) {\
	for_each(refl::reflect(data).members, [&](auto member) {\
		ar & member.get(data);\
	});\
}
#define ST_SERIALIZABLE_ARR_TYPE(TYPE,SIZE)\
template <typename Archive>\
inline void serialize(Archive& ar, TYPE& data, unsigned int version) {\
	for (size_t i = 0; i < SIZE; i++)\
	{\
		ar & data[i];\
	}\
}
