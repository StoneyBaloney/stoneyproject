#pragma once
#ifdef SP_USE_EDITOR
#define SDL_MAIN_HANDLED
#include <wx/wx.h>
#include <wx/nativewin.h>
#include "StoneyEditorMain.h"
#include "EditorSceneManager.h"

using NativeWindowHandle = HWND;

struct WidgetAlloc {
	template <typename T, typename...Args>
	static T* Alloc(Args...args) {
		auto* ret = new T(args...);
		return ret;
	}

	template<typename T>
	static void DeAlloc(T* data) {
		data = nullptr;   //No deleting wx managed memory
	}
};
class StUpdateTimer : public wxTimer {
};
class EditorApp : public wxApp
{
	wxPanel* m_editor_panel_dummy{ nullptr };
	wxNativeContainerWindow* m_editor_panel{ nullptr };
	SDL_Window* m_window{ nullptr };
	StoneyEditorMain* m_wx_main_window{ nullptr };
	DataRegistrar<> data_reg;
	DataRegistrar<WidgetAlloc> reg_managed;
	wxStreamToTextRedirector* m_output_redirector{ nullptr };
	SDL_Window* m_event_other_dummy{ nullptr };
	NativeWindowHandle game_hwnd{ NULL };

	StoneyCore::Editor::EditorSceneManager scene_handler;
	StUpdateTimer* m_render_timer{ nullptr };

	friend class EditorFrame;
public:
	//StoneyCore::SceneHandler scene_handler;
	void ClickPane(wxAuiManagerEvent& evt);
	void ToggleGameWindowDock(wxSizeEvent& evt);
	void EditorFocus(wxCommandEvent& evt);
	void RenderTimerCallback(wxTimerEvent& evt);
	void IdleEvent(wxIdleEvent& evt);
	void GameViewResize(wxSizeEvent& evt);
	virtual bool OnInit();

	virtual int OnExit() {
		m_render_timer->Stop();
		data_reg.ClearAll();
		return 0;
	}

	EditorApp() {};
	~EditorApp() {
		//Disconnect(wxID_ANY, wxEVT_SIZE, wxSizeEventHandler(EditorApp::GameViewResize), NULL, (EditorApp*)wxTheApp);
		//	m_wx_main_window->m_mgr.Disconnect(wxEVT_AUI_PANE_ACTIVATED, wxAuiManagerEventHandler(EditorApp::ClickPane));
	};
};

int main(int argc, char* argv[]);
#endif
