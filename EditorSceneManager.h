#pragma once
#ifdef SP_USE_EDITOR

#include <array>
#include <wx/nativewin.h>
#include <wx/wfstream.h>
#include <magic_enum.hpp>
#include <GL/glew.h>
#include <glm/gtx/intersect.hpp>
#include "GameData.h"
#include "EditorTypes.h"

#include "StPropertyPanel.h"

#include "InstanceRegistry.h"

#include "EditorAssetManager.h"

#include "EditorUtils.h"
#include "Reflection.h"
#include "Timing.h"
namespace StoneyCore
{
	namespace Editor
	{
		struct EditorLightingManager {
			entt::registry m_lights;
		};
		struct MeshShapeHandle {
			stString name;
			uint32 shape_id;
		};
		struct EditorRenderableState {
			stVector<glm::mat4> mats;
			stVector<Transform> transforms;
			stVector<MeshShapeHandle> shape_ids;
			uint32 batch_id{ 0 };
			uint32 version{ 0 };
			uint32 num_verts{ 0 };
			uint32 batch_version{ 0 };
		};
		struct EditorRenderableBatch {
			stVector<EditorRenderableState> batches;
		};
		struct PrefabEditorState {
			StPropertyPage* page{ nullptr };
			PrefabID prefab_id{ 0 };
			bool active = false;
		};
		struct ShaderUniformMeta {
			stString name;
			stString type;
			GLint binding{ 0 };
		};

		struct LightEditorState {
			LightType type{};
			EntType id{};
		};
		struct SelectedInstanceInfo {
			PrefabID prefab_id{};
			EntID entity_id{};
			bool selected{ false };
		};
		struct ShaderMetaData {
			stVector<ShaderUniformMeta> uniform_bindings;
			uint32 shader_id{ 0 };
		};
		const auto sizdfads = sizeof(EditorRenderableState);
		using ShaderIntrospectionCallbackQueue = moodycamel::ConcurrentQueue<StoneyCore::Editor::ShaderMetaData>;
		void DefaultExitCB();
		class EditorSceneManager
		{
		public:
			EditorSceneManager(fu2::function<void(void)> exit_callback = DefaultExitCB);
		private:

			AssetHandler m_asset_handler;

			StoneyCore::InstanceRegistry m_entity_manager2;

			//Editor panels and pages
			StPropertyPanel* m_property_panel{ nullptr };
			StPrefabPanel* m_prefab_panel{ nullptr };
			StShaderPanel* m_shader_panel{ nullptr };
			wxStyledTextCtrl* glsl_scint{ nullptr };
			wxNativeContainerWindow* m_editor_window{ nullptr };
			wxPropertyGridManager* m_editor_prop_mgr{ nullptr };
			StPropertyPanel* m_prop_panel{ nullptr };
			stVector<StPropertyPanel*> panels;
			stVector<StPropertyPage*> m_editor_property_pages;
			std::map<StPropertyPage*, uint32> m_editor_prefab_page_map;
			std::unordered_map<stString, EntType> m_prefab_map;

			PrefabEditorState current_prefab_state;

			//Shader editor

			wxChoice* m_shader_choices{ nullptr };
			ShaderEditorData current_shader_info;

			//Lighting Editor

			StLightEditorPanel* m_lighting_panel{ nullptr };
			LightEditorState m_light_editor_state;
			std::map<int, EntType> m_light_choices;

			//Editor gizmo

			Editor::EditorSceneWidget editor_widget;
			WidgetTransformState m_gizmo_state;
			SelectedInstanceInfo m_selected_instance;
			Transform widget_transform;

			uint32 widget_pid_x{ 0 };
			uint32 widget_pid_y{ 0 };
			uint32 widget_pid_z{ 0 };

			uint32 widget_px_shape_id_x{ 0 };
			uint32 widget_px_shape_id_y{ 0 };
			uint32 widget_px_shape_id_z{ 0 };

			physx::PxRigidActor* widget_actor_x{ nullptr };
			physx::PxRigidActor* widget_actor_y{ nullptr };
			physx::PxRigidActor* widget_actor_z{ nullptr };
			physx::PxTransform widget_px_transform{ physx::PxTransform(0,0,0) };
			physx::PxVec3 m_widget_px_location;

			fu2::function<void(void)> m_exit_callback;
			//Editor Assets
			EditorAssetRegistryArchive m_created_assets;
			float test_light_move_speed{1.f};
			float test_light_diffuse{ 1.f };
			float test_light_constant{ 1.f };
			float test_light_linear{ 0.5f };
			float test_light_quadratic{ 0.01f };
			bool trap_mouse{ false };

		public:

			void StartRenderer();
			void SubmitPendingBatches();
			void ShutdownRenderer();

			bool InitPhysics();
			void UpdateRendererScene();;

			void HandlePropertyChangeEvent(wxCommandEvent& evt);

			HWND InitRendererEditor(Renderer::RendererInitInfo init_info);

			PrefabID AddPrefab(PrefabDescription& description);

			void AddInstance(const PrefabID prefab_id, const Transform& transform) {
				m_entity_manager2.AddInstance(prefab_id, transform);
			}
			uint32 AddMaterial(const StoneyCore::Material& mat) {
				return m_entity_manager2.AddMaterial(mat);
			}
			void SetPropertyPanel(StPropertyPanel* prop_panel);

			void HandleEventsEditor();

			void UpdateAll();


			

			~EditorSceneManager();
		private:
			void HandleApplyShaderChanges(wxCommandEvent& evt);
			void SetWidgetPose();
		};
	}
}

#endif // SP_USE_EDITOR