#pragma once
#include "Defines.h"
#ifdef _WIN32
#ifndef USE_SDL
#include <Windows.h>
namespace WindowsData {
	struct WinData {
		HWND m_hwnd;

		HINSTANCE m_hinstance;

		PWSTR m_pcmdLine;

		HDC m_device_context;
	};

	void SetWinData();

	void SetHwnd(HWND _hwnd);

	const WinData GetWinData();
};

#endif // !USE_SDL
#endif // _WIN32