#pragma once
#include <foonathan/memory/memory_pool.hpp>
#include <boost/pool/object_pool.hpp>
#include "ShaderGenerator.h"
#include "BatchRenderer3D.h"
namespace StoneyCore {
	namespace Renderer {
		//This class is responsible for interactions with the renderer
		class MaterialBuffer {
			stVector<float> m_mat_buffer;
		public:
			uint32 AddMaterial(const Material& mat);
			const stVector<float>& GetMaterials() { return m_mat_buffer; };
		};
		struct ShaderStorage {

			Renderer::ShaderGenParams params;
			stString vertex_shader;
			stString frag_shader;
		};
		class RendererProxy {
			DataRegistrar<> m_data_reg;
			MaterialBuffer materials;
			boost::object_pool<Renderer::RenderBatch2> m_batch_pool;
			Renderer::BatchRenderer3D* m_batch_renderer{ nullptr };
			Renderer::RenderInitializer m_renderer_handle;
			Renderer::RendererConstArgs& m_queues = m_renderer_handle.m_queues;
			stVector<Renderer::RenderBatch2*> pending_batches;
			stSet<Renderer::RenderBatch2*> m_free_batches;
			uint32 m_next_batch_id{ 1 };
			uint32 num_batches{ 0 };
			stMap<int64_t, ShaderStorage> shader_ids;
		public:
			
			void PrepareModelShaders(const StoneyCore::ModelAsset& model, stVector<Renderer::RenderBatch2*>& batches, stVector<ModelAssetDescTable>& model_batch_descs);
			void PrepareModelBatches(const ModelAsset& model, const PrefabDescription& description, stVector<uint32>& out_batch_ids, stVector<ModelAssetDescTable>& model_batch_descs);
			void SubmitShader(const ShaderLoadMessage& shader);
			void StartRenderer();
			void SubmitPendingBatches();
			void ShutdownRenderer();
			void WaitForFreeBatch(Renderer::RenderBatch2* batch);
			uint32 render_buffer_version{ 0 };
			bool BeginSceneUpdate(Renderer::RenderBuffer*& buffer, bool wait_for_batch = false);
			void Shutdown();
			void SinkRenderBuffer(Renderer::RenderBuffer* buffer);
			bool RendererIsValid();
			uint32 AddMaterial(const Material& mat);
			RendererProxy();;
			~RendererProxy();
			void QueueBatches(stVector<RenderBatch2*>& batches);
#ifdef SP_USE_EDITOR
			HWND InitRendererEditor(const Renderer::RendererInitInfo& init_info);
#endif // SP_USE_EDITOR
		};
	}
}