#pragma once
#include "GameData.h"

namespace StoneyCore {
	namespace Renderer {
		constexpr auto VERTEX_BINDING_POSITIONS = 0;
		constexpr auto VERTEX_BINDING_NORMALS = 1;
		constexpr auto VERTEX_BINDING_TRANSFORM = 2;
		constexpr auto VERTEX_BINDING_UV_COORDS = 6;
		constexpr auto VERTEX_BINDING_BITANGENTS = 14;
		constexpr auto VERTEX_BINDING_TANGENTS = 15;
		class ShaderGenerator
		{
		public:
			ShaderGenerator() {
			}
			static bool GenerateShader(const ShaderGenParams params, stString& out_vertex, stString& out_frag);

			static void GenerateDeferredShaderParams(const StoneyCore::ModelAsset& model, stVector<ShaderGenParams>& out_params);
		};

		//void GenerateFragmentOutputOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag);

		void GenerateFragmentTextureInputs(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag);

		//	void GenerateFragOutputLayout(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag);
		//void GenerateUVFragOutputOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag);
		//void GenerateFragmentColorOps(const StoneyCore::Renderer::ShaderGenParams& params, stString& out_frag);
	}
}
