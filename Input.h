#pragma once
#include <stdint.h>
enum class EVENT_TYPE {
	KeyDown,
	KeyUp,
	MouseMovement
};

namespace Input {
	void PumpEvents();
}
