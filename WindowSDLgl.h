#pragma once

#ifdef USE_SDL
#include <vector>

#include <SDL.h>
#include "Defines.h"
#include "SEngineTypes.h"
#include "Enums.h"
#include "WindowData.h"

namespace WindowSDLgl {
	using WindowHandle = SDL_Window *;
	using GLContext = SDL_GLContext;
	typedef uint32_t WindowID;

	SDL_Window* MakeWindow(StoneyCore::WindowInitInfo _init_info);

	GL_Context CreateContextAndMakeCurrent(SDL_Window* window);

	bool MakeContextCurrent(WindowHandle window, GLContext context);

	void ClearContext(WindowHandle window, GLContext context);

	void Swap(WindowHandle window);
};

#endif
