#include "Defines.h"

#ifdef _WIN32
#ifdef SP_USE_EDITOR

#include "SEditor.h"

#else

#ifndef _CONSOLE

#include "WindowsEntry.h"
#else
#include "ConsoleEntry.h"
#endif
#endif
#else
#include "LinuxEntry.h"

#endif // !USE_SDL