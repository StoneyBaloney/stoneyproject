#include "Timing.h"

double StoneyCore::stClock::SetDelta() {
	using namespace std;

	using ms = boost::chrono::duration<double, boost::nano>;
	auto&& now = boost::chrono::high_resolution_clock::now();
	delta = boost::chrono::duration_cast<ms>(now - last).count() * 0.0000001;
	last = now;
	auto start = delta_accum.size() - 1;
	while (start > 0)
	{
		delta_accum[start] = delta_accum[start - 1];
		start--;
	}
	delta_accum[0] = delta;
	delta_avg = 0;
	for (auto& val : delta_accum) {
		delta_avg += val;
	}
	delta_avg /= 3;

	return delta;
}