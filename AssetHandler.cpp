#include <magic_enum.hpp>
#include "AssetHandler.h"
#include <fstream>
#include <sstream>
#define STB_IMAGE_IMPLEMENTATION
#include <stbimage/stb_image.h>

namespace StoneyCore {
	bool AssetHandler::LoadTextFileToString(std::string& output_string, const std::string& _filename)
	{
		return LoadTextFileToString(output_string, _filename.c_str());
	}
	bool AssetHandler::LoadTextFileToString(std::string& output_string, const char* _filename)
	{
		output_string = {};
		std::ifstream filestream;
		filestream.open(_filename);
		if (filestream.fail()) {
			filestream.close();
			return false;
		}
		std::stringstream stream;
		stream << filestream.rdbuf();
		output_string = stream.str();

		return true;
	}
	void AssetHandler::FreeTexture(Texture& texture)
	{
		if (texture.image)
		{
			stbi_image_free(texture.image);
			texture.image = nullptr;
		}
	}
	ASSET_LOAD_CODE AssetHandler::LoadTexture(StoneyCore::Texture& texture, const char* _filename, int desired_components, std::string& warn)
	{
		using lc = ASSET_LOAD_CODE;
		if (std::filesystem::exists(_filename))
		{
			Texture _texture;
			warn = {};
			_texture.image = stbi_loadf(_filename, &_texture.width, &_texture.height, &_texture.num_components,0);
			if (_texture.image) {
				texture = _texture;
				texture.valid = true;
				return lc::LOAD_CODE_success;
			}
			else {
				warn = "Load image failed.";
				texture.valid = false;
				return lc::LOAD_CODE_unknown_failure;
			}
		}
		else {
			texture.valid = false;
			warn = Concat("Could not find file ", std::string(_filename), ".");
			return lc::LOAD_CODE_invalid_filename;
		}
	}

	bool AssetHandler::GetTextureInfo(const char* filename, TextureInfo& info) {
		return stbi_info(filename, &info.width, &info.height, &info.num_components) != 0;
	}
	bool AssetHandler::GetTextureInfo(const char* filename, int* width, int* height, int* num_components) {
		return stbi_info(filename, width, height, num_components) != 0;
	}

	//bool AssetHandler::ExportModelToTextAsCPP(const char* _filename)
	//{
	//	using namespace Renderer;
	//	ModelData model;
	//	std::string warn, err;
	//	if (LoadModel(model, _filename, warn, err))
	//	{
	//		std::string new_filename = _filename;
	//		new_filename += "stoney_exported_code";
	//		std::ofstream outputfile(new_filename);
	//		auto total_verts = [&]() {
	//			size_t ret = 0;
	//			for (auto& shape : model.shapes) {
	//				ret += shape.mesh.indices.size();
	//			}
	//			return ret;
	//		};

	//		auto current_shape = 1;
	//		auto last_shape = model.shapes.size();
	//		for (auto& shape : model.shapes) {
	//			outputfile << " constexpr auto GetShape" << current_shape << "(){ \n\t\t using namespace glm;\n";
	//			outputfile << "std::array<vec3," << shape.mesh.indices.size() << "> ret = {\n\t\t";
	//			auto current_index = 1;
	//			auto last_index = shape.mesh.indices.size();
	//			auto line_helper = 0;
	//			for (auto& index : shape.mesh.indices) {
	//				outputfile << "vec3(";
	//				outputfile << model.attrib.vertices[3 * index.vertex_index];
	//				outputfile << ",";
	//				outputfile << model.attrib.vertices[3 * index.vertex_index + 1];
	//				outputfile << ",";
	//				outputfile << model.attrib.vertices[3 * index.vertex_index + 2];
	//				if ((current_shape == last_shape) && (current_index == last_index))
	//				{
	//					outputfile << ")";
	//				}
	//				else {
	//					outputfile << "),";
	//				}
	//				current_index++;

	//				line_helper++;
	//				if (line_helper % 3 == 0)
	//				{
	//					outputfile << "\n\t\t";
	//				}
	//			}
	//			current_shape++;
	//			outputfile << "}; \n \t\treturn ret; } \n\n";
	//		}

	//		outputfile.close();
	//	}

	//	return false;
	//}

	bool AssetHandler::LoadShaderToLoadMsg(ShaderLoadMessage& load_msg) {
		auto& load_info = load_msg.load_info;
		return AssetHandler::LoadTextFileToString(load_msg.vert_shader, load_info.vs_filename)
			&& AssetHandler::LoadTextFileToString(load_msg.frag_shader, load_info.fs_filename);
	}

	bool AssetHandler::LoadShaderToLoadMsg(ShaderLoadMessage& load_msg, const char* vs_filename, const char* fs_filename) {
		load_msg.load_info.vs_filename = vs_filename;
		load_msg.load_info.fs_filename = fs_filename;
		return LoadShaderToLoadMsg(load_msg);
	}

	void SetTextureParams(aiTextureMapMode texture_wrap_u, StoneyCore::TEX_WRAP_MODE& target)
	{
		aiTextureMapMode mode = texture_wrap_u;
		using wm = TEX_WRAP_MODE;

		switch (texture_wrap_u)
		{
		case aiTextureMapMode_Wrap:
		{
			target = wm::Wrap;
			break;
		}
		case aiTextureMapMode_Clamp:
		{
			target = wm::Clamp;
			break;
		}
		case aiTextureMapMode_Decal:
		{
			target = wm::Decal;
			break;
		}
		case aiTextureMapMode_Mirror:
		{
			target = wm::Mirror;
			break;
		}
		default:
			break;
		}
	}

	bool GetTextureInfo(const aiMaterial* material, aiTextureType type, stVector<StoneyCore::AssetMeta>& out_asset_metas, const std::string& model_directory, stString& out_tex_filename, StoneyCore::TextureInfo& tex_info)
	{
		bool ret = false;
		if (material->GetTextureCount(type))
		{
			aiString path{};
			if (material->GetTexture(type, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
			{
				aiTextureMapMode tex_param_u{};
				aiTextureMapMode tex_param_v{};
				if (material->Get(AI_MATKEY_MAPPINGMODE_U(type, 0), tex_param_u) == AI_SUCCESS)
				{
					SetTextureParams(tex_param_u, tex_info.map_u);
				}
				if (material->Get(AI_MATKEY_MAPPINGMODE_V(type, 0), tex_param_v) == AI_SUCCESS)
				{
					SetTextureParams(tex_param_v, tex_info.map_v);
				}

				auto& out_tex = out_asset_metas.emplace_back();

				auto texture_path = std::filesystem::path(path.C_Str());
				auto model_dir = std::filesystem::path(model_directory);
				auto textures_dir = std::filesystem::path("Textures/");

				bool valid = true;
				if (std::filesystem::exists(texture_path))
				{
					out_tex.filename = texture_path.string();
				}
				else if (std::filesystem::exists(model_dir / texture_path))
				{
					out_tex.filename = (model_dir / texture_path).string();
				}
				else if (std::filesystem::exists(textures_dir / texture_path.filename()))
				{
					out_tex.filename = (textures_dir / texture_path.filename()).string();
				}
				else if (std::filesystem::exists(textures_dir / texture_path))
				{
					out_tex.filename = (textures_dir / texture_path).string();
				}
				else {
					valid = false;
				}
				if (valid)
				{
					out_tex_filename = out_tex.filename;
				}
				ret = valid;
			}
		}
		return ret;
	}
	auto GetaiTextureType(uint32 type, bool swap_disp = false) {
		aiTextureType ret{ aiTextureType_DIFFUSE };
		switch (type)
		{
		case StoneyCore::TEX_TYPE_DIFFUSE:
			ret = aiTextureType_DIFFUSE;
			break;
		case StoneyCore::TEX_TYPE_NORM:
			if (swap_disp)
			{
				ret = aiTextureType_DISPLACEMENT;
			}
			else {
				ret = aiTextureType_NORMALS;
			}
			break;
		case StoneyCore::TEX_TYPE_SPECULAR:
			ret = aiTextureType_SPECULAR;
			break;
		case StoneyCore::TEX_TYPE_AMBIENT:
			ret = aiTextureType_AMBIENT;
			break;
		case StoneyCore::TEX_TYPE_EMISSIVE:
			ret = aiTextureType_EMISSIVE;
			break;
		case StoneyCore::TEX_TYPE_HEIGHT:
			ret = aiTextureType_HEIGHT;
			break;
		case StoneyCore::TEX_TYPE_OPACITY:
			ret = aiTextureType_OPACITY;
			break;
		case StoneyCore::TEX_TYPE_LIGHTING:
			ret = aiTextureType_LIGHTMAP;
			break;
		default:
			LogErrorToFile("Invalid parameters for texture type. See Assethandler.cpp");
			break;
		}
		return ret;
	}

	void LoadMeshTextureInfo(const aiMaterial* material, const std::string& model_directory, stVector<StoneyCore::AssetMeta>& out_asset_metas, StoneyCore::MeshData& out_mesh, std::array<TextureInfo, 8> & tex_infos, bool& out_swap_disp)
	{
		stSet<aiTextureType> tex_types;
		for (int i = 0; i < AI_TEXTURE_TYPE_MAX; i++)
		{
			auto type = magic_enum::enum_cast<aiTextureType>(i);
			if (type.has_value())
			{
				auto ai_type = type.value();
				if (material->GetTextureCount(ai_type))
				{
					tex_types.insert(ai_type);
				}
			}
		}
		bool swap_disp{ false };
		if (tex_types.find(aiTextureType_NORMALS) == tex_types.end())
		{
			if (tex_types.find(aiTextureType_DISPLACEMENT) != tex_types.end())
			{
				swap_disp = true;
			}
		}

					auto names = magic_enum::enum_names<TEXTURE_TYPE>();
		for (uint32 type = 0; type < MAX_TEXTURE_CHANNELS; type++)
		{
			auto& texture_info = tex_infos[type];
			auto ai_type = GetaiTextureType(type, swap_disp);

			if (GetTextureInfo(material, ai_type, out_asset_metas, model_directory, out_mesh.textures[type], texture_info))
			{
				if (type != TEX_TYPE_AMBIENT || (out_mesh.textures[TEX_TYPE_DIFFUSE] != out_mesh.textures[TEX_TYPE_AMBIENT]))
				{
					out_mesh.mesh_info.texture_flags |= (1 << type);
					AssetHandler::GetTextureInfo(out_mesh.textures[type].c_str(), texture_info);
					auto enum_type = (TEXTURE_TYPE)type;
					//texture_info.type = enum_type.has_value() ? enum_type.value(): TEX_TYPE_DIFFUSE;
				
					auto name = names[type];
						LogErrorToFile(Concat(name));
					
				
				}
			};
		}
		out_swap_disp = swap_disp;
	}

	void LoadMeshUVCoords(const aiMaterial* material, const aiMesh* mesh, StoneyCore::ModelVertices& out_vertices, const bool swap_disp)
	{
		float insert_buffer[2];
		const size_t num_verts = mesh->mNumVertices;
		stMap<uint32, uint32> uv_map;
		for (uint32 channel = 0; channel < 8; channel++)
		{
			auto type = GetaiTextureType(channel, swap_disp);
			int uv_channel_id{};
			if (material->GetTextureCount(type) && material->Get(AI_MATKEY_UVWSRC(type, 0), uv_channel_id) == AI_SUCCESS)
			{
				if (mesh->mTextureCoords[uv_channel_id])
				{
					auto& out_uv_channel = out_vertices.texcoords[channel];
					auto& out_index = out_vertices.uv_channel_indices[channel];

					if (uv_map.find(uv_channel_id) == uv_map.end())
					{
						uv_map[uv_channel_id] = channel;

						for (size_t i = 0; i < num_verts; i++)			//load the texture coords
						{
							insert_buffer[0] = mesh->mTextureCoords[uv_channel_id][i].x;
							insert_buffer[1] = mesh->mTextureCoords[uv_channel_id][i].y;
							out_uv_channel.insert(out_uv_channel.end(), &insert_buffer[0], &insert_buffer[2]);
						}
					}
					out_index = uv_map[uv_channel_id];
				}
			}
		}
	}

	void LoadMeshMaterialInfo(const aiScene* scene, const aiMesh* mesh, const std::string& model_directory, stVector<StoneyCore::AssetMeta>& out_asset_metas, StoneyCore::MeshData& out_mesh, std::array<TextureInfo, 8> & tex_info, StoneyCore::ModelVertices& out_model)
	{
		//Get and validate textures for this model
		auto material = scene->mMaterials[mesh->mMaterialIndex];
		if (material)
		{
			bool swap_disp{ false };
			LoadMeshTextureInfo(material, model_directory, out_asset_metas, out_mesh, tex_info, swap_disp);
			LoadMeshUVCoords(material, mesh, out_model, swap_disp);
		}
	}

	inline void LoadMeshGeometry(const aiMesh* mesh, StoneyCore::ModelVertices& out_vertices)
	{
		unsigned int next_indices[3]{};

		float next_texcoords[2]{};
		for (size_t i = 0; i < mesh->mNumFaces; i++)
		{
			auto& face = mesh->mFaces[i];
			for (size_t i = 0; i < face.mNumIndices; i++)
			{
				next_indices[i] = face.mIndices[i];
			}
			out_vertices.indices.insert(out_vertices.indices.end(), &next_indices[0], &next_indices[3]);
		}
		size_t num_verts = mesh->mNumVertices;
		out_vertices.vertices.reserve(3ull * num_verts);
		out_vertices.normals.reserve(3ull * num_verts);
		for (size_t channel = 0; channel < 8; channel++)
		{
			if (mesh->mTextureCoords[channel])
			{
				auto& uv_channel = out_vertices.texcoords[channel];
				uv_channel.reserve(3ull * num_verts);
			}
		}

		float insert_buffer[3]{};
		if (num_verts)
		{
		
			for (size_t i = 0; i < num_verts; i++)			//load the positions
			{
				insert_buffer[0] = mesh->mVertices[i].x;
				insert_buffer[1] = mesh->mVertices[i].y;
				insert_buffer[2] = mesh->mVertices[i].z;
				out_vertices.vertices.insert(out_vertices.vertices.end(), &insert_buffer[0], &insert_buffer[3]);


			}
			if (mesh->HasNormals())
			{

				for (size_t i = 0; i < num_verts; i++)			//load normals
				{
					insert_buffer[0] = mesh->mNormals[i].x;
					insert_buffer[1] = mesh->mNormals[i].y;
					insert_buffer[2] = mesh->mNormals[i].z;
					out_vertices.normals.insert(out_vertices.normals.end(), &insert_buffer[0], &insert_buffer[3]);
				}
			}
			if (mesh->HasTangentsAndBitangents())
			{
				for (size_t i = 0; i < num_verts; i++)			//load tangents
				{
					insert_buffer[0] = mesh->mTangents[i].x;
					insert_buffer[1] = mesh->mTangents[i].y;
					insert_buffer[2] = mesh->mTangents[i].z;
					out_vertices.tangents.insert(out_vertices.tangents.end(), &insert_buffer[0], &insert_buffer[3]);

					insert_buffer[0] = mesh->mBitangents[i].x;
					insert_buffer[1] = mesh->mBitangents[i].y;
					insert_buffer[2] = mesh->mBitangents[i].z;
					out_vertices.bitangents.insert(out_vertices.bitangents.end(), &insert_buffer[0], &insert_buffer[3]);
				}
			}
		}







	}

	void LoadSceneNodesRecursive(const aiNode* node, const aiScene* scene, const std::string& model_directory, StoneyCore::ModelAsset& model, stModelHierarchy& mesh_node, stVector<StoneyCore::AssetMeta>& texture_infos) {
		for (size_t i = 0; i < node->mNumChildren; i++)
		{
			size_t current_node_index = mesh_node.child_nodes.size();
			auto& child_node = mesh_node.child_nodes.emplace_back();

			LoadSceneNodesRecursive(node->mChildren[i], scene, model_directory, model, mesh_node.child_nodes[current_node_index], texture_infos);
		}

		for (size_t i = 0; i < node->mNumMeshes; i++)
		{
			auto mesh_index = node->mMeshes[i];

			auto* mesh = scene->mMeshes[mesh_index];

			if (!mesh->mNumFaces)
			{
				continue;
			}

			mesh_node.mesh_indices.emplace_back(uint32(model.meshes.size()));
			auto& out_mesh = model.meshes.emplace_back();

			auto& tex_info = model.texture_infos.emplace_back();
			auto& out_vertices = model.vertices.emplace_back();
			out_mesh.name = mesh->mName.C_Str();
			


			LoadMeshMaterialInfo(scene, mesh, model_directory, texture_infos, out_mesh, tex_info, out_vertices);

			LoadMeshGeometry(mesh, out_vertices);
		}
	}

	ASSET_LOAD_CODE AssetHandler::LoadMesh(StoneyCore::ModelAsset& model_data, stVector<StoneyCore::AssetMeta>& texture_infos, const char* _filename, unsigned int flags) {
		using lc = ASSET_LOAD_CODE;
		std::filesystem::path mesh_path(_filename);
		if (!std::filesystem::exists(mesh_path))
		{
			return LOAD_CODE_invalid_filename;
		}

		Assimp::Importer importer;
		
		auto scene_p = importer.ReadFile(_filename, flags);

		if (!scene_p)
		{
			std::string err = importer.GetErrorString();
			LogErrorToFile(err);

			return LOAD_CODE_unknown_failure;
		}

		if (!scene_p->HasMeshes())
		{
			return LOAD_CODE_invalid_format;
		}
		
		auto root = scene_p->mRootNode;

		if (root->mNumMeshes)
		{
			model_data.has_root_meshes = false;
		}

		auto mesh_dir = mesh_path.root_path().string() / mesh_path.relative_path();
		mesh_dir.remove_filename();

		LoadSceneNodesRecursive(root, scene_p, mesh_dir.string(), model_data, model_data.hierarchy, texture_infos);

		return LOAD_CODE_success;
	}
	void AssetHandler::SaveTextOverwrite(const stString & filename, const stString & text) {
		using namespace std::filesystem;
		path out_path(filename);		
		if (!is_directory(out_path.parent_path())) {
			
			create_directory(out_path.parent_path());
		}
		std::ofstream file_save(out_path);		
		file_save << text;
		file_save.close();
	}
	StoneyCore::ModelAsset& AssetCache::GetModelAssets(const std::string& model_filename) {
		auto&& asset_id = GetValidAssetID(model_filename, m_model_map);
		auto& meta = m_assets.get<StoneyCore::AssetMeta>(asset_id);
		auto& model = m_assets.get_or_assign<StoneyCore::ModelAsset>(asset_id);
		if (meta.valid)
		{
			return model;
		}
		else {
			stVector<AssetMeta> textures;
			if (m_handler.LoadMesh(model, textures, model_filename.c_str(),
				aiProcess_ValidateDataStructure |
				aiProcess_PreTransformVertices |
				aiProcess_OptimizeMeshes |
				aiProcess_Triangulate | 
				aiProcess_TransformUVCoords |
				aiProcess_GenUVCoords |
				aiProcess_FlipUVs|
				aiProcess_CalcTangentSpace
			)) {
				LogErrorToFile(Concat("Failed to load mesh for file ", model_filename, ".\n"));
			}
			else {
				for (auto& mesh_data : model.meshes) {
					auto& tex = model.textures.emplace_back();
					for (uint32 type = 0; type < MAX_TEXTURE_CHANNELS; type++)
					{
						if (type == TEXTURE_TYPE::TEX_TYPE_AMBIENT)
						{
							if (mesh_data.mesh_info.texture_flags & (1 << TEX_TYPE_DIFFUSE))
							{
								if (mesh_data.textures[TEX_TYPE_DIFFUSE] == mesh_data.textures[TEX_TYPE_AMBIENT])
								{
									break;
								}
							}
						}
						auto flag = (1 << type);
						if (mesh_data.mesh_info.texture_flags & flag)
						{
							
							mesh_data.mesh_info.uses_texture = true;							
							
							auto& texture = GetTexture(mesh_data.textures[type]);
							tex[type] = texture;
							
						}
					}
				}

				meta.valid = true;
			}
		}

		return model;
	}
}