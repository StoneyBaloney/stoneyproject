﻿#pragma once
#include "SRendererGL.h"

#include "RenderUtils.h"

namespace StoneyCore {
	namespace Renderer {
		class BatchRenderer3D
		{
		public:

			RendererConstArgs m_queues;
			moodycamel::ProducerToken free_ptok;
			moodycamel::ProducerToken free_batch_ptok;
			std::map<int64_t, uint32_t> program_id_map;
			std::map<uint32_t, uint32_t> batch_id_map;
			stVector<ShaderProgram> shader_infos;
			RenderStateArray render_states;

			BigBitField<MAX_RENDER_STATES> valid_states;
			BigBitField<MAX_RENDER_STATES> valid_batch_indices;
			Window::WindowHandle main_window{};
			Window::GLContext main_context{};
			std::thread m_thread;
			RendererInitInfo m_init_info{};
			GLuint main_camera_ubo{ 0 };
			LightingRenderState m_lighting_state;
			DeferredPipelineRenderState m_pipeline_state;
			DeferredGbufferGL& m_framebuffer = m_pipeline_state.m_framebuffer;
			uint32_t render_buffer_version{ 0 };
			ShaderLoadMessage shader_msg;
#ifdef SP_USE_EDITOR
			Editor::EditorWidgetRenderState m_editor_widget_state;
			Editor::EditorLightDebugState m_light_debug_state;
			//LightingDebugShapeState m_lighting_shape_state;
			stVector<GL_Indirect> m_editor_widget_indirects;
			HWND InitInEditor(RendererInitInfo init_info);
#endif // SP_USE_EDITOR
			bool is_init = false;
			std::atomic<bool> should_run{ false };

			Window::WindowHandle GetMainWindow() const { return main_window; }
			void Init(RendererInitInfo init_info);

			void DrawAll();

			void DrawBufferForward(StoneyCore::Renderer::RenderBuffer* render_buffer);

			void UpdateLightBuffer(StoneyCore::Renderer::RenderBuffer& render_buffer);

			void UpdateCameraBuffers(StoneyCore::Renderer::RenderBuffer& render_buffer);

			void HandleRenderWindowResize(StoneyCore::Renderer::RenderBuffer& render_buffer);

			void UpdateAndDrawScene();
			void CheckForShaderUpdates();
			void RenderLoop();
			void CheckForRenderTask();
			void Shutdown();
			void Start();
			void ProcessBatchShaderState(const StoneyCore::Renderer::RenderBatch2& batch, StoneyCore::RenderState& batch_state);
			void ProcessBatch2(const Renderer::RenderBatch2& batch);
			bool GenerateRenderState2(RenderState& batch_state, const RenderBatch2& batch);

			void CheckForBatches();

			BatchRenderer3D(RendererConstArgs init_data);

		public:
			~BatchRenderer3D();
		};

		//void DrawEditorGizmo_Forward(StoneyCore::Editor::EditorWidgetRenderState& widget_state, StoneyCore::Renderer::RenderBuffer* render_buffer, stVector<StoneyCore::Renderer::GL_Indirect>& indirects);
	}
}