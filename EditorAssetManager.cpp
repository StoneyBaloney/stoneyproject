#ifdef SP_USE_EDITOR

#include "EditorAssetManager.h"
#include "Reflection.h"
namespace StoneyCore {
	namespace Editor {
		EditorAssetManager::EditorAssetManager()
		{
		}

		EditorAssetManager::~EditorAssetManager()
		{
		}
		bool EditorAssetManager::SavePrefab(std::string& prefab_name) {
			if (m_assets.m_prefab_map.find(prefab_name) == m_assets.m_prefab_map.end())
			{
				return false;
			}
			else {
				return true;
			}
		}
		bool EditorAssetManager::GetPrefab(std::string& prefab_name, PrefabDescription& out_prefab) {
			if (m_assets.m_prefab_map.find(prefab_name) == m_assets.m_prefab_map.end())
			{
				return false;
			}
			else {
				out_prefab = m_assets.m_prefabs[m_assets.m_prefab_map[prefab_name]];
				return true;
			}
		}
		inline bool EditorAssetManager::TryAddPrefab(PrefabDescription& prefab) {
			if (m_assets.m_prefab_map.find(prefab.name) == m_assets.m_prefab_map.end())
			{
				m_assets.m_prefab_map[prefab.name] = uint32(m_assets.m_prefabs.size());
				m_assets.m_prefabs.emplace_back(prefab);
				return true;
			}
			else {
				return false;
			}
		}
	}
}
#endif // SP_USE_EDITOR