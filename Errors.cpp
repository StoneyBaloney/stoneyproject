#include <iostream>
#include <filesystem>
#include <map>
#include <SDL.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <c314Mpmc/concurrentqueue.h>
#include "StoneyMain.h"
#include "Errors.h"

//std::map<VkResult, std::string> error_descriptions = {
//{VkResult::VK_ERROR_DEVICE_LOST,"ERRORS"},
//{VkResult::VK_ERROR_OUT_OF_HOST_MEMORY,"A host memory allocation has failed. " } ,
//{VkResult::VK_ERROR_OUT_OF_DEVICE_MEMORY,"A device memory allocation has failed. " } ,
//{VkResult::VK_ERROR_INITIALIZATION_FAILED,"Initialization of an object could not be completed for implementation - specific reasons. " } ,
//{VkResult::VK_ERROR_DEVICE_LOST,"The logical or physical device has been lost.See Lost Device " } ,
//{VkResult::VK_ERROR_MEMORY_MAP_FAILED,"Mapping of a memory object has failed. " } ,
//{VkResult::VK_ERROR_LAYER_NOT_PRESENT,"A requested layer is not present or could not be loaded. " } ,
//{VkResult::VK_ERROR_EXTENSION_NOT_PRESENT,"A requested extension is not supported. " } ,
//{VkResult::VK_ERROR_FEATURE_NOT_PRESENT,"A requested feature is not supported. " } ,
//{VkResult::VK_ERROR_INCOMPATIBLE_DRIVER,"The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation - specific reasons. " } ,
//{VkResult::VK_ERROR_TOO_MANY_OBJECTS,"Too many objects of the type have already been created. " } ,
//{VkResult::VK_ERROR_FORMAT_NOT_SUPPORTED,"A requested format is not supported on this device. " } ,
//{VkResult::VK_ERROR_FRAGMENTED_POOL,"A pool allocation has failed due to fragmentation of the pool�s memory.This must only be returned if no attempt to allocate host or device memory was made to accomodate the new allocation.This should be returned in preference to VK_ERROR_OUT_OF_POOL_MEMORY, but only if the implementation is certain that the pool allocation failure was due to fragmentation. " } ,
//{VkResult::VK_ERROR_SURFACE_LOST_KHR,"A surface is no longer available. " } ,
//{VkResult::VK_ERROR_NATIVE_WINDOW_IN_USE_KHR,"The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again. " } ,
//{VkResult::VK_ERROR_OUT_OF_DATE_KHR,"A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail.Applications must query the new surface properties and recreate their swapchain if they wish to continue presenting to the surface. " } ,
//{VkResult::VK_ERROR_INCOMPATIBLE_DISPLAY_KHR,"The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image. " } ,
//{VkResult::VK_ERROR_INVALID_SHADER_NV,"One or more shaders failed to compile or link.More details are reported back to the application via .. / .. / html / vkspec.html#VK_EXT_debug_report if enabled. " } ,
//{VkResult::VK_ERROR_OUT_OF_POOL_MEMORY,"A pool memory allocation has failed.This must only be returned if no attempt to allocate host or device memory was made to accomodate the new allocation.If the failure was definitely due to fragmentation of the pool, VK_ERROR_FRAGMENTED_POOL should be returned instead. " } ,
//{VkResult::VK_ERROR_INVALID_EXTERNAL_HANDLE,"An external handle is not a valid handle of the specified type. " } ,
//{VkResult::VK_ERROR_FRAGMENTATION_EXT,"A descriptor pool creation has failed due to fragmentation. " }
//};

Errors::Errors()
{
}
Errors::~Errors()
{
}
#ifdef SP_USE_EDITOR

#endif // !SP_USE_EDITOR
std::string queue_str{};
stString queue_buf;
moodycamel::ConcurrentQueue<std::string> mt_log_queue;

auto rotating_logger = []() {
	if (!std::filesystem::is_directory("Logs")) {
		std::filesystem::create_directory("Logs");
	}
	return spdlog::rotating_logger_mt("some_logger_name", "Logs/rotating.txt", 1048576 * 10, 3);
}();
void PrintErrorQueue(int num_errors) {
	queue_buf = "";
	uint32 num_err{};
	for (size_t i = 0; i < num_errors; i++)
	{
		if (mt_log_queue.try_dequeue(queue_str))
		{
			queue_buf += queue_str;
			queue_buf += "\n";
			num_err++;
		}
	}
	if (num_err)
	{
		std::cout << queue_buf << "\n";
	}
}
void LogErrorToFile(std::string err) {
#ifdef _CONSOLE
	std::cout << err << "\n";
#endif // _CONSOLE

	rotating_logger->info(err);
#ifdef SP_USE_EDITOR

	mt_log_queue.enqueue(err);

#endif // !SP_USE_EDITOR
}

inline void FatalError(const char* _error_msg)
{
#ifndef SP_USE_EDITOR
	StoneyMain::Shutdown();

#endif // !SP_USE_EDITOR
	//Should probably do something useful here.
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Critical Error", _error_msg, NULL);
}

inline void FatalError(stVector<char>& CharVector)
{
	std::string Error = (const char*)(CharVector.data());

#ifndef SP_USE_EDITOR
	StoneyMain::Shutdown();

#endif // !SP_USE_EDITOR
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Critical Error", Error.c_str(), NULL);
}
inline void FatalError(std::string _errorString)
{
#ifndef SP_USE_EDITOR
	StoneyMain::Shutdown();

#endif // !SP_USE_EDITOR
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Critical Error", _errorString.c_str(), NULL);
}

inline void WarnMessage(const std::string& message)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Warning: ", message.c_str(), NULL);
}