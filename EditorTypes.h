#pragma once
#ifdef SP_USE_EDITOR
#include <entt/entity/registry.hpp>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/manager.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "SEngineTypes.h"
#include "Lighting.h"
#include "GameData.h"

class StPropertyPage : public wxPropertyGridPage {
public:

	virtual bool IsHandlingAllEvents() const {
		return true;
	}
};
namespace StoneyCore {
	namespace Editor {
		enum class SHADER_TYPE {
			vert,
			frag,
			invalid
		};
		struct ShaderEditorData {
			std::string filename;
			ShaderLoadInfo load_info;
			SHADER_TYPE type{ SHADER_TYPE::invalid };
			stVector<uint32> affected_batches;
			stVector<uint32> affected_prefabs;
		};
		enum WIDGET_TRANSFORM_TYPE {
			TranslateX,
			TranslateY,
			TranslateZ,
			None
		};
		enum WIDGET_MOVEMENT_MODIFIER {
			ScreenX_Positive,
			ScreenX_Negative,
			ScreenY_Positive,
			ScreenY_Negative,
		};
		using WidgetType = WIDGET_TRANSFORM_TYPE;
		struct WidgetInstanceInfo {
			uint32 entity_id{ 0 };
		};
		struct WidgetTransformState {
			WidgetInstanceInfo instance_info;
			WIDGET_MOVEMENT_MODIFIER screen_modifier;
			WIDGET_TRANSFORM_TYPE transform_type{ WidgetType::None };
			glm::vec2 original_screen_loc{};
			bool active{ false };
		};
		struct EditorLightInfo
		{
		};
		struct EditorLightsManager {
		};
		struct PrefabInfo {
		};

		enum class ATTRIBUTE_TYPE {
			vec2,
			vec3,
			vec4,
			mat3,
			mat4,
			block
		};
		struct UniformAttribute {
			unsigned int location{ 0 };
			ATTRIBUTE_TYPE type;
			wxString name;
		};
		struct VertexAttribute {
			unsigned int binding{ 0 };
			ATTRIBUTE_TYPE type;
			wxString name;
		};
		struct ShaderMeta {
			std::string filename;
			stVector<VertexAttribute> vertex_attributes;
			stVector<wxString> uniform_names;
		};
	}
}

#endif