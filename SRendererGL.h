#pragma once
#include "FramebufferGL.h"
#include "GLErrorHandler.h"
#include "RenderBatch.h"

#ifdef _WIN32
using SP_WINDOW_HANDLE = HWND;
#endif // _WIN32

#ifdef USE_SDL
#include "WindowSDLgl.h"
namespace Window = WindowSDLgl;
typedef SDL_GLContext GL_Context;
#elif _WIN32
#include "WindowWin32gl.h"
namespace Window = WindowWin32gl;
#else
#include "WindowLinuxX.h"
namespace Window = WindowLinuxX;
#endif
namespace StoneyCore {
	namespace Renderer {
		using RenderStateArray = stVector<RenderState>;

		using BatchQueue = moodycamel::ConcurrentQueue<RenderBatch2*>;

		using ShaderQueue = moodycamel::ConcurrentQueue<ShaderLoadMessage>;

		using RenderBufferQueue = moodycamel::ConcurrentQueue<RenderBuffer*>;

		using RenderCallbackQueue = moodycamel::ConcurrentQueue<RenderCallback>;

		using RenderQueue = moodycamel::ConcurrentQueue<RendererTask>;
		struct RendererConstArgs {
			BatchQueue* batch_queue{ nullptr };
			BatchQueue* free_batch_queue{ nullptr };
			RenderBufferQueue* render_buffer_queue{ nullptr };
			RenderBufferQueue* free_buffer_queue{ nullptr };
			RenderQueue* render_queue{ nullptr };
			ShaderQueue* shader_queue{ nullptr };
			RenderCallbackQueue* render_callback_queue{ nullptr };
			bool IsValid();
		};
		struct DeferredPipelineRenderState {
			DeferredGbufferGL m_framebuffer;
			GLuint geometry_pass_shader{ 0 };
			GLuint lighting_pass_shader{ 0 };
			GLuint post_process_pass_shader{ 0 };
			GLuint screen_quad_vao{ 0 };
			GLuint clear_program{};
			bool is_valid{ false };
		};
		struct LightingRenderState {
			GLuint dir_light_ubo;
			GLuint point_light_ubo;
		};
		

		std::array<float, 20> GetNormalizedQuadBufferGL();

		GLuint CreateVAOAndBind();

		void CleanupRenderState(RenderState& render_state);

		GLuint CreateInstanceBuffer(const RenderBatch2& batch);

		

		void CreateElementArray(stVector<GLuint>& out_array, const RenderBatch2& batch);

		GLuint CreateElementBuffer(const stVector<GLuint>& element_array);

		GLuint CreateVBO(const stVector<GLfloat>& buffer, GLuint update_type, const ShaderGenParams & params);

	

		void GenerateIndexedVertexArray(const RenderBatch2& batch, stVector<GLfloat>& buffer);

		void GenerateNonIndexedVertexArray2(const RenderBatch2& batch, stVector<GLfloat>& buffer);



	


		void SetTexParam(StoneyCore::TEX_WRAP_MODE wrap_u, GLenum& tex_param);

	

		

		void GenBatchTexture2(const RenderBatch2& batch, RenderState& out_render_state);

		GLuint GenBatchTexture(RenderBatch2* batch);

		bool VerifyBatch2(const StoneyCore::RenderState& batch_state, const Renderer::RenderBatch2& batch);

		bool VerifyBatch(StoneyCore::RenderState& batch_state, Renderer::RenderBatch2* batch);

		GLuint CreateShader(const std::string& vert_shader, const std::string& frag_shader, const char* vert_filename = nullptr, const char* frag_filename = nullptr);
		GLuint CreateShader(const StoneyCore::ShaderLoadMessage& load_msg);;
		void LinkShaders(const GLuint& ProgramID, const GLuint& VertexShaderID, const GLuint& FragmentShaderID);

		void CompileShader(const std::string& Contents, const GLuint& ID, GLuint ProgramID, const char* filename = nullptr);

		void HandleWindowResizeGL(int x, int y);

		void DrawBuffer_Forward(GLuint& cam_ubo, RenderBuffer* render_buffer, BigBitField<MAX_RENDER_STATES>& valid_batches, RenderStateArray& render_states);

		void DrawBatch_Forward(StoneyCore::RenderState& render_state, StoneyCore::Renderer::BatchRenderData& batch_data);

		void DrawBatchIndexed(StoneyCore::RenderState& render_state, StoneyCore::Renderer::BatchRenderData& batch_data);

		void UpdateAndDrawBatch(StoneyCore::RenderState& render_state, StoneyCore::Renderer::BatchRenderData& batch_data);

		//void DrawBuffer_Deferred(GLuint& cam_ubo, RenderBuffer* render_buffer, BigBitField<MAX_RENDER_STATES> & valid_batches, RenderStateArray& render_states);

		bool InitDeferredFrameBufferGL(DeferredGbufferGL& framebuffer, RendererInitInfo& init_info);
#ifdef SP_USE_EDITOR
		void DrawEditorGizmo_Forward(StoneyCore::Editor::EditorWidgetRenderState& widget_state, StoneyCore::Renderer::RenderBuffer* render_buffer, stVector<StoneyCore::Renderer::GL_Indirect>& indirects);

#endif //SP_USE_EDITOR
	}
}