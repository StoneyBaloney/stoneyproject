#pragma once

#include <atomic>
#include <set>
#ifdef _WIN32
#include <GL/glew.h>
#else
#include <GL/glxew.h>
#endif //
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <c314Mpmc/concurrentqueue.h>

#include <function2/function2.hpp>
#include "SEngineTypes.h"
#include "Lighting.h"

#include "Transform.h"

#ifdef SP_USE_EDITOR

#include "EditorSceneWidget.h"

#endif // SP_USE_EDITOR

namespace physx {
	class PxRigidActor;
}
namespace StoneyCore {
	namespace Editor {
		class EditorSceneHandler;
	}

	struct Transform {
		glm::vec3 location{ 0 };
		glm::quat rotation{};
		glm::vec3 scale{ 1 };
	};
	struct ScreenSettings {
		int width{ 800 };
		int height{ 600 };
	};
	struct SceneObjectHandle {
		uint32_t version{ 0 };
		uint32_t batch_index{ 0 };
		uint32_t instance_index{ 0 };
	};
	struct SceneObject {
		uint32_t version{ 0 };
		Transform m_transform;
		SceneObjectHandle parent;
		uint32_t id{ 0 };
	};
	struct SceneBatch
	{
		uint32_t version{ 0 };
		stVector<glm::mat4> mats;
		stVector<SceneObject> transforms;
		uint32_t num_verts{ 0 };
		bool updated{ 0 };
	};
	struct RenderableNode {
		uint32_t version{ 0 };
		uint32_t id{ UINT32_MAX };
		uint32_t batch_version{ 0 };
		stVector<SceneBatch> renderables;
	};
	struct SceneGraph {
		uint32_t version{ 0 };
		uint32_t id{};
		stVector<RenderableNode> batches;
	};
	struct PrefabHandle {
		uint32 prefab_index{ 0 };
		uint32 batch_index{ 0 };
		uint32 renderable_index{ 0 };
		uint32 m_physics_handle{ 0 };
	};

	struct InstanceHandle {
	private:
		PrefabHandle prefab_handle;
		uint32_t instance_index{ 0 };
		uint32 physics_handle{ 0 };

		friend class SceneHandler;
#ifdef SP_USE_EDITOR
		friend class Editor::EditorSceneHandler;
#endif // SP_USE_EDITOR
	};

	enum class RENDER_CB_TYPE
	{
		Success,
		BatchLoadFailure,
		RuntimeFailure
	};

	enum UPDATE_TYPE {
		Static,
		Dynamic
	};
	enum VERTEX_SPEC_TYPE {
		NonIndexed,
		Indexed
	};
	struct PrefabIndex {
		PrefabIndex() :
			prefab_index(0)
		{
		};
		void SetIndex(size_t index) { prefab_index = index; };
		size_t GetIndex() const { return prefab_index; };
	private:
		size_t prefab_index = 0;
	};
	struct PrefabAssetIndices {
		size_t model_index;
		size_t texture_index;
		size_t renderable_index;
		size_t shader_index;
		size_t transform_index;
		PrefabIndex prefab_index;
	};

	struct ShaderLoadInfo {
		ShaderLoadInfo(const ShaderLoadInfo& _other) = default;
		ShaderLoadInfo() = default;
		stString vs_filename;
		stString fs_filename;
		stString shader_name;
		bool operator ==(const ShaderLoadInfo& _other)
		{
			return (fs_filename == _other.fs_filename) && (vs_filename == _other.vs_filename);
		}

		/*	bool operator &(const ShaderLoadInfo& _other)
			{
				return (fs_filename == _other.fs_filename) || (vs_filename == _other.vs_filename);
			}*/
		bool operator !=(const ShaderLoadInfo& _other)
		{
			return (fs_filename != _other.fs_filename) || (vs_filename != _other.vs_filename);
		}
	};
	enum class SHADER_PASS_TYPE {
		SG_Renderpass_Forward,
		SG_Renderpass_Geometry,
		SG_Renderpass_Lighting,
		SG_Renderpass_PostProcess
	};
	struct ShaderLoadMessage {
		ShaderLoadInfo load_info{ ".","." };
		stString vert_shader;
		stString frag_shader;
		SHADER_PASS_TYPE shader_type{ SHADER_PASS_TYPE::SG_Renderpass_Forward };
		int64_t program_id{ 0 };
	};
	enum class TEX_WRAP_MODE {
		Clamp,
		Wrap,
		Decal,
		Mirror,
		Other
	};
	struct RenderState {
		GLuint program_id{};
		GLuint geometry_pass_id{};
		GLuint render_type{};
		GLuint vao_id{};
		GLuint vbo_id{};
		GLuint instance_vbo{};
		GLuint element_buffer{};
		GLuint material_vbo{};
		stVector<std::array<GLuint,2>> texture_ids; 
		GLuint instance_buffer_size{};
		GLuint update_type{};
		GLuint version{ UINT32_MAX };
		GLuint num_renderables{};
		uint32_t batch_version{ 0 };
		operator bool()const {
			return vao_id
				&& vbo_id
				&& instance_vbo
				&& update_type;
		}
	};
	struct Material {
		vec3 ambient{ 1 };
		vec3 diffuse{ 1 };
		vec3 specular{ 0 };
		vec3 shininess{ 0 };
	};
	constexpr uint32 RG_NUM_MATERIALS = 4u;
	enum TEXTURE_TYPE : int32_t {
		TEX_TYPE_DIFFUSE = 0,
		TEX_TYPE_NORM = 1,
		TEX_TYPE_SPECULAR = 2,
		TEX_TYPE_AMBIENT = 3,
		TEX_TYPE_EMISSIVE = 4,
		TEX_TYPE_HEIGHT = 5,
		TEX_TYPE_OPACITY = 6,
		TEX_TYPE_LIGHTING = 7
	};
	constexpr uint32 MAX_TEXTURE_CHANNELS = 8;

	struct TextureInfo {
		int width{};
		int height{};
		int num_components{};
		TEX_WRAP_MODE map_u{ TEX_WRAP_MODE::Wrap };
		TEX_WRAP_MODE map_v{ TEX_WRAP_MODE::Wrap };
		TEXTURE_TYPE type{ TEX_TYPE_DIFFUSE };
		bool operator ==(const TextureInfo& other)const {
			return
				width == other.width &&
				height == other.height &&
				num_components == other.num_components &&
				map_u == other.map_u &&
				map_v == other.map_v;
		}
		bool operator !=(const TextureInfo& other)const {
			return !((*this) == other);
		}
	};
	struct Texture {
		Texture() = default;
		Texture(const Texture& other) = default;
		float* image{ nullptr };
		int width{ 0 };
		int height{ 0 };
		int num_components{ 0 };
		bool valid{ false };
	};
	const auto adsfdsfff = sizeof(TextureInfo);
	enum class BATCH_RENDER_TYPE {
		Deferred_Default,
		Deferred_Custom,
		Forward_Default,
		Forward_Custom
	};

	struct RenderSpec {
		RenderSpec() = default;
		RenderSpec(const RenderSpec& other) = default;
		//uint32_t shader_id{0};
		GLuint draw_type{ GL_TRIANGLES };
		UPDATE_TYPE update_type{ UPDATE_TYPE::Static };
		VERTEX_SPEC_TYPE vertex_type{ VERTEX_SPEC_TYPE::NonIndexed };
		BATCH_RENDER_TYPE render_type{ BATCH_RENDER_TYPE::Deferred_Default };
		bool uses_material{ false };
		bool use_texture{ false };

		GLuint GetUpdateTypeGL()const {
			return update_type == UPDATE_TYPE::Static ? GL_STATIC_DRAW : GL_STREAM_DRAW;;
		}
		bool operator==(const RenderSpec& _other)const {
			return (update_type == _other.update_type) &&
				(vertex_type == _other.vertex_type) &&
				//	(shader_id == _other.shader_id) &&
				(draw_type == _other.draw_type) &&
				(uses_material == _other.uses_material) &&
				(use_texture == _other.use_texture);
		}
		bool operator!=(const RenderSpec& _other)const {
			return (update_type != _other.update_type) ||
				(vertex_type != _other.vertex_type) ||
				//	(shader_id != _other.shader_id) ||
				(draw_type != _other.draw_type) ||
				(uses_material != _other.uses_material) ||
				(use_texture != _other.use_texture);
		}
	};
	struct RenderInfo {
		RenderInfo(const RenderInfo& _other);
		ShaderLoadInfo shader_stages;
		UPDATE_TYPE render_type;
		VERTEX_SPEC_TYPE vertex_type;
		RenderInfo() = default;
		RenderInfo(const ShaderLoadInfo& _shader_stages, const UPDATE_TYPE& _render_type, const VERTEX_SPEC_TYPE& _vertex_type) :
			shader_stages(_shader_stages),
			render_type(_render_type),
			vertex_type(_vertex_type)
		{
		}

		bool operator==(const RenderInfo& _other) {
			return (shader_stages == _other.shader_stages) && (render_type == _other.render_type) && (vertex_type == _other.vertex_type);
		}
		bool operator!=(const RenderInfo& _other) {
			return (shader_stages != _other.shader_stages) || (render_type != _other.render_type) || (vertex_type != _other.vertex_type);
		}
	};

	//To be replaced
	struct PrefabDescription {
		//StEDITOR_EXP()
		PrefabDescription() = default;
		//PrefabDescription(const PrefabDescription& other) = default;

		stString name;
		stString model_filename;
		RenderSpec  render_spec;

	private:
	};

	struct _Transform {
		stVector<glm::vec4> instance_locations;
	};
	struct SceneDescription {
		stVector<PrefabDescription> descriptions;
	};

	const auto sizeofPrefabDesc = sizeof(PrefabDescription);

	namespace AssetManager {
		typedef float* STB_Image;
		struct ImageData {
			ImageData() :
				image(0),
				width(0),
				height(0),
				num_components(0)

			{}
			ImageData(const ImageData& _image_data) :
				image(_image_data.image),
				width(_image_data.width),
				height(_image_data.height),
				num_components(_image_data.num_components)

			{}

			STB_Image image;
			int width;
			int height;

			int num_components;

			void Empty();
		};

		enum class ASSET_TYPE : char {
			Texture,
			Model,
			ShaderStage,
			Font
		};
	}

	struct MeshInfo {
		uint32 texture_flags{};
		bool uses_texture{ false };
		bool uses_material{ false };
	};
	struct ModelVertices {
		stVector<float> vertices;
		stVector<float> normals;
		stVector<float> tangents;
		stVector<float> bitangents;
		std::array<stVector<float>, 8> texcoords; // 8 channels
		std::array<int8_t, 8> uv_channel_indices;

		stVector<uint32_t> indices;
	};

	struct MeshData {
		stString name;
		std::array<stString, 8> textures{};
		MeshInfo mesh_info;
	};

	struct stModelHierarchy {
		stVector<uint32> mesh_indices;
		stVector<stModelHierarchy> child_nodes;
	};
	struct ModelAsset {
		bool has_root_meshes{ false };
		stModelHierarchy hierarchy;
		stVector<MeshData> meshes;
		stVector<ModelVertices> vertices;
		//stVector<TextureInfo> diffuse_tex_infos;
		stVector<std::array<TextureInfo, MAX_TEXTURE_CHANNELS>> texture_infos; // 8 uv channels
		stVector<std::array<Texture, MAX_TEXTURE_CHANNELS>> textures;
	};

	struct ModelAssetDescTable {
		stVector<uint32> textured_meshes;
		stVector<stVector<uint32>> textured_mesh_groups;
		stVector<uint32> untextured_meshes;
	};

	namespace Renderer {
		enum SHADER_GEN_TYPE : int8_t {
			SHADER_GEN__Forward = 0,
			SHADER_GEN_Deffered_Geometry = 1
		};
		enum SHADER_GEN_VERTEX_INPUT {
			SHADER_IN_POSITION = 1 << 0,
			SHADER_IN_NORMAL = 1 << 1,
			SHADER_IN_UV_COORDS = 1 << 2,
			SHADER_IN_WORLD_TRANSFORM = 1 << 3,
			SHADER_IN_TANGENTS = 1 << 4,
			SHADER_IN_PROJECTION = 1 << 5
		};
		enum SHADER_GEN_VERTEX_OUTPUT {
			VERT_OUT_FRAG_POSITION = 1 << 0,
			VERT_OUT_NORMAL = 1 << 1,
			VERT_OUT_UV_COORDS = 1 << 2,
			VERT_OUT_FLAT_CAMERA_POS = 1 << 3,
			VERT_OUT_DRAW_ID = 1 << 4,
			VERT_OUT_COLOR = 1 << 5
		};

		enum SHADER_GEN_FRAG_OUTPUT {
			SHADER_OUT_FRAG_COLOR = 1 << 0,
			SHADER_OUT_FRAG_NORMAL_AND_DEPTH = 1 << 1 // Deferred only
		};

		// SHADER_IN_POSITION | SHADER_IN_NORMAL | SHADER_IN_UV_COORDS | SHADER_IN_WORLD_TRANSFORM | SHADER_IN_PROJECTION | SHADER_IN_WORLD_VIEW
	//	constexpr int8_t SHADER_DEFAULT_INPUTS = SHADER_IN_POSITION | SHADER_IN_NORMAL | SHADER_IN_WORLD_TRANSFORM | SHADER_IN_PROJECTION;
		//constexpr int8_t SHADER_DEFAULT_OUTPUTS = VERT_OUT_FRAG_POSITION | VERT_OUT_NORMAL | VERT_OUT_DRAW_ID | VERT_OUT_FLAT_CAMERA_POS;
		//constexpr int8_t SHADER_DEFAULT_FRAG_OUTPUTS = SHADER_GEN_FRAG_COLOR | SHADER_GEN_FRAG_NORMAL_AND_DEPTH;
		//constexpr char SHADER_GEN_DEFAULT_UV_CHANNELS = 0;

		struct ShaderGenParams {
			int8_t vertex_inputs{};
			int8_t vertex_outputs{};
			int8_t frag_outputs{};
			SHADER_GEN_TYPE gen_type{};
			std::array<int8_t, MAX_TEXTURE_CHANNELS> uv_channels{};
			int8_t texture_flags{};

			bool operator ==(const ShaderGenParams& other) {
				return
					vertex_inputs == other.vertex_inputs &&
					vertex_outputs == other.vertex_outputs &&
					frag_outputs == other.frag_outputs &&
					gen_type == other.gen_type &&
					uv_channels == other.uv_channels;
			};
		};

		using ShaderText = stString;
		struct RendererInitInfo {
			int window_width;
			int window_height;
		};
		enum class RENDERER_REQUEST_TYPE {
			ReloadBatches
		};

		struct GL_Indirect {
			GLuint  count{ 0 };
			GLuint  instanceCount{ 0 };
			GLuint  first{ 0 };
			GLuint  baseInstance{ 0 };
			GL_Indirect(GLuint _count = 0, GLuint _instance_count = 0, GLuint _first = 0, GLuint _base_instance = 0) :
				count(_count), instanceCount(_instance_count), first(_first), baseInstance(_base_instance) { };
		};
		struct GL_IndexedIndirect {
			GLuint  count{ 0 };
			GLuint  instanceCount{ 0 };
			GLuint  firstIndex{ 0 };
			GLuint  baseVertex{ 0 };
			GLuint  baseInstance{ 0 };
		};

		struct  RendererTask {
			fu2::function<void(void)> m_task;
			uint32 id{ 0 };
		};

		struct VertexInterleaved {
			float location[3];
			float normal[3];
			float texcoords[2];
		};
		const auto sizeasd = sizeof(VertexInterleaved);
		struct RENDERER_ModelData {
			stVector<VertexInterleaved> vertices;
			unsigned int num_verts;
		};

		struct VertexData {
			stVector<float> vertex_locations;
			stVector<float> vertex_normals;
			stVector<float> text_coords;
			stVector<unsigned int> indices;
		};
		struct BatchData {
			BatchData(const RenderInfo& _render_info);
			RenderInfo render_info;
			RENDERER_ModelData model_data;
			stVector<Texture> textures;
			size_t num_verts = 0;
			stVector<size_t> renderable_order;
			stVector<size_t> model_order;
			stVector<size_t> texture_order;
			size_t total_instances = 0;
			size_t shader_info_index;
		};

		struct RENDERER_GL_arrays_indirect {
			GLuint  count;
			GLuint  instanceCount;
			GLuint  first;
			GLuint  baseInstance;
		};
		struct BatchIndirects {
			stVector <RENDERER_GL_arrays_indirect> indirects;
		};

#define RENDERER_MAX_INSTANCES 2000
#define RENDERER_MAX_BATCHES 500

		struct ShaderHandle {
			ShaderHandle(const char* _vs_filename, const char* _fs_filename) :
				vs_handle(0),
				fs_handle(0)
			{
			}
			//void GetVShader(stString & out_string);
			//void GetFShader(stString & out_string);

			bool is_compiled = false;
			GLuint program_id{};
			GLuint vert_shader_id{};
			GLuint frag_shader_id{};
			size_t vs_handle{};
			size_t fs_handle{};
		};
		struct RenderableData {
			/*	RenderableData()  :
					batch_index(0),
					instance_index(0),
					num_instances(0)
				{
				}*/
			RenderableData(const RenderableData& _other) = default;
			size_t batch_index;
			size_t instance_index;
			size_t transform_index;
			unsigned int num_instances;
			unsigned int num_verts;
		};

		using FloatVec = stVector<GLfloat>;
		enum class RENDER_CB_MSG_TYPE {
			LoadBatch,
			LoadShader,
			DrawBatch
		};
		enum class RENDER_CB_MSG {
			Failure,
			Sucess
		};
		struct RenderCallback {
			uint32 identifier{ 0 };
			RENDER_CB_MSG_TYPE msg_type;
			RENDER_CB_MSG msg;
		};

		struct BatchRenderData {
			uint32_t batch_id{ 0 };
			uint32_t version{ UINT32_MAX };
			uint32_t batch_version{ 0 };
			stVector<glm::mat4> transforms;
			stVector<GL_Indirect> indirects;
			stVector<GL_IndexedIndirect> indexed_indirects;
			bool valid{ false };
		};
		struct RenderBuffer {
			glm::mat4 camera_mvp;
			glm::mat4 camera_view;
			uint32_t version{ UINT32_MAX };
			stVector<BatchRenderData> indexed_batch_data;
			stVector<BatchRenderData> batch_data;
			LightPropertiesBuffer m_dynamic_lighting;
			int screen_x{ 800 };
			int screen_y{ 600 };
			bool valid{ false };
			bool deferred{ false };
#ifdef SP_USE_EDITOR

			StoneyCore::Editor::EditorSceneWidget control_widget;

#endif // SP_USE_EDITOR
		};
		const auto safd = sizeof(RenderBuffer);
		struct ShaderProgram {
			GLuint program_id{ 0 };
		};
	}
	using VertexArray = stVector<Renderer::VertexInterleaved>;
}
#ifdef SP_USE_EDITOR

#include "ReflectedTypes.h"

#endif // SP_USE_EDITOR
