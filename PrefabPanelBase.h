#pragma once
#ifdef SP_USE_EDITOR

#include <wx/wx.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/manager.h>

class PrefabPanelBase : public wxPanel
{
private:

protected:
	wxPropertyGridManager* m_property_mgr;
	wxButton* m_show_prefab_button;
	wxChoice* m_prefab_choice;
	wxButton* m_apply_prefab_button;

public:

	PrefabPanelBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(471, 597), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~PrefabPanelBase();
};
#endif // SP_USE_EDITOR