#pragma once
#include "SEngineTypes.h"
#ifdef USE_SDL
#include <SDL.h>

typedef SDL_GLContext GL_Context;

#endif // USE_SDL
namespace StoneyCore {
	struct WindowInitInfo {
		int m_location_x;
		int m_location_y;
		int m_width;
		int m_height;
		uint32 m_flags;
	};
}