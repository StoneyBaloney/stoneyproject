#include <magic_enum.hpp>
#include "RenderBatch.h"

namespace StoneyCore {
	namespace Renderer {
		RenderBatch2::RenderBatch2()
		{
		}

		RenderBatch2::~RenderBatch2()
		{
		}
		auto tex_tuple(const TextureInfo& info) {
			return std::make_tuple(info.width, info.height, info.num_components);
		}

		inline void AppendFloatVector(stVector<float>& target, const stVector<float>& input) {
			target.insert(target.end(), input.begin(), input.end());
		}
		inline void AppendIntVector(stVector<GLuint>& target, const stVector<GLuint>& input) {
			target.insert(target.end(), input.begin(), input.end());
		}

		void LoadAssetToBatch(RenderBatch2& out_batch, const StoneyCore::ModelAsset& model, const ModelAssetDescTable& asset_desc) {
			uint32 texture_layer = 0;

			for (auto& mesh_group : asset_desc.textured_mesh_groups)
			{
				auto& batch_verts = out_batch.batch_verts.emplace_back();;
				auto& batch_pos = batch_verts.vertices;
				auto& batch_norm = batch_verts.normals;
				auto& batch_tcor = batch_verts.texcoords;
				auto& out_texture_channels = out_batch.textures;
				auto& batch_tangents = batch_verts.tangents;
				auto& batch_bitangents = batch_verts.bitangents;
				auto& batch_ind = batch_verts.indices;
				int tex_index = 0;

				for (auto& mesh_index : mesh_group) {
					auto& mesh_verts = model.vertices[mesh_index];
					uint32 num_verts = uint32(mesh_verts.indices.size());
					auto& mesh_pos = mesh_verts.vertices;
					AppendFloatVector(batch_pos, mesh_pos);

					auto& mesh_norm = mesh_verts.normals;
					AppendFloatVector(batch_norm, mesh_norm);

					auto& mesh_tang = mesh_verts.tangents;
					AppendFloatVector(batch_tangents, mesh_tang);

					auto& mesh_bitang = mesh_verts.bitangents;
					AppendFloatVector(batch_bitangents, mesh_bitang);

					auto& mesh_tcors = mesh_verts.texcoords;
					for (uint32 channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++) {

						if (model.textures[mesh_index][channel].valid && (model.meshes[mesh_index].mesh_info.texture_flags & (1 << channel)))
						{
							if (model.textures[mesh_index][channel].image)
							{

								out_texture_channels[channel].emplace_back(model.textures[mesh_index][channel]);
								
							}
						}
						AppendFloatVector(batch_tcor[channel], mesh_tcors[channel]);

					}


					auto& mesh_ind = mesh_verts.indices;
					AppendIntVector(batch_ind, mesh_ind);
				}
				
			}
		}

		void SortModelAssets(const StoneyCore::ModelAsset& model, stVector<ModelAssetDescTable>& asset_desc) {
			asset_desc.clear();
			std::array<int, 7>sadf;
			auto val = std::make_tuple(sadf);
			stVector<uint32> textured_mesh_indices;
			for (size_t i = 0; i < model.meshes.size(); i++)
			{
				if (model.meshes[i].mesh_info.texture_flags)
				{
					textured_mesh_indices.push_back(uint32(i));
				}
			}

			stMap<std::array<std::tuple<int, int, int>, 8>, stVector<uint32>> textured_batch_map;

			std::array<std::tuple<int, int, int>, 8> temp_key = {};

			for (size_t i = 0; i < textured_mesh_indices.size(); i++)
			{
				auto mesh_index = textured_mesh_indices[i];
				auto& mesh = model.meshes[mesh_index];
				auto& infos = model.texture_infos[mesh_index];
				for (uint32 type = 0; type < MAX_TEXTURE_CHANNELS; type++)
				{
					auto& info = infos[type];
					auto& tuple = temp_key[type];
					uint32 type_flag = (1u << type);
					int width{}, height{}, num_components{};
					if (mesh.mesh_info.texture_flags & type_flag)
					{
						
						width = info.width;
						height = info.height;
						num_components = info.num_components;
					}
					tuple = std::make_tuple(width, height, num_components);
				}
				textured_batch_map[temp_key].emplace_back(mesh_index);
			}
			asset_desc.resize(textured_batch_map.size());
			uint32 batch_index{};
			for (auto& batch : textured_batch_map)
			{
				auto& mesh_indices = batch.second;
				auto& out_indices = asset_desc[batch_index];

				Append(mesh_indices, out_indices.textured_meshes);
				stMap<std::array<stString, 8>, uint32> mesh_group; // matches the texture config to the batch
				for (auto& mesh_index : mesh_indices)
				{
					auto& mesh = model.meshes[mesh_index];
					auto& textures = mesh.textures;
					auto find_tex = mesh_group.find(textures);
					if (find_tex == mesh_group.end())
					{
						mesh_group[textures] = batch_index;
						out_indices.textured_mesh_groups.emplace_back().emplace_back(mesh_index);
					}
					else {
						auto batch_index = (*find_tex).second;

						out_indices.textured_mesh_groups[batch_index].emplace_back(mesh_index);
					}
				}
				batch_index++;
			}
		}

		void InitModelBatches(const stVector<RenderBatch2*>& batches, const StoneyCore::ModelAsset& model, const stVector<ModelAssetDescTable>& asset_desc)
		{
			LogErrorToFile("TESTING");
			if (batches.size() != asset_desc.size())
			{
				return;
			}
			for (size_t i = 0; i < asset_desc.size(); i++)
			{
				if (asset_desc[i].textured_meshes.size()) {
					//	TextureInfo tex_info = //model.diffuse_tex_infos[][asset_desc[i].diffuse_tex_meshes[0]];
					auto* batch = batches[i];

					batch->texture_infos = model.texture_infos[asset_desc[i].textured_meshes[0]];

					Renderer::LoadAssetToBatch(*batch, model, asset_desc[i]);
				}
				else {
					continue;
				}
			}
		}

		stVector<RenderBatch2*> AllocBatches(uint32 num_batches, DataRegistrar<>& reg, uint32 next_id, stVector<uint32>& out_indices) {
			stVector<RenderBatch2*> ret;
			auto pushed = 0;
			while (pushed != num_batches)
			{
				auto* next = reg.Create<RenderBatch2>(next_id++);

				while (!next)
				{
					next = reg.Create<RenderBatch2>(next_id++);
				}

				out_indices.emplace_back(next_id - 1);
				ret.emplace_back(next);
				pushed++;
			}
			return ret;
		}
	}
}