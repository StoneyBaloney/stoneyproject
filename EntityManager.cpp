#include "EntityManager.h"

EntID EnityManager::AddInstance(PrefabID& prefab_id) {
	auto ret = EntID(entity_reg.create());
	entity_reg.assign<PrefabID>(ret, prefab_id);
	return ret;
}

EnityManager::EnityManager()
{
}

EnityManager::~EnityManager()
{
}