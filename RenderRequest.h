#pragma once
#include "pch.h"
namespace StoneyCore {
	namespace Renderer {
		enum class RENDER_REQUEST_TYPE {
			LoadObject,
			UnloadObject,
			Shutdown,
		};
		union RequestData
		{
			RendererInitInfo init_info;
		};

		struct BatchIDs {
			GLuint program_id;
			GLuint vao_id;
			GLuint vbo_id;
			GLuint instance_data_buffer;
			GLuint element_buffer;
			GLuint texture_id;
			GLuint num_draws;
			GLuint indirect_buffer;
			size_t indirect_index;
			size_t instance_begin;
		};
		struct RenderRequest
		{
			RENDER_REQUEST_TYPE request_type;
			RequestData request_data;
			void Init(RendererInitInfo init) {
				request_data.init_info = init;
			}
			void LoadBatch() {
			}
		public:
			RenderRequest();
			~RenderRequest();
		};
	}
}