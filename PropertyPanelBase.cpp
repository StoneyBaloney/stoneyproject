#ifdef SP_USE_EDITOR

#include "PropertyPanelBase.h"

PropertyPanelBase::PropertyPanelBase(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style, name)
{
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	m_main_notebook = new wxAuiNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_TAB_EXTERNAL_MOVE);

	bSizer2->Add(m_main_notebook, 16, wxEXPAND | wxALL, 5);

	bSizer1->Add(bSizer2, 16, wxEXPAND, 0);

	this->SetSizer(bSizer1);
	this->Layout();
}

PropertyPanelBase::~PropertyPanelBase()
{
}
#endif // SP_USE_EDITOR