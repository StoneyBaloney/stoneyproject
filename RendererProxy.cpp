#include "RendererProxy.h"

namespace StoneyCore {
	namespace Renderer {
		inline auto str() {}
		inline int64_t GenerateShaderID(const ShaderGenParams& params) {
			static_assert(sizeof(char) == sizeof(int8_t));
		//	stString ret{};
			//ret += std::to_string(params.vertex_inputs);
			//ret += std::to_string(params.vertex_outputs);
			//ret += std::to_string(params.frag_outputs);
			//for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
			//{
			//	ret += std::to_string(channel);
			//	ret += std::to_string(params.uv_channels[channel]);
			//}
			//ret += std::to_string(params.texture_flags);
			//ret += std::to_string(params.gen_type);






			int64_t ret{};
			ret |= int64_t(params.vertex_inputs);
			ret |= (int64_t(params.vertex_outputs) << 8ll);
			ret |= (int64_t(params.frag_outputs) << 16ll);
			for (int8_t channel = 0; channel < MAX_TEXTURE_CHANNELS; channel++)
			{
				
				ret |= (int64_t(1) << channel) << int64_t((16ll+channel));
			}
			ret |= (int64_t(params.texture_flags) << 32ll);
			ret |= (int64_t(params.gen_type) << 48ll);

			return ret;
		}

		void RendererProxy::PrepareModelShaders(const StoneyCore::ModelAsset& model, stVector<Renderer::RenderBatch2*>& batches, stVector<ModelAssetDescTable>& model_batch_descs) {
			stVector<Renderer::ShaderGenParams> gen_params;
			ShaderGenerator::GenerateDeferredShaderParams(model, gen_params);

			for (auto& param : gen_params) {
				int64_t key = GenerateShaderID(param);

				if (shader_ids.find(key) == shader_ids.end())
				{
					auto& shader = shader_ids[key];
					auto& vert = shader.vertex_shader;
					auto& frag = shader.frag_shader;
					ShaderGenerator::GenerateShader(param, vert, frag);
					ShaderLoadMessage msg;
					msg.frag_shader = frag;
					msg.vert_shader = vert;
					msg.shader_type = SHADER_PASS_TYPE::SG_Renderpass_Geometry;
					msg.program_id = key;
					msg.load_info.shader_name = std::to_string(key);
					SubmitShader(msg);
					auto vert_name = Concat("Shaders/Generated/",std::to_string(key),".vert");
					auto frag_name = Concat("Shaders/Generated/",std::to_string(key), ".frag");
					AssetHandler::SaveTextOverwrite(vert_name,vert);
					AssetHandler::SaveTextOverwrite(frag_name,frag);


				}
			}

			bool check_array_size = model_batch_descs.size() == batches.size();
			if (check_array_size)
			{
				uint32 batch_index{};
				for (auto& batch : model_batch_descs) 
				{
					for (auto& mesh_group : batch.textured_mesh_groups)
					{
						for (auto& mesh_index : mesh_group)
						{
							auto key = GenerateShaderID(gen_params[mesh_index]);
							if (shader_ids.find(key) != shader_ids.end())
							{
								batches[batch_index]->shader_id = key;
								batches[batch_index]->shader_params = shader_ids[key].params;
							}
							else {
								batches[batch_index]->shader_id = 0;
							}
							break;
						}
					}
					batch_index++;
				}
			}
			else {
				LogErrorToFile("Improper internal format for render batch. See RenderProzy.cpp.\n");
				return;
			}

		}

		void RendererProxy::PrepareModelBatches(const ModelAsset& model, const PrefabDescription& description, stVector<uint32>& out_batch_ids, stVector<ModelAssetDescTable>& model_batch_descs)
		{
			model_batch_descs.clear();
			static stVector<Renderer::RenderBatch2*> batch_vec;
			batch_vec.clear();
			Renderer::SortModelAssets(model, model_batch_descs);
			for (size_t i = 0; i < model_batch_descs.size(); i++)
			{
				auto batch = m_batch_pool.construct();
				auto batch_id = m_next_batch_id++;
				out_batch_ids.emplace_back(batch_id);
				batch->batch_id = batch_id;
				batch_vec.emplace_back(batch);
				batch->render_spec = description.render_spec;
				batch->batch_type = 1;
				num_batches++;
			}
			Renderer::InitModelBatches(batch_vec, model, model_batch_descs);
			PrepareModelShaders(model, batch_vec,model_batch_descs);
			Append(batch_vec, pending_batches);
		}

		void RendererProxy::SubmitShader(const ShaderLoadMessage& shader) {
			m_queues.shader_queue->enqueue(shader);
		}

		void RendererProxy::StartRenderer() {
			if (m_batch_renderer)
			{
				m_batch_renderer->Start();
			}
		}

		void RendererProxy::SubmitPendingBatches() {
			if (m_batch_renderer && m_queues.batch_queue)
			{
				m_queues.batch_queue->enqueue_bulk(m_renderer_handle.batch_ptok, pending_batches.begin(), pending_batches.size());
				pending_batches.clear();
			}
		}

		void RendererProxy::ShutdownRenderer() {
			m_batch_renderer->Shutdown();
		}

		void RendererProxy::WaitForFreeBatch(Renderer::RenderBatch2* batch) {
			if (m_free_batches.find(batch) != m_free_batches.end())
			{
				return;
			}
			else {
				Renderer::RenderBatch2* deque_temp[64] = { nullptr };
				while (m_free_batches.find(batch) == m_free_batches.end())
				{
					while (auto num_batches = m_queues.free_batch_queue->try_dequeue_bulk(deque_temp, 64)) {
						m_free_batches.insert(deque_temp, &deque_temp[num_batches]);
					}
				}
			}
		}

		bool RendererProxy::BeginSceneUpdate(Renderer::RenderBuffer*& out_buffer_p, bool wait_for_batch) {
			bool ret{ false };
			out_buffer_p = nullptr;
			while ((!m_queues.free_buffer_queue->try_dequeue(out_buffer_p)) && wait_for_batch)
			{
			}
			if (out_buffer_p)
			{
				ret = true;
			}

			return ret;
		}

		void RendererProxy::Shutdown() {
			if (m_batch_renderer)
			{
				m_batch_renderer->Shutdown();
			}
		}

		void RendererProxy::SinkRenderBuffer(Renderer::RenderBuffer* buffer) {
			m_queues.render_buffer_queue->enqueue(buffer);
		}

		bool RendererProxy::RendererIsValid() {
			return bool(m_batch_renderer);
		}

		uint32 RendererProxy::AddMaterial(const Material& mat) {
			return materials.AddMaterial(mat);
		}

		RendererProxy::RendererProxy() :
			m_renderer_handle(m_data_reg)
		{
			if (m_renderer_handle.m_queues.IsValid())
			{
				m_batch_renderer = m_data_reg.Create<Renderer::BatchRenderer3D>(
					"m_batch_renderer"_h32, m_renderer_handle.m_queues);
			}
			if (!m_batch_renderer)
			{
				LogErrorToFile("Failed to start renderer!");
			}
		}

		RendererProxy::~RendererProxy() {
			if (m_batch_renderer)
			{
				m_batch_renderer->Shutdown();
			}
		}

		void RendererProxy::QueueBatches(stVector<RenderBatch2*>& batches) {
			Append(batches, pending_batches);
		}
		HWND RendererProxy::InitRendererEditor(const Renderer::RendererInitInfo& init_info) {
			if (m_batch_renderer)
			{
				m_queues.render_buffer_queue->enqueue_bulk(m_renderer_handle.buffer_ptok, m_renderer_handle.m_render_buffers, MAX_RENDER_BUFFERS);

				return m_batch_renderer->InitInEditor(init_info);
			}
			return NULL;
		}
		constexpr uint32 AMBIENT = 0;
		constexpr uint32 DIFFUSE = 3;
		constexpr uint32 SPECULAR = 6;
		constexpr uint32 SHININESS = 9;
		void SetMaterialValues(const glm::vec3& in_vec, const uint32 index, stVector<float>& out) {
			if (!((index + 2) < out.size()))
			{
				return;
			}
			for (size_t i = 0; i < 3; i++)
			{
				out[i + index] = in_vec[i];
			}
		}
	
		uint32 MaterialBuffer::AddMaterial(const Material& mat) {
			auto start = uint32(m_mat_buffer.size());
			m_mat_buffer.resize(m_mat_buffer.size() + (RG_NUM_MATERIALS * 3u));
			SetMaterialValues(mat.ambient, start + AMBIENT, m_mat_buffer);
			SetMaterialValues(mat.diffuse, start + DIFFUSE, m_mat_buffer);
			SetMaterialValues(mat.specular, start + SPECULAR, m_mat_buffer);
			SetMaterialValues(mat.shininess, start + SHININESS, m_mat_buffer);	
		

			return start;
		}
	}
}