#include "RenderUtils.h"

StoneyCore::Renderer::RenderInitializer::RenderInitializer(DataRegistrar<>& data_reg) :
	batch_ptok(*data_reg.Create<Renderer::BatchQueue>("m_batch_queue"_h32, 20, 8, 8)),
	buffer_ptok(*data_reg.Create<Renderer::RenderBufferQueue>("m_render_buffer_queue"_h32, 20, 8, 8)),
	render_queue_ptok(*data_reg.Create<Renderer::RenderQueue>("m_render_queue"_h32, 20, 8, 8)),
	shader_ptok(*data_reg.Create<Renderer::ShaderQueue>("m_shader_queue"_h32, 20, 8, 8))
{
	m_queues.render_callback_queue = data_reg.Create<RenderCallbackQueue>("m_render_cb_queue"_h32, 20, 8, 8);
	m_queues.free_batch_queue = data_reg.Create<BatchQueue>("m_free_batch_queue"_h32, 20, 8, 8);
	m_queues.free_buffer_queue = data_reg.Create<RenderBufferQueue>("m_free_buffer_queue"_h32, 6ull * MAX_RENDER_BUFFERS, 8ull, 8ull);
	m_queues.render_queue = data_reg.GetData< RenderQueue>("m_render_queue"_h32);
	m_queues.batch_queue = data_reg.GetData<BatchQueue>("m_batch_queue"_h32);
	m_queues.render_buffer_queue = data_reg.GetData<RenderBufferQueue>("m_render_buffer_queue"_h32);
	m_queues.shader_queue = data_reg.GetData<ShaderQueue>("m_shader_queue"_h32);

	for (size_t i = 0; i < MAX_RENDER_BUFFERS; i++)
	{
		auto name = std::string("render_buffer");
		name += uint64(i);
		m_render_buffers[i] = data_reg.Create<RenderBuffer>(hash_32_fnv1a_const(name.c_str()));
	}

	for (auto buffer : m_render_buffers) {
		if (!buffer)
		{
			LogErrorToFile("Failed to create render buffers!");
		}
	}
}