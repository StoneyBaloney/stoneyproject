#pragma once
#include <entt/entity/registry.hpp>

#include "SEngineTypes.h"
#include "GameData.h"
#include "AssetHandler.h"
#include "RenderBatch.h"
#include "RendererProxy.h"
#include "StPxWorld.h"
#include "AssetHandler.h"
#include "EntityManager.h"
#include "StPxWorld.h"
#include "Timing.h"
#include "ShaderGenerator.h"

namespace StoneyCore {
	struct stBatchInfo {
		stVector<Renderer::ModelInfo> infos;
		uint32 num_renderables{};
		uint32 batch_index{};
	};
	struct PrefabInstanceData {
		uint32 batch_version{ 0 };
		uint32 num_instances{};

		stVector<uint32> batch_ids;
		stVector<stBatchInfo> batch_infos;
		bool indexed{ false };

		entt::registry m_instance_reg;
	};
	struct ModelComponent {
		stString name;
		stVector<uint32> batch_offsets;
	};
	struct PrefabMeta {
		PrefabDescription description;
	};
	struct PrefabHandle2 {
	};
	struct InstanceBufferInfo {
		uint32 batch_index{ 0 };
		uint32 num_instances{};
	};
	void DefaultExitCallback();;
	class InstanceRegistry
	{
		boost::object_pool<PrefabInstanceData> m_instance_buffer_pool;
		DataRegistrar<> m_data_reg;
		Renderer::RendererProxy m_render_proxy;
		uint32 render_buffer_version{};
		fu2::function<void(void)> m_exit_callback;
		ScreenSettings m_screen_settings;
		Camera main_camera;
		stUnorderedMap<PrefabID, PrefabInstanceData*> m_prefab_map;

		stVector<PrefabInstanceData*> m_instance_buffers;

		PrefabID m_next_id{ 1 };
		uint32 m_next_batch_id{};
		StPxPhysics::StPxWorld* m_physics_world{ nullptr };

		stClock m_clock;

		StoneyCore::AssetCache m_asset_manager;
		ShaderTracker m_shader_tracker;

		uint32 num_indexed_batches{ 0 };
		uint32 num_non_indexed_batches{ 0 };

		PrefabID LoadModelsAndInitBatches(const PrefabDescription& description);
		PrefabID CreateInstanceBufferObject(const PrefabDescription& description, const stVector<uint32>& batch_ids, const StoneyCore::ModelAsset& model, const stVector<ModelAssetDescTable>& model_batch_desc);
		void BeginUpdatePhysicsScene(float delta);
		void GetPhysicsResults();
		
		
	public:
		InstanceRegistry(fu2::function<void(void)> exit_callback = DefaultExitCallback);
		~InstanceRegistry();

		void HandleRenderWindowResize(int x, int y);
		void HandleEvent(const SDL_Event& evt);

		bool InitPhysics();
		void PrepareRenderBuffer(StoneyCore::Renderer::RenderBuffer& buffer);
		void WriteSceneToRenderBuffer(StoneyCore::Renderer::RenderBuffer& buffer);
		void UpdateRenderBuffer(Renderer::RenderBuffer& buffer);
		void SubmitRenderPasses();
		void StartRenderer();
		void SubmitPendingBatches();
		void ShutdownRenderer();
		void AddInstance(const PrefabID& prefab_id, const Transform& transform);

		PrefabID AddPrefab(const PrefabDescription& description);
		Camera GetMainCamera();
		double GetDelta();
		double GetDeltaAvg();


#ifdef SP_USE_EDITOR
		void SinkRenderBuffer(Renderer::RenderBuffer* buffer);
		bool BeginSceneUpdate(Renderer::RenderBuffer*& buffer);
		uint32 AddMaterial(const StoneyCore::Material& mat) {
			return m_render_proxy.AddMaterial(mat);
		}
		HWND InitRendererEditor(const Renderer::RendererInitInfo& init_info);
#endif // SP_USE_EDITOR
	};
}