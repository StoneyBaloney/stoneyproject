#pragma once
#ifdef SP_USE_EDITOR

#include "PropertyPanelBase.h"
#include "StShaderPanel.h"
#include "StPrefabPanel.h"
#include "StLightEditorPanel.h"
class StPropertyPanel : public PropertyPanelBase
{
public:
	StShaderPanel* m_shader_panel{ nullptr };
	StPrefabPanel* m_prefab_panel{ nullptr };
	StLightEditorPanel* m_lighting_panel{ nullptr };

	friend class EditorApp;
	friend class StoneyCore::Editor::EditorSceneHandler;
	friend class StoneyCore::Editor::EditorSceneManager;
	StPropertyPanel(wxWindow* parent = nullptr, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(315, 540), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);

	~StPropertyPanel();
};
#endif // SP_USE_EDITOR
