#pragma once
#include <utility>
#include <entt/entity/registry.hpp>
#include "SEngineTypes.h"

//class PrefabID {
//	entt::entity id;
//public:
//	PrefabID() = default;
//	PrefabID(entt::entity&& other_id) :id(other_id) {
//	}
//	PrefabID(entt::entity& other_id) :id(other_id) {
//	}
//	operator entt::entity&() {
//		return id;
//	}
//
//	friend class EnityManager;
//};
//
//class EntID {
//	entt::entity id;
//public:
//	EntID() = default;
//	EntID(entt::entity& other_id) :id(other_id) {
//	}
//	EntID(entt::entity other_id) :id(other_id) {
//	}
//	operator entt::entity&() {
//		return id;
//	}
//	friend class EnityManager;
//};
//class RegistryID {
//	entt::entity id;
//public:
//	RegistryID(entt::entity&& other_id) :id(other_id) {
//	}
//	operator entt::entity&() {
//		return id;
//	}
//	friend class EnityManager;
//};

using EntType = entt::entity;
using PrefabID = entt::entity;
using EntID = entt::entity;
using RegistryID = entt::entity;
using RegistryVector = stVector<entt::registry>;
class EnityManager
{
public:

	entt::registry prefab_reg;
	entt::registry entity_reg;
	template <typename PrefabType>
	auto AddPrefab(const PrefabType& prefab) {
		PrefabID ret(prefab_reg.create());
		prefab_reg.assign<PrefabType>(ret, prefab);

		return ret;
	}
	template <typename PrefabType>
	auto& GetPrefab(PrefabID prefab_id) {
		return prefab_reg.get<PrefabType>(prefab_id);
	}
	EntID AddInstance(PrefabID& prefab_id);
	template <typename Component>
	auto& AddComponent(EntID& entity_id, Component& comp) {
		return entity_reg.assign<Component>(entity_id, comp);
	}
	template <typename Component>
	auto& GetComponent(EntID& entity_id, Component& comp) {
		return entity_reg.get<Component>(entity_id);
	}

	template <typename PrefabMeta>
	auto& AssignPrefabMeta(PrefabID& prefab_id, PrefabMeta& prefab_meta) {
		return prefab_reg.assign<PrefabMeta>(prefab_id, prefab_meta);
	}
	template <typename MetaType, typename...Args>
	auto& AssignPrefabMeta(PrefabID& prefab_id, Args&& ...args) {
		return prefab_reg.assign<MetaType>(prefab_id, args...);
	}
	template <typename MetaType, typename...Args>
	auto& AssignPrefabMeta(PrefabID& prefab_id, Args& ...args) {
		return prefab_reg.assign<MetaType>(prefab_id, args...);
	}
	template <typename MetaType>
	auto& AssignPrefabMeta(PrefabID& prefab_id) {
		return prefab_reg.assign<MetaType>(prefab_id);
	}

	EnityManager();
	~EnityManager();
};
