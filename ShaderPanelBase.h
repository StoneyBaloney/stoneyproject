#pragma once
#ifdef SP_USE_EDITOR

#include <wx/wx.h>
#include <wx/stc/stc.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/manager.h>
#include <wx/aui/auibook.h>
class ShaderPanelBase : public wxPanel
{
private:

protected:
	wxAuiNotebook* m_auinotebook;
	wxPanel* m_shader_info_panel;
	wxPropertyGridManager* m_property_mgr;
	wxPanel* m_shader_panel;
	wxStyledTextCtrl* m_scintilla3;
	wxPanel* m_ctrl_panel;
	wxButton* m_apply_button;
	wxChoice* m_shader_choices;
	wxButton* m_load_button;
	wxButton* m_save_button;
	wxButton* m_saveas_button;

public:

	ShaderPanelBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(583, 512), long style = wxHSCROLL | wxTAB_TRAVERSAL, const wxString& name = wxEmptyString);
	~ShaderPanelBase();
};
#endif // SP_USE_EDITOR
