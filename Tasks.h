#pragma once
#include <function2/function2.hpp>
#include "Defines.h"
#include "BigBitField.h"

using TaskIDField = BigBitField<MAX_TASKS>;
using DataIDField = BigBitField<MAX_DATA_TYPES>;

typedef uint64_t STONEY_SYSTEM_ID_MASK;
typedef uint64_t TASK_ID;

using Task = fu2::function<void(void)>;

enum SGS_THREAD_AFFINITY : uint8_t {
	MainOnly,
	RenderOnly,
	Relaxed,
	PinToSingle
};

struct TaskInfo {
	TaskInfo(
		uint32_t _id, TaskIDField _dependencies, DataIDField _readable_ids, DataIDField _writable_ids,
		SGS_THREAD_AFFINITY _thread_affinity = SGS_THREAD_AFFINITY::MainOnly
	) :
		m_id(_id),
		dependencies(_dependencies),
		m_readable_data_ids(_readable_ids),
		m_writable_data_ids(_writable_ids),
		thread_affinity(_thread_affinity)
	{
	}
	TaskInfo(SGS_THREAD_AFFINITY _thread_affinity = SGS_THREAD_AFFINITY::MainOnly) :
		thread_affinity(_thread_affinity),
		m_id(0)
	{
	}
	fu2::function<void()> func;
	SGS_THREAD_AFFINITY thread_affinity;
	uint32_t m_id;
	TaskIDField dependencies;
	DataIDField m_readable_data_ids;
	DataIDField m_writable_data_ids;
};