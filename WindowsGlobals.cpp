#include "Defines.h"
#ifdef _WIN32
#ifndef USE_SDL

#include <type_traits>
#include "WindowsGlobals.h"

namespace WindowsData {
	namespace {
		WinData g_data;
	}

	void SetWinData() {
		g_data.m_hinstance = GetModuleHandle(NULL);

		g_data.m_pcmdLine = GetCommandLine();

		g_data.m_device_context = GetDC(g_data.m_hwnd);
	}

	void SetHwnd(HWND _hwnd)
	{
		g_data.m_hwnd = _hwnd;
	}

	const WinData GetWinData() {
		return g_data;
	}
}

#endif // !USE_SDL
#endif // _WIN32