#ifdef SP_USE_EDITOR
#include "EditorFrame.h"
#include "SEditor.h"

EditorFrame::EditorFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxAuiMDIParentFrame(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_ALLOW_ACTIVE_PANE | wxAUI_MGR_ALLOW_FLOATING | wxAUI_MGR_DEFAULT);

	m_menubar = new wxMenuBar(0);
	m_file_menu = new wxMenu();
	wxMenuItem* m_menu_exit;
	m_menu_exit = new wxMenuItem(m_file_menu, wxID_ANY, wxString(wxT("Exit")), wxEmptyString, wxITEM_NORMAL);
	m_file_menu->Append(m_menu_exit);

	m_menubar->Append(m_file_menu, wxT("File"));

	m_help_window = new wxMenu();
	m_menubar->Append(m_help_window, wxT("Help"));

	this->SetMenuBar(m_menubar);

	m_mgr.Update();
	this->Centre(wxBOTH);
}

EditorFrame::~EditorFrame()
{
	m_mgr.UnInit();
}
#endif // !1