#pragma once
#include "Tasks.h"
struct DAGNode {
	TaskIDField vertices;
	uint32_t m_id;
};

using TaskGraph = stVector<DAGNode>;

class DAG {
	uint32_t start{ 0 };
	bool complete{ 0 };

	TaskIDField contained_tasks;

public:
	TaskGraph m_graph;
	using GraphType = decltype(m_graph);

	void Clear();

	bool GenDag(const stVector<TaskInfo>& g_infos);

	bool AddTask();

	const TaskGraph& Get() {
		return m_graph;
	}

	template <typename PrintFunc>
	void Print(PrintFunc& print_func) {
		print_func(m_graph);
	}
	template <typename PrintFunc>
	void Print(PrintFunc&& print_func) {
		print_func(m_graph);
	}
private:
	bool initGraph(const stVector<TaskInfo>& g_infos, TaskIDField& pushed_ids, TaskIDField& remaining_indices);
	TaskIDField getNextTasks(const stVector<TaskInfo>& g_infos, TaskIDField& _remaining, TaskIDField& pushed_ids);
};